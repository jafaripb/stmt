use stmtsdm;

-- NIDN
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'tbl_dosen' AND COLUMN_NAME = 'nidn')
	ALTER TABLE dbo.tbl_dosen ADD nidn VARCHAR(64) NULL;
	
-- Status Aktif
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tbl_ref_status_aktif')
BEGIN
	CREATE TABLE [dbo].[tbl_ref_status_aktif](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[status_aktif] [varchar](50) NOT NULL,
		[keterangan] [varchar](255) NULL,
	 CONSTRAINT [PK_tbl_ref_status_aktif] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'tbl_dosen' AND COLUMN_NAME = 'status_aktif_id')
BEGIN
	ALTER TABLE dbo.tbl_dosen ADD status_aktif_id INT NULL;
	ALTER TABLE dbo.tbl_dosen ADD CONSTRAINT FK_status_aktif FOREIGN KEY (status_aktif_id) REFERENCES tbl_ref_status_aktif (id);
END
--seed
IF NOT EXISTS (SELECT * FROM dbo.tbl_ref_status_aktif WHERE status_aktif = 'Aktif')
	INSERT INTO dbo.tbl_ref_status_aktif (status_aktif, keterangan) VALUES ('Aktif', 'Aktif');
IF NOT EXISTS (SELECT * FROM dbo.tbl_ref_status_aktif WHERE status_aktif = 'Tidak Aktif')
	INSERT INTO dbo.tbl_ref_status_aktif (status_aktif, keterangan) VALUES ('Tidak Aktif', 'Tidak Aktif');





--------- Asal Perguruan Tinggi
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tbl_riwayat_pendidikan')
BEGIN
	CREATE TABLE [dbo].[tbl_riwayat_pendidikan](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[dosen_id] [int] NOT NULL,
		[jenjang] [varchar](4) NOT NULL,
		[nama_institusi] [varchar](128) NOT NULL,
		[program_studi] [varchar](128) NULL,
		[gelar] [varchar](50) NULL,
		[create_on] [datetime] NULL,
		[create_by] [varchar](50) NULL,
		[modified_on] [datetime] NULL,
		[modified_by] [varchar](50) NULL,
		
	 CONSTRAINT [PK_tbl_riwayat_pendidikan] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	
	ALTER TABLE [dbo].[tbl_riwayat_pendidikan]  WITH CHECK ADD  CONSTRAINT [FK_tbl_riwayat_pendidikan_tbl_dosen] FOREIGN KEY([dosen_id])
	REFERENCES [dbo].[tbl_dosen] ([id])
	ON UPDATE CASCADE
	ON DELETE CASCADE

	ALTER TABLE [dbo].[tbl_riwayat_pendidikan] CHECK CONSTRAINT [FK_tbl_riwayat_pendidikan_tbl_dosen]
	
END


--------- Bidang Keahlian

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tbl_bidang_keahlian')
BEGIN
	CREATE TABLE [dbo].[tbl_bidang_keahlian](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[dosen_id] [int] NOT NULL,
		[bidang_keahlian] [varchar](128) NOT NULL,
		[create_on] [datetime] NULL,
		[create_by] [varchar](50) NULL,
		[modified_on] [datetime] NULL,
		[modified_by] [varchar](50) NULL,
	 CONSTRAINT [PK_tbl_bidang_keahlian] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	 ALTER TABLE [dbo].[tbl_bidang_keahlian]  WITH CHECK ADD  CONSTRAINT [FK_tbl_bidang_keahlian_tbl_dosen] FOREIGN KEY([dosen_id])
	REFERENCES [dbo].[tbl_dosen] ([id])
	ON UPDATE CASCADE
	ON DELETE CASCADE
END

----- pengalaman bidang transportasi
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tbl_pengalaman_bdg_transport')
BEGIN
	CREATE TABLE [dbo].[tbl_pengalaman_bdg_transport](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[dosen_id] [int] NOT NULL,
		[bidang_keahlian] [varchar](128) NOT NULL,
		[create_on] [datetime] NULL,
		[create_by] [varchar](50) NULL,
		[modified_on] [datetime] NULL,
		[modified_by] [varchar](50) NULL,
	 CONSTRAINT [PK_tbl_pengalaman_bdg_trans] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	 ALTER TABLE [dbo].[tbl_pengalaman_bdg_transport]  WITH CHECK ADD  CONSTRAINT [FK_tbl_pengalaman_bdg_transport_tbl_dosen] FOREIGN KEY([dosen_id])
	REFERENCES [dbo].[tbl_dosen] ([id])
	ON UPDATE CASCADE
	ON DELETE CASCADE
END

----- pengalaman bidang nontransasi
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tbl_pengalaman_bdg_nontrans')
BEGIN
	CREATE TABLE [dbo].[tbl_pengalaman_bdg_nontrans](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[dosen_id] [int] NOT NULL,
		[bidang_keahlian] [varchar](128) NOT NULL,
		[create_on] [datetime] NULL,
		[create_by] [varchar](50) NULL,
		[modified_on] [datetime] NULL,
		[modified_by] [varchar](50) NULL,
	 CONSTRAINT [PK_tbl_pengalaman_bdg_nontrans] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	 ALTER TABLE [dbo].[tbl_pengalaman_bdg_nontrans]  WITH CHECK ADD  CONSTRAINT [FK_tbl_pengalaman_bdg_nontrans_tbl_dosen] FOREIGN KEY([dosen_id])
	REFERENCES [dbo].[tbl_dosen] ([id])
	ON UPDATE CASCADE
	ON DELETE CASCADE
END

----- mata kuliah yg diampu
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tbl_ampu_matkul')
BEGIN
	CREATE TABLE [dbo].[tbl_ampu_matkul](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[dosen_id] [int] NOT NULL,
		[mata_kuliah] [varchar](128) NOT NULL,
		[create_on] [datetime] NULL,
		[create_by] [varchar](50) NULL,
		[modified_on] [datetime] NULL,
		[modified_by] [varchar](50) NULL,
	 CONSTRAINT [PK_tbl_ampu_matkul] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	 ALTER TABLE [dbo].[tbl_ampu_matkul]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ampu_matkul_tbl_dosen] FOREIGN KEY([dosen_id])
	REFERENCES [dbo].[tbl_dosen] ([id])
	ON UPDATE CASCADE
	ON DELETE CASCADE
END

----- pelatihan
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tbl_pelatihan_dosen')
BEGIN
	CREATE TABLE [dbo].[tbl_pelatihan_dosen](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[dosen_id] [int] NOT NULL,
		[pelatihan] [varchar](128) NOT NULL,
		[create_on] [datetime] NULL,
		[create_by] [varchar](50) NULL,
		[modified_on] [datetime] NULL,
		[modified_by] [varchar](50) NULL,
	 CONSTRAINT [PK_tbl_pelatihan_dosen] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	 ALTER TABLE [dbo].[tbl_pelatihan_dosen]  WITH CHECK ADD  CONSTRAINT [FK_tbl_pelatihan_dosen_tbl_dosen] FOREIGN KEY([dosen_id])
	REFERENCES [dbo].[tbl_dosen] ([id])
	ON UPDATE CASCADE
	ON DELETE CASCADE
END

----- seminar
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tbl_seminar_dosen')
BEGIN
	CREATE TABLE [dbo].[tbl_seminar_dosen](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[dosen_id] [int] NOT NULL,
		[seminar] [varchar](128) NOT NULL,
		[create_on] [datetime] NULL,
		[create_by] [varchar](50) NULL,
		[modified_on] [datetime] NULL,
		[modified_by] [varchar](50) NULL,
	 CONSTRAINT [PK_tbl_seminar_dosen] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	 ALTER TABLE [dbo].[tbl_seminar_dosen]  WITH CHECK ADD  CONSTRAINT [FK_tbl_seminar_dosen_tbl_dosen] FOREIGN KEY([dosen_id])
	REFERENCES [dbo].[tbl_dosen] ([id])
	ON UPDATE CASCADE
	ON DELETE CASCADE
END

----- penelitian
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tbl_penelitian_dosen')
BEGIN
	CREATE TABLE [dbo].[tbl_penelitian_dosen](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[dosen_id] [int] NOT NULL,
		[penelitian] [varchar](128) NOT NULL,
		[create_on] [datetime] NULL,
		[create_by] [varchar](50) NULL,
		[modified_on] [datetime] NULL,
		[modified_by] [varchar](50) NULL,
	 CONSTRAINT [PK_tbl_penelitian_dosen] PRIMARY KEY CLUSTERED 
	(
		[id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	 ALTER TABLE [dbo].[tbl_penelitian_dosen]  WITH CHECK ADD  CONSTRAINT [FK_tbl_penelitian_dosen_tbl_dosen] FOREIGN KEY([dosen_id])
	REFERENCES [dbo].[tbl_dosen] ([id])
	ON UPDATE CASCADE
	ON DELETE CASCADE
END

----- homebase
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'tbl_dosen' AND COLUMN_NAME = 'homebase')
	ALTER TABLE dbo.tbl_dosen ADD homebase VARCHAR(10) NULL;
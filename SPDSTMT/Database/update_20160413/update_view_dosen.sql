USE [stmtsdm]
GO

/****** Object:  View [dbo].[view_dosen]    Script Date: 04/14/2016 14:17:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*					  dbo.tbl_ref_status_aktif AS rsa ON rsa.id = a.status_aktif LEFT OUTER JOIN--*/
ALTER VIEW [dbo].[view_dosen]
AS
SELECT     a.id AS dosen_id, a.kd_nik, ISNULL(LTRIM(RTRIM(b.gelar_depan)) + ' ', '') + ISNULL(LTRIM(RTRIM(b.nama)) + ' ', '') + ISNULL(LTRIM(RTRIM(b.gelar_belakang)) + ' ', '') AS nama_dosen, 
                      b.tempat_lahir, b.tanggal_lahir, b.tanggal_masuk, b.tanggal_sk, b.jenis_kelamin_id, c.jenis_kelamin, b.agama_id, d.agama, a.status_dosen_id, e.status_dosen, f.id AS sertifikasi_id, b.gelar_depan, 
                      b.gelar_belakang, b.nama, h.id AS pendidikan_id, h.tingkat_pendidikan, h.nilai AS nilai_pendidikan, i.id AS jabatan_fungsional_id, i.jabatan_fungsional, i.nilai AS nilai_jabatan_fungsional, 
                      g.pengalaman AS lama_pengalaman, g.pengalaman_id, k.nilai AS nilai_pengalaman, g.id AS dosen_jenjang_id, g.jenjang_id, l.jenjang_kelompok, g.nilai_jenjang, a.record_status, m.id AS alamat_id,
                       m.jalan, m.kelurahan, m.kecamatan, m.kota, m.negara_id, m.provinsi_id, m.kode_pos, n.id AS rekening_id, n.atas_nama, n.no_rekening, n.bank_id, o.nama_bank, n.nama_bank_cabang, 
                      n.kode_bank, g.pengalaman, dbo.tbl_ref_golongan_darah.golongan_darah, dbo.tbl_ref_golongan_darah.id AS golongan_darah_id, dbo.tbl_telepon_hp.no_tlp_hp, 
                      dbo.tbl_ref_jenjang_inter.id AS jenjang_inter_id, dbo.tbl_ref_jenjang_inter.jenjang_inter, dbo.tbl_ref_honor_inter.honor_per_hadir AS honor_per_hadir_inter, a.nidn, a.status_aktif_id, rsa.status_aktif,
                       a.homebase, vdp.prodi AS homebase_prodi, vdp.jenjang AS homebase_jenjang
FROM         dbo.tbl_dosen AS a LEFT OUTER JOIN
                      dbo.tbl_pegawai AS b ON a.pegawai_id = b.id LEFT OUTER JOIN
                      dbo.tbl_ref_jenis_kelamin AS c ON b.jenis_kelamin_id = c.id LEFT OUTER JOIN
                      dbo.tbl_ref_agama AS d ON d.id = b.agama_id LEFT OUTER JOIN
                      dbo.tbl_ref_status_dosen AS e ON e.id = a.status_dosen_id LEFT OUTER JOIN
                      dbo.tbl_dosen_sertifikasi AS f ON a.id = f.dosen_id LEFT OUTER JOIN
                      dbo.tbl_dosen_jenjang AS g ON a.id = g.dosen_id LEFT OUTER JOIN
                      dbo.tbl_ref_pendidikan AS h ON h.id = g.pendidikan_id LEFT OUTER JOIN
                      dbo.tbl_ref_jabatan_fungsional AS i ON g.jabatan_fungsional_id = i.id LEFT OUTER JOIN
                      dbo.tbl_ref_pengalaman AS k ON g.pengalaman_id = k.id LEFT OUTER JOIN
                      dbo.tbl_ref_jenjang AS l ON g.jenjang_id = l.id LEFT OUTER JOIN
                      dbo.tbl_alamat AS m ON b.id = m.pegawai_id LEFT OUTER JOIN
                      dbo.tbl_rekening AS n ON b.id = n.pegawai_id LEFT OUTER JOIN
                      dbo.tbl_ref_bank AS o ON n.bank_id = o.id LEFT OUTER JOIN
                      dbo.tbl_email AS p ON p.pegawai_id = b.id LEFT OUTER JOIN
                      dbo.tbl_ref_golongan_darah ON b.golongan_darah_id = dbo.tbl_ref_golongan_darah.id LEFT OUTER JOIN
                      dbo.tbl_telepon_hp ON b.id = dbo.tbl_telepon_hp.pegawai_id LEFT OUTER JOIN
                      dbo.tbl_dosen_jenjang_inter ON a.id = dbo.tbl_dosen_jenjang_inter.dosen_id LEFT OUTER JOIN
                      dbo.tbl_ref_jenjang_inter ON dbo.tbl_dosen_jenjang_inter.jenjang_kelas_inter_id = dbo.tbl_ref_jenjang_inter.id LEFT OUTER JOIN
                      dbo.tbl_ref_honor_inter ON dbo.tbl_ref_jenjang_inter.id = dbo.tbl_ref_honor_inter.jenjang_inter_id LEFT OUTER JOIN
                      dbo.tbl_ref_status_aktif AS rsa ON rsa.id = a.status_aktif_id LEFT OUTER JOIN
                      dbo.view_daftar_prodi AS vdp ON vdp.kode_dikti = a.homebase

GO



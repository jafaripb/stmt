﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMatrix.WebData;
using SPDSTMT.Models;

namespace SPDSTMT.Filters
{
    public class MyCustomRoleProvider : SimpleRoleProvider
    {
        internal stmtsdmEntities db = new stmtsdmEntities();
        public override string[] GetRolesForUser(string username)
        {
            var VUser = db.view_user.Where(d => d.username == username).Select(d => new { d.role}).ToArray();
            string[] roles = new string[VUser.Count()];
            int i = 0;
            foreach (var item in VUser)
            {
                roles[i] = item.role;
                i++;
            }
            return roles;
        }
    }
}

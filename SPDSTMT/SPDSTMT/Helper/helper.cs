﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SPDSTMT.Helper
{
    public class helper
    {
        public static string SaveSuksesHtml()
        {
            string result = "<div class='alert alert-success alert-dismissable'>" +
                          "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>" +
                                    "<h4><i class='icon fa fa-check'></i>Berhasil</h4><p>Data Berhasil di Tambah</p></div>";

            return result;
        }

        public static string UpdateSuksesHtml()
        {
            string result = "<div class='alert alert-success alert-dismissable'>" +
                          "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>" +
                                    "<h4><i class='icon fa fa-check'></i>Berhasil</h4><p>Data Berhasil di Update</p></div>";

            return result;
        }

        public static string RemoveSuksesHtml()
        {
            string result = "<div class='alert alert-success alert-dismissable'>" +
                          "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>" +
                                    "<h4><i class='icon fa fa-check'></i>Berhasil</h4><p>Data Berhasil di Hapus</p></div>";

            return result;
        }

        public static string SaveSukses()
        {
            string result = "Data Berhasil di Tambah";

            return result;
        }

        public static string UpdateSukses()
        {
            string result = "Data Berhasil di Update";

            return result;
        }

        public static string ErrorMessageWithElement(string error)
        {
            string result = "<div  class='alert alert-warning alert-dismissable'>" +
                          "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>" +
                                    "<h4><i class='icon fa fa-warning'></i>Error</h4><p>" + error + "</p></div>";
           
            return result;
        }

        public static string messageSuksesHtml(string message)
        {
            string result = "<div class='alert alert-success alert-dismissable'>" +
                          "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>" +
                                    "<h4><i class='icon fa fa-check'></i>Berhasil</h4><p>" + message + "</p></div>";

            return result;
        }


        public static string Data()
        {
            return "Data Berhasil di Update";
        }

        private static string _ExeFileWKHTMLTOPDF = string.Empty;

        public static void WebToPDFStream(string pExeFileWKHTMLTOPDF, string pUrl, string pToDirectory, string pToFileName)
        {
            _ExeFileWKHTMLTOPDF = pExeFileWKHTMLTOPDF;

            Process p = new Process();
            string strToFilePath = pToDirectory + "\\" + pToFileName;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.FileName = pExeFileWKHTMLTOPDF;
            p.StartInfo.WorkingDirectory = pToDirectory + "\\";

            string switches = "";
            switches += "--print-media-type ";
            switches += "--margin-top 10mm --margin-bottom 10mm --margin-right 10mm --margin-left 10mm ";
            switches += "--page-size Letter ";
            p.StartInfo.Arguments = switches + " " + pUrl + " " + pToFileName;
            p.Start();
            p.WaitForExit(60000);
            int returnCode = p.ExitCode;
            p.Close();

        }

        public static void WebToPDFStreamLandScape(string pExeFileWKHTMLTOPDF, string pUrl, string pToDirectory, string pToFileName)
        {
            _ExeFileWKHTMLTOPDF = pExeFileWKHTMLTOPDF;

            Process p = new Process();
            string strToFilePath = pToDirectory + "\\" + pToFileName;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.FileName = pExeFileWKHTMLTOPDF;
            p.StartInfo.WorkingDirectory = pToDirectory + "\\";

            string switches = "-O Landscape ";
            switches += "--print-media-type ";
            switches += "--margin-top 10mm --margin-bottom 10mm --margin-right 10mm --margin-left 10mm ";
            switches += "--page-size A4 ";
            p.StartInfo.Arguments = switches + " " + pUrl + " " + pToFileName;
            p.Start();
            p.WaitForExit(60000);
            int returnCode = p.ExitCode;
            p.Close();

        }

        public static string ConvertToHarga(decimal values)
        {
            string hasil="";
            

            return hasil;
        }

        public static DateTime? ConvertDate(string date, string formatDate)
        {
            try
            {
                DateTime Date = DateTime.ParseExact(date, formatDate, CultureInfo.InvariantCulture);
                return Date;
            }
            catch {

                return null;
            }
        }
    }
    
}

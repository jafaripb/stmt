﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPDSTMT.Models
{
    public class ViewModelPegawai
    {
    }

    public class ViewPegawai
    {
        public int id { get; set; }
        public string nama { get; set; }
        public string kd_nik { get; set; }
        public string tempat_lahir { get; set; }
        public Nullable<System.DateTime> tanggal_lahir { get; set; }
        public Nullable<int> jenis_kelamin_id { get; set; }
        public string gelar_depan { get; set; }
        public string gelar_belakang { get; set; }
        public Nullable<int> agama_id { get; set; }
        public Nullable<int> golongan_darah_id { get; set; }
        public Nullable<System.DateTime> tanggal_masuk { get; set; }
        public Nullable<System.DateTime> tanggal_keluar { get; set; }
        public Nullable<System.DateTime> tanggal_sk { get; set; }
        public Nullable<byte> record_status { get; set; }
        public Nullable<System.DateTime> create_on { get; set; }
        public string create_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string modified_by { get; set; }
        public string jalan { get; set; }
        public string kelurahan { get; set; }
        public string provinsi_id { get; set; }
        public string kode_pos { get; set; }
        public string no_rekening { get; set; }
        public string atas_nama { get; set; }
        public string kode_bank { get; set; }
        public string nama_bank_cabang { get; set; }

    }
}
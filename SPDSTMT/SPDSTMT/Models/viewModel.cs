﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPDSTMT.Models
{
    public class viewModel
    {
    }
    public class DataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
    }

    public class viewHonorMatakKuliahDosen {
        public string kelas { get; set; }
        public string name_course { get; set; }
        public Nullable<int> credit_course { get; set; }
        public Nullable<double> honor_per_sks { get; set; }
        public Nullable<double> honor_tunjangan { get; set; }
        public string code_course { get; set; }
        public Nullable<int> jumlahHadir { get; set; }
        public Nullable<double> total_honor_per_kelas { get; set; }
        public Nullable<double> total_tunjangan_per_kelas { get; set; }
    }

    public class ResultMessage
    {
        public int status { get; set; }
        public string message { get; set; }
    }

    public class RefPendidikan
    {
        public int id { get; set; }
        public string tingkat_pendidikan { get; set; }
        public Nullable<int> bobot { get; set; }
        public Nullable<decimal> nilai { get; set; }
        public Nullable<System.DateTime> create_on { get; set; }
        public string create_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string modified_by { get; set; }
    }

    public class RefJabatanFungsional
    {

        public int id { get; set; }
        public string jabatan_fungsional { get; set; }
        public Nullable<int> bobot { get; set; }
        public Nullable<decimal> nilai { get; set; }
        public Nullable<System.DateTime> create_on { get; set; }
        public string create_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string modified_by { get; set; }
   }

   public partial class RefPengalaman
   {
       public int id { get; set; }
       public Nullable<int> min { get; set; }
       public Nullable<int> max { get; set; }
       public Nullable<int> bobot { get; set; }
       public Nullable<decimal> nilai { get; set; }
       public Nullable<System.DateTime> create_on { get; set; }
       public string create_by { get; set; }
       public Nullable<System.DateTime> modified_on { get; set; }
       public string modified_by { get; set; }
   }

   public partial class RefJenjang
   {
       public int id { get; set; }
       public string jenjang_kelompok { get; set; }
       public Nullable<decimal> nilai_min { get; set; }
       public Nullable<decimal> nilai_max { get; set; }
       public Nullable<System.DateTime> create_on { get; set; }
       public string create_by { get; set; }
       public Nullable<System.DateTime> modified_on { get; set; }
       public string modified_by { get; set; }
   }

   public partial class RefStatusDosen
   {

       public int id { get; set; }
       public string status_dosen { get; set; }
       public string keterangan { get; set; }
       public Nullable<System.DateTime> create_on { get; set; }
       public string create_by { get; set; }
       public Nullable<System.DateTime> modified_on { get; set; }
       public string modified_by { get; set; }
   }

   public partial class RefHonorarium
   {
       public int id { get; set; }
       public Nullable<int> jenjang_id { get; set; }
       public string jenjang_nama { get; set; }
       public Nullable<decimal> honor_per_sks { get; set; }
       public Nullable<decimal> transpor_per_hadir { get; set; }
       public Nullable<System.DateTime> create_on { get; set; }
       public string create_by { get; set; }
       public Nullable<System.DateTime> modified_on { get; set; }
       public string modified_by { get; set; }
   }

   public partial class RefHonorTunjangan
   {
       public int id { get; set; }
       public Nullable<int> status_dosen_id { get; set; }
       public string status_dosen { get; set; }       
       public Nullable<decimal> honor_tunjangan { get; set; }
       public string keterangan { get; set; }
       public Nullable<System.DateTime> create_on { get; set; }
       public string create_by { get; set; }
       public Nullable<System.DateTime> modified_on { get; set; }
       public string modified_by { get; set; }
   }

   public partial class RefHonorJenjangInter
   {
       public int jenjang_inter_id { get; set; }
       public string jenjang_inter { get; set; }
       public Nullable<decimal> honor_per_hadir { get; set; }
       public Nullable<System.DateTime> create_on { get; set; }
       public string create_by { get; set; }
       public Nullable<System.DateTime> modified_on { get; set; }
       public string modified_by { get; set; }
   }

   public partial class ViewRefBank
   {
       public int id { get; set; }
       public string nama_bank { get; set; }
       public string kode_bank { get; set; }
       public Nullable<decimal> biaya_bank { get; set; }

   }

    public class ValidasiModel
    {
        public int error { get; set; }
        public string[] arrMessage { get; set; }
    }
    public partial class ViewKehadiranDosen
    {
        public int id { get; set; }
        public Nullable<int> document_id { get; set; }
        public string code_lecturer { get; set; }
        public string name_course { get; set; }
        public string fullname_user { get; set; }
        public Nullable<int> credit_course { get; set; }
        public string kelas { get; set; }
        public string date_document { get; set; }
        public string code_lecture { get; set; }
        public string acronym_concentration { get; set; }
        public Nullable<int> prog_id { get; set; }
        public string code_course { get; set; }
        public Nullable<System.DateTime> create_on { get; set; }
        public string create_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string modified_by { get; set; }
    }
    public partial class VWDosen
    {
        public int dosen_id { get; set; }
        public string kd_nik { get; set; }
        public string nidn { get; set; }
        public string nama_dosen { get; set; }
        public string tempat_lahir { get; set; }
        public string tanggal_lahir { get; set; }
        public string tanggal_masuk { get; set; }
        public string tanggal_sk { get; set; }
        public Nullable<int> jenis_kelamin_id { get; set; }
        public string jenis_kelamin { get; set; }
        public Nullable<int> agama_id { get; set; }
        public string agama { get; set; }
        public Nullable<int> status_dosen_id { get; set; }
        public string status_dosen { get; set; }
        public Nullable<int> status_aktif_id { get; set; }
        public string status_aktif { get; set; }
        public Nullable<int> sertifikasi_id { get; set; }
        public string gelar_depan { get; set; }
        public string gelar_belakang { get; set; }
        public string nama { get; set; }
        public Nullable<int> pendidikan_id { get; set; }
        public string tingkat_pendidikan { get; set; }
        public Nullable<decimal> nilai_pendidikan { get; set; }
        public Nullable<int> jabatan_fungsional_id { get; set; }
        public string jabatan_fungsional { get; set; }
        public Nullable<decimal> nilai_jabatan_fungsional { get; set; }
        public Nullable<int> lama_pengalaman { get; set; }
        public Nullable<int> pengalaman_id { get; set; }
        public Nullable<decimal> nilai_pengalaman { get; set; }
        public Nullable<int> dosen_jenjang_id { get; set; }
        public Nullable<int> jenjang_id { get; set; }
        public string jenjang_kelompok { get; set; }
        public Nullable<decimal> nilai_jenjang { get; set; }
        public Nullable<int> record_status { get; set; }
        public Nullable<int> alamat_id { get; set; }
        public string jalan { get; set; }
        public string kelurahan { get; set; }
        public string kecamatan { get; set; }
        public string kota { get; set; }
        public string negara_id { get; set; }
        public string provinsi_id { get; set; }
        public string kode_pos { get; set; }
        public Nullable<int> rekening_id { get; set; }
        public string atas_nama { get; set; }
        public string no_rekening { get; set; }
        public Nullable<int> bank_id { get; set; }
        public string nama_bank { get; set; }
        public string nama_bank_cabang { get; set; }
        public string kode_bank { get; set; }
        public Nullable<int> pengalaman { get; set; }
        public string golongan_darah { get; set; }
        public Nullable<int> golongan_darah_id { get; set; }
        public string no_tlp_hp { get; set; }
        public string jenjang_inter { get; set; }
        public Nullable<int> jenjang_inter_id { get; set; }
        public Nullable<decimal> honor_per_hadir_inter { get; set; }

        public string homebase { get; set; }

        public List<AttributRiwayatDosen> bidang_keahlian { get; set; }
        public List<AttributRiwayatDosen> pengalaman_transport { get; set; }
        public List<AttributRiwayatDosen> pengalaman_nontrans { get; set; }
        public List<AttributRiwayatDosen> ampu_matkul { get; set; }
        public List<AttributRiwayatDosen> pelatihan { get; set; }
        public List<AttributRiwayatDosen> seminar { get; set; }
        public List<AttributRiwayatDosen> penelitian { get; set; }
    }

    public partial class VWUserRole
    {
        public Nullable<int> UserId { get; set; }
        public string  username { get; set; }
        public string password { get; set; }
        public Nullable<int> roleId { get; set; }
        public string role { get; set; }
    }

    public class v_riwayat_pendidikan
    {
        public Nullable<int> id { get; set; }
        public string jenjang { get; set; }
        public string nama_institusi { get; set; }
        public string program_studi { get; set; }
        public string gelar { get; set; }
        public bool delete { get; set; }

        public Nullable<System.DateTime> create_on { get; set; }
        public string create_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string modified_by { get; set; }
    }

    public class AttributRiwayatDosen {
        public int id { get; set; }
        public string keterangan { get; set; }
        public bool deleted { get; set; }
    }
}

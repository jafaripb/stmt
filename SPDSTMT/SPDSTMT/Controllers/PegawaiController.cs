﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPDSTMT.Helper;
using SPDSTMT.Models;

namespace SPDSTMT.Controllers
{
    public class PegawaiController : BaseController
    {
        //
        // GET: /Default1/

        public ActionResult Index()
        {
            var tbl_pegawai = db.tbl_pegawai.Include(t => t.tbl_ref_agama).Include(t => t.tbl_ref_golongan_darah).Include(t => t.tbl_ref_jenis_kelamin);
            return View(tbl_pegawai.ToList());
        }

        public class DataTableDataViewPegawai : DataTableData
        {
            public List<view_pegawai> data { get; set; }
        }

        public JsonResult GetListPegawai(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string NamaPegwai = System.Web.HttpContext.Current.Request["nama_pegawai"].ToString();
            DataTableDataViewPegawai dt = new DataTableDataViewPegawai();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities())
            {
                var a = db.view_pegawai.AsQueryable();
                dt.recordsTotal = a.Count();
                if (NamaPegwai != string.Empty) a = a.Where(x => x.nama_pegawai.Contains(NamaPegwai));
                if (sortColumn == 0)
                {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.id);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.id);
                }
                if (sortColumn == 1)
                {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.nama_pegawai);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_pegawai);
                }
                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Default1/Details/5

        public ActionResult Details(int id = 0)
        {
            tbl_pegawai tbl_pegawai = db.tbl_pegawai.Find(id);
            if (tbl_pegawai == null)
            {
                return HttpNotFound();
            }
            return View(tbl_pegawai);
        }

        //
        // GET: /Default1/Create

        public ActionResult Create()
        {
            ViewBag.agama_id = new SelectList(db.tbl_ref_agama, "id", "agama");
            ViewBag.golongan_darah_id = new SelectList(db.tbl_ref_golongan_darah, "id", "golongan_darah");
            ViewBag.jenis_kelamin_id = new SelectList(db.tbl_ref_jenis_kelamin, "id", "kode");
            return View();
        }

        //
        // POST: /Default1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_pegawai tbl_pegawai)
        {
            if (ModelState.IsValid)
            {
                db.tbl_pegawai.Add(tbl_pegawai);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.agama_id = new SelectList(db.tbl_ref_agama, "id", "agama", tbl_pegawai.agama_id);
            ViewBag.golongan_darah_id = new SelectList(db.tbl_ref_golongan_darah, "id", "golongan_darah", tbl_pegawai.golongan_darah_id);
            ViewBag.jenis_kelamin_id = new SelectList(db.tbl_ref_jenis_kelamin, "id", "kode", tbl_pegawai.jenis_kelamin_id);
            return View(tbl_pegawai);
        }

        //
        // GET: /Default1/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tbl_pegawai tbl_pegawai = db.tbl_pegawai.Find(id);
            if (tbl_pegawai == null)
            {
                return HttpNotFound();
            }


            ViewBag.SelectAgama = new SelectList(db.tbl_ref_agama, "id", "agama", tbl_pegawai.agama_id);
            ViewBag.SelectGolonganDarah = new SelectList(db.tbl_ref_golongan_darah, "id", "golongan_darah", tbl_pegawai.golongan_darah_id);
            ViewBag.SelectJenisKelamin = new SelectList(db.tbl_ref_jenis_kelamin, "id", "jenis_kelamin", tbl_pegawai.jenis_kelamin_id);
            int? zz = null;
            if (tbl_pegawai.tbl_rekening.FirstOrDefault() != null)
                if (tbl_pegawai.tbl_rekening.FirstOrDefault().bank_id != null) 
                zz = tbl_pegawai.tbl_rekening.FirstOrDefault().tbl_ref_bank.id;
            ViewBag.SelectBank = new SelectList(db.tbl_ref_bank, "id", "nama_bank", zz);
            return View(tbl_pegawai);
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_pegawai tbl_pegawai)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_pegawai).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.agama_id = new SelectList(db.tbl_ref_agama, "id", "agama", tbl_pegawai.agama_id);
            ViewBag.golongan_darah_id = new SelectList(db.tbl_ref_golongan_darah, "id", "golongan_darah", tbl_pegawai.golongan_darah_id);
            ViewBag.jenis_kelamin_id = new SelectList(db.tbl_ref_jenis_kelamin, "id", "kode", tbl_pegawai.jenis_kelamin_id);
            return View(tbl_pegawai);
        }

        [HttpPost]
        public ActionResult AjaxEdit(tbl_pegawai tbl_pegawai)
        {
            var shit = Request["tbl_pegawai"];
            ResultMessage result = new ResultMessage();
            int status = 0;
            string message = "";
            DateTime dtNow=DateTime.Now;
            try
            {
                if (ModelState.IsValid)
                {
                    using (var db2 = new stmtsdmEntities())
                    {
                        tbl_pegawai tblPegawai = db2.tbl_pegawai.Where(d => d.id == tbl_pegawai.id).FirstOrDefault();

                        tblPegawai.gelar_depan = tbl_pegawai.gelar_depan;
                        tblPegawai.nama = tbl_pegawai.nama;
                        tblPegawai.gelar_belakang = tbl_pegawai.gelar_belakang;
                        tblPegawai.agama_id = tbl_pegawai.agama_id;
                        tblPegawai.golongan_darah_id = tbl_pegawai.golongan_darah_id;
                        tblPegawai.jenis_kelamin_id = tbl_pegawai.jenis_kelamin_id;
                        tblPegawai.tanggal_lahir = tbl_pegawai.tanggal_lahir;
                        tblPegawai.tempat_lahir = tbl_pegawai.tempat_lahir;
                        tblPegawai.modified_by = user;
                        tblPegawai.modified_on = DateTime.Now;

                        if (tblPegawai.tbl_rekening != null)
                        {
                            tblPegawai.tbl_rekening.FirstOrDefault().no_rekening = tbl_pegawai.tbl_rekening.FirstOrDefault().no_rekening;
                            tblPegawai.tbl_rekening.FirstOrDefault().atas_nama = tbl_pegawai.tbl_rekening.FirstOrDefault().atas_nama;
                            tblPegawai.tbl_rekening.FirstOrDefault().bank_id = tbl_pegawai.tbl_rekening.FirstOrDefault().bank_id;
                            tblPegawai.tbl_rekening.FirstOrDefault().nama_bank_cabang = tbl_pegawai.tbl_rekening.FirstOrDefault().nama_bank_cabang;
                            tblPegawai.tbl_rekening.FirstOrDefault().modified_by = user;
                            tblPegawai.tbl_rekening.FirstOrDefault().modified_on = dtNow;
                        }
                        else
                        {
                            tbl_rekening tblRekening = new tbl_rekening();

                            tblRekening.no_rekening = tbl_pegawai.tbl_rekening.FirstOrDefault().no_rekening;
                            tblRekening.atas_nama = tbl_pegawai.tbl_rekening.FirstOrDefault().atas_nama;
                            tblRekening.bank_id = tbl_pegawai.tbl_rekening.FirstOrDefault().bank_id;
                            tblRekening.nama_bank_cabang = tbl_pegawai.tbl_rekening.FirstOrDefault().nama_bank_cabang;
                            tblRekening.create_by = user;
                            tblRekening.create_on = dtNow;
                            tblPegawai.tbl_rekening.Add(tblRekening);
                        }

                        if (tblPegawai.tbl_telepon_hp != null)
                        {
                            tblPegawai.tbl_telepon_hp.FirstOrDefault().no_tlp_hp = tbl_pegawai.tbl_telepon_hp.FirstOrDefault().no_tlp_hp;
                            tblPegawai.tbl_telepon_hp.FirstOrDefault().modified_by = user;
                            tblPegawai.tbl_telepon_hp.FirstOrDefault().modified_on = dtNow;
                        }
                        else
                        {
                            tbl_telepon_hp tblTelpon = new tbl_telepon_hp();

                            tblTelpon.no_tlp_hp = tbl_pegawai.tbl_telepon_hp.FirstOrDefault().no_tlp_hp;
                            tblTelpon.create_by = user;
                            tblTelpon.create_on = dtNow;
                            tblPegawai.tbl_telepon_hp.Add(tblTelpon);
                        }

                        if (tblPegawai.tbl_alamat != null)
                        {
                            tblPegawai.tbl_alamat.FirstOrDefault().jalan = tbl_pegawai.tbl_alamat.FirstOrDefault().jalan;
                            tblPegawai.tbl_alamat.FirstOrDefault().kelurahan = tbl_pegawai.tbl_alamat.FirstOrDefault().kelurahan;
                            tblPegawai.tbl_alamat.FirstOrDefault().kecamatan = tbl_pegawai.tbl_alamat.FirstOrDefault().kecamatan;
                            tblPegawai.tbl_alamat.FirstOrDefault().provinsi_id = tbl_pegawai.tbl_alamat.FirstOrDefault().provinsi_id;
                            tblPegawai.tbl_alamat.FirstOrDefault().kode_pos = tbl_pegawai.tbl_alamat.FirstOrDefault().kode_pos;
                            tblPegawai.tbl_alamat.FirstOrDefault().modified_by = user;
                            tblPegawai.tbl_alamat.FirstOrDefault().modified_on = dtNow;
                        }
                        else
                        {
                            tbl_alamat tblAlamat = new tbl_alamat();

                            tblAlamat.jalan = tbl_pegawai.tbl_alamat.FirstOrDefault().jalan;
                            tblAlamat.kelurahan = tbl_pegawai.tbl_alamat.FirstOrDefault().kelurahan;
                            tblAlamat.kecamatan = tbl_pegawai.tbl_alamat.FirstOrDefault().kecamatan;
                            tblAlamat.provinsi_id = tbl_pegawai.tbl_alamat.FirstOrDefault().provinsi_id;
                            tblAlamat.kode_pos = tbl_pegawai.tbl_alamat.FirstOrDefault().kode_pos;
                            tblAlamat.create_by = user;
                            tblAlamat.create_on = dtNow;
                            tblPegawai.tbl_alamat.Add(tblAlamat);
                        }

                        if (tblPegawai.tbl_rekening != null)
                        {
                            tblPegawai.tbl_rekening.FirstOrDefault().no_rekening = tbl_pegawai.tbl_rekening.FirstOrDefault().no_rekening;
                            tblPegawai.tbl_rekening.FirstOrDefault().atas_nama = tbl_pegawai.tbl_rekening.FirstOrDefault().atas_nama;
                            tblPegawai.tbl_rekening.FirstOrDefault().bank_id = tbl_pegawai.tbl_rekening.FirstOrDefault().bank_id;
                            tblPegawai.tbl_rekening.FirstOrDefault().nama_bank_cabang = tbl_pegawai.tbl_rekening.FirstOrDefault().nama_bank_cabang;
                            tblPegawai.tbl_alamat.FirstOrDefault().modified_by = user;
                            tblPegawai.tbl_alamat.FirstOrDefault().modified_on = dtNow;
                        }
                        else
                        {
                            tbl_alamat tblAlamat = new tbl_alamat();

                            tblAlamat.jalan = tbl_pegawai.tbl_alamat.FirstOrDefault().jalan;
                            tblAlamat.kelurahan = tbl_pegawai.tbl_alamat.FirstOrDefault().kelurahan;
                            tblAlamat.kecamatan = tbl_pegawai.tbl_alamat.FirstOrDefault().kecamatan;
                            tblAlamat.provinsi_id = tbl_pegawai.tbl_alamat.FirstOrDefault().provinsi_id;
                            tblAlamat.kode_pos = tbl_pegawai.tbl_alamat.FirstOrDefault().kode_pos;
                            tblAlamat.create_by = user;
                            tblAlamat.create_on = dtNow;
                            tblPegawai.tbl_alamat.Add(tblAlamat);
                        }
                        db2.Entry(tblPegawai).State = EntityState.Modified;
                        db2.SaveChanges();
                    }
                    
                    status = 1;
                    message = helper.UpdateSuksesHtml();
                }
            }
            catch (Exception ex) { status = 0; message = helper.ErrorMessageWithElement(ex.ToString()); }
            finally {
                result.status = status;
                result.message = message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxEditDetails(tbl_pegawai tbl_pegawai)
        {
            var shit = Request["tbl_pegawai"];
            ResultMessage result = new ResultMessage();
            int status = 0;
            string message = "";
            DateTime dtNow = DateTime.Now;
            try
            {
                if (ModelState.IsValid)
                {
                    using (var db2 = new stmtsdmEntities())
                    {
                        tbl_pegawai tblPegawai = db2.tbl_pegawai.Where(d => d.id == tbl_pegawai.id).FirstOrDefault();

                        tblPegawai.gelar_depan = tbl_pegawai.gelar_depan;
                        tblPegawai.nama = tbl_pegawai.nama;
                        tblPegawai.gelar_belakang = tbl_pegawai.gelar_belakang;
                        tblPegawai.agama_id = tbl_pegawai.agama_id;
                        tblPegawai.golongan_darah_id = tbl_pegawai.golongan_darah_id;
                        tblPegawai.jenis_kelamin_id = tbl_pegawai.jenis_kelamin_id;
                        tblPegawai.tanggal_lahir = tbl_pegawai.tanggal_lahir;
                        tblPegawai.tempat_lahir = tbl_pegawai.tempat_lahir;
                        tbl_pegawai.modified_by = user;
                        tbl_pegawai.modified_on = DateTime.Now;

                        foreach (var item in tbl_pegawai.tbl_telepon_hp)
                        {
                            if (item.id > 0)
                            {
                                item.modified_by = user;
                                item.modified_on = dtNow;
                            }
                            else
                            {
                                item.create_by = user;
                                item.create_on = dtNow;
                                tblPegawai.tbl_telepon_hp.Add(item);
                            }
                        }

                        foreach (var item in tblPegawai.tbl_telepon_hp)
                        {
                            foreach (var itembaru in tbl_pegawai.tbl_telepon_hp)
                            {
                                if (item.id == itembaru.id)
                                {
                                    item.no_tlp_hp = itembaru.no_tlp_hp;
                                }
                            }
                        }
                        //tblPegawai.tbl_telepon_hp = tbl_pegawai.tbl_telepon_hp;

                        foreach (var item in tbl_pegawai.tbl_alamat)
                        {

                            if (item.id > 0)
                            {
                                item.modified_by = user;
                                item.modified_on = dtNow;
                            }
                            else
                            {
                                item.create_by = user;
                                item.create_on = dtNow;
                                tblPegawai.tbl_alamat.Add(item);
                            }
                        }
                        foreach (var item in tblPegawai.tbl_alamat)
                        {
                            foreach (var itembaru in tbl_pegawai.tbl_alamat)
                            {
                                if (item.id == itembaru.id)
                                {
                                    item.jalan = itembaru.jalan;
                                    item.kelurahan = itembaru.kelurahan;
                                    item.kecamatan = itembaru.kecamatan;
                                    item.provinsi_id = itembaru.provinsi_id;
                                    item.kode_pos = itembaru.kode_pos;
                                }
                            }
                        }
                        //tblPegawai.tbl_alamat = tbl_pegawai.tbl_alamat;

                        foreach (var item in tbl_pegawai.tbl_rekening)
                        {
                            if (item.id > 0)
                            {
                                item.modified_by = user;
                                item.modified_on = dtNow;
                            }
                            else
                            {
                                item.create_by = user;
                                item.create_on = dtNow;
                                tblPegawai.tbl_rekening.Add(item);
                            }
                        }
                        foreach (var item in tblPegawai.tbl_rekening)
                        {
                            foreach (var itembaru in tbl_pegawai.tbl_rekening)
                            {
                                if (item.id == itembaru.id)
                                {
                                    item.no_rekening = itembaru.no_rekening;
                                    item.atas_nama = itembaru.atas_nama;
                                    item.bank_id = itembaru.bank_id;
                                    item.nama_bank_cabang = itembaru.nama_bank_cabang;
                                }
                            }
                        }
                        //tblPegawai.tbl_rekening = tbl_pegawai.tbl_rekening;

                        //db2.SaveChanges();
                        //tbl_telepon_hp.no_tlp_hp 
                        db2.Entry(tblPegawai).State = EntityState.Modified;
                        db2.SaveChanges();
                    }

                    status = 1;
                    message = helper.UpdateSuksesHtml();
                }
            }
            catch (Exception ex) { status = 0; message = helper.ErrorMessageWithElement(ex.ToString()); }
            finally
            {
                result.status = status;
                result.message = message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Default1/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tbl_pegawai tbl_pegawai = db.tbl_pegawai.Find(id);
            if (tbl_pegawai == null)
            {
                return HttpNotFound();
            }
            return View(tbl_pegawai);
        }

        //
        // POST: /Default1/Delete/5

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    tbl_pegawai tbl_pegawai = db.tbl_pegawai.Find(id);
        //    db.tbl_pegawai.Remove(tbl_pegawai);
        //    //db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        public JsonResult DeleteConfirmed(int id)
        {
            ResultMessage resultMessage=new ResultMessage();
            string message = "";
            int status = 0;
            try
            {
                tbl_pegawai tbl_pegawai = db.tbl_pegawai.Find(id);
                tbl_pegawai.record_status = 0;
                db.SaveChanges();
                message=helper.RemoveSuksesHtml();
                status = 1;
            }
            catch (Exception ex) { status = -1; message = ex.ToString(); }
            finally { resultMessage.message = message; resultMessage.status = status; }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}




//tbl_rekening tblRekening = db2.tbl_rekening.Where(d => d.id == tbl_pegawai.id).FirstOrDefault();
////int cektblRekening = 0;
////if (tblRekening != null) cektblRekening = 1;
/////if (cektblRekening == 1) tblPegawai.tbl_rekening.FirstOrDefault().id = tblRekening.id;
/////tblRekening.pegawai_id = tbl_pegawai.id;
////tblRekening.kode_bank = tbl_pegawai.tbl_rekening.FirstOrDefault().kode_bank;
////tblRekening.no_rekening = tbl_pegawai.tbl_rekening.FirstOrDefault().no_rekening;
////tblRekening.atas_nama = tbl_pegawai.tbl_rekening.FirstOrDefault().atas_nama;
////tblRekening.nama_bank_cabang = tbl_pegawai.tbl_rekening.FirstOrDefault().nama_bank_cabang;
////if (cektblRekening == 0) db2.tbl_rekening.Add(tblRekening);
/////db2.SaveChanges();

//tbl_telepon_hp tblTelponHp = db2.tbl_telepon_hp.Where(d => d.id == tbl_pegawai.id).FirstOrDefault();
//int cektblTelponHp = 0;
//if (tblTelponHp != null) cektblTelponHp = 1;
//if (cektblTelponHp == 0) tblTelponHp = new tbl_telepon_hp();
//tblTelponHp.pegawai_id = tbl_pegawai.id;
//tblTelponHp.no_tlp_hp = tbl_pegawai.tbl_telepon_hp.FirstOrDefault().no_tlp_hp;
//if (cektblTelponHp == 0) db2.tbl_telepon_hp.Add(tblTelponHp);
/////db2.SaveChanges();

//tbl_alamat tblAlamat = db2.tbl_alamat.Where(d => d.id == tbl_pegawai.id).FirstOrDefault();
//int cektbltblAlamat = 0;
//if (tblAlamat != null) cektbltblAlamat = 1;
//if (cektbltblAlamat == 0) tblAlamat = new tbl_alamat();
//tblAlamat.pegawai_id = tbl_pegawai.id;
//tblAlamat.jalan = tbl_pegawai.tbl_alamat.FirstOrDefault().jalan;
//tblAlamat.kelurahan = tbl_pegawai.tbl_alamat.FirstOrDefault().kelurahan;
//tblAlamat.kecamatan = tbl_pegawai.tbl_alamat.FirstOrDefault().kecamatan;
//tblAlamat.provinsi_id = tbl_pegawai.tbl_alamat.FirstOrDefault().provinsi_id;
//tblAlamat.kode_pos = tbl_pegawai.tbl_alamat.FirstOrDefault().kode_pos;
//if (cektbltblAlamat == 0) db2.tbl_alamat.Add(tblAlamat);
////db2.SaveChanges();
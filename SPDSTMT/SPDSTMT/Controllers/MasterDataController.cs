﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPDSTMT.Models;
using SPDSTMT.Helper;
using SPDSTMT.Filters;
using System.Globalization;

namespace SPDSTMT.Controllers
{
    public class MasterDataController : BaseController
    {
        // [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            //ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public ActionResult Dosen()
        {
            // ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }
        [Authorize]
        public ActionResult CreateDosen()
        {
            ViewBag.SelectAgama = new SelectList(db.tbl_ref_agama, "id", "agama");
            ViewBag.SelectGolonganDarah = new SelectList(db.tbl_ref_golongan_darah, "id", "golongan_darah");
            ViewBag.SelectJenisKelamin = new SelectList(db.tbl_ref_jenis_kelamin, "id", "jenis_kelamin");
            ViewBag.SelectBank = new SelectList(db.tbl_ref_bank, "id", "nama_bank");
            ViewBag.Pendidikan = new SelectList(db.tbl_ref_pendidikan, "id", "tingkat_pendidikan");
            ViewBag.JabatanFungsional = new SelectList(db.tbl_ref_jabatan_fungsional, "id", "jabatan_fungsional");
            ViewBag.Jenjang = new SelectList(db.tbl_ref_jenjang, "id", "jenjang_kelompok");
            ViewBag.StatusDosen = new SelectList(db.tbl_ref_status_dosen, "id", "status_dosen");
            ViewBag.StatusAktif = new SelectList(db.tbl_ref_status_aktif, "id", "status_aktif");

            ViewBag.DaftarProdi = new SelectList(db.view_daftar_prodi.Select(a => new { a.kode_dikti, label = a.jenjang + " - " + a.prodi }), "kode_dikti", "label");
            // ViewBag.baseUrl = baseUrl;
            return View();
        }
        [Authorize]
        public ActionResult EditDosen(int id = 0)
        {
            view_dosen ViewDosen = db.view_dosen.Where(d => d.dosen_id == id).FirstOrDefault();
            if (ViewDosen == null) {
                return HttpNotFound();
            }

            ViewBag.SelectAgama = new SelectList(db.tbl_ref_agama, "id", "agama", ViewDosen.agama_id);
            ViewBag.SelectGolonganDarah = new SelectList(db.tbl_ref_golongan_darah, "id", "golongan_darah", ViewDosen.golongan_darah_id);
            ViewBag.SelectJenisKelamin = new SelectList(db.tbl_ref_jenis_kelamin, "id", "jenis_kelamin", ViewDosen.jenis_kelamin_id);
            ViewBag.SelectBank = new SelectList(db.tbl_ref_bank, "id", "nama_bank", ViewDosen.bank_id);
            ViewBag.Pendidikan = new SelectList(db.tbl_ref_pendidikan, "id", "tingkat_pendidikan", ViewDosen.pendidikan_id);
            ViewBag.JabatanFungsional = new SelectList(db.tbl_ref_jabatan_fungsional, "id", "jabatan_fungsional", ViewDosen.jabatan_fungsional_id);
            ViewBag.Jenjang = new SelectList(db.tbl_ref_jenjang, "id", "jenjang_kelompok", ViewDosen.jenjang_id);
            ViewBag.StatusDosen = new SelectList(db.tbl_ref_status_dosen, "id", "status_dosen", ViewDosen.status_dosen_id);
            ViewBag.KelompokInter = new SelectList(db.tbl_ref_jenjang_inter, "id", "jenjang_inter", ViewDosen.jenjang_inter_id);
            ViewBag.StatusAktif = new SelectList(db.tbl_ref_status_aktif, "id", "status_aktif");

            ViewBag.DaftarProdi = new SelectList(db.view_daftar_prodi.Select(a => new { a.kode_dikti, label = a.jenjang + " - " + a.prodi }), "kode_dikti", "label");

            // ViewBag.baseUrl = baseUrl;

            //baca keterangan riwayat dosen
            ViewBag.bidang_keahlian = db.tbl_bidang_keahlian.Where(a => a.dosen_id == ViewDosen.dosen_id).Select(a => new AttributRiwayatDosen {
                id = a.id,
                keterangan = a.bidang_keahlian
            }).ToList();

            ViewBag.pengalaman_transport = db.tbl_pengalaman_bdg_transport.Where(a => a.dosen_id == ViewDosen.dosen_id).Select(a => new AttributRiwayatDosen {
                id = a.id,
                keterangan = a.bidang_keahlian
            }).ToList();

            ViewBag.pengalaman_nontrans = db.tbl_pengalaman_bdg_nontrans.Where(a => a.dosen_id == ViewDosen.dosen_id).Select(a => new AttributRiwayatDosen {
                id = a.id,
                keterangan = a.bidang_keahlian
            }).ToList();

            ViewBag.ampu_matkul = db.tbl_ampu_matkul.Where(a => a.dosen_id == ViewDosen.dosen_id).Select(a => new AttributRiwayatDosen {
                id = a.id,
                keterangan = a.mata_kuliah
            }).ToList();

            ViewBag.pelatihan = db.tbl_pelatihan_dosen.Where(a => a.dosen_id == ViewDosen.dosen_id).Select(a => new AttributRiwayatDosen {
                id = a.id,
                keterangan = a.pelatihan
            }).ToList();

            ViewBag.penelitian = db.tbl_penelitian_dosen.Where(a => a.dosen_id == ViewDosen.dosen_id).Select(a => new AttributRiwayatDosen {
                id = a.id,
                keterangan = a.penelitian
            }).ToList();

            ViewBag.seminar = db.tbl_seminar_dosen.Where(a => a.dosen_id == ViewDosen.dosen_id).Select(a => new AttributRiwayatDosen {
                id = a.id,
                keterangan = a.seminar
            }).ToList();

            return View(ViewDosen);
        }
        [Authorize]
        public ActionResult Jenjang()
        {
            //ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public ActionResult RefData()
        {
            /// ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public class DataTableDataVDosen : DataTableData
        {
            public List<view_dosen> data { get; set; }
        }

        [HttpPost]
        public JsonResult GetListDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string KdNik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string NamaDosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
            string StatusDosen = System.Web.HttpContext.Current.Request["status_dosen"].ToString();

            List<view_dosen> lstMhs = new List<view_dosen>();
            DataTableDataVDosen dt = new DataTableDataVDosen();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            // note: we only sort one column at a time
            // if (Request.QueryString["order[0][column]"] != null)
            // {

            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            // }
            //if (Request.QueryString["order[0][dir]"] != null)
            //{
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                var a = db.view_dosen.Where(d => d.record_status == 1).AsQueryable();
                dt.recordsTotal = a.Count();
                if (KdNik != string.Empty) a = a.Where(x => x.kd_nik.Contains(KdNik));
                if (NamaDosen != string.Empty) a = a.Where(x => x.nama_dosen.Contains(NamaDosen));
                if (StatusDosen != string.Empty) {
                    int statusDosenId = Convert.ToInt32(StatusDosen);
                    a = a.Where(x => x.status_dosen_id == statusDosenId);
                }
                if (sortColumn == 1) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik);
                }
                if (sortColumn == 2) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.nama_dosen);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_dosen);
                }

                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public class DataTableDataVJenjangDosen : DataTableData
        {
            public List<view_jenjang_dosen> data { get; set; }
        }

        public JsonResult GetListJenjangDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string KdNik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string NamaDosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
            string StatusDosen = System.Web.HttpContext.Current.Request["status_dosen"].ToString();
            List<view_jenjang_dosen> lstMhs = new List<view_jenjang_dosen>();
            DataTableDataVJenjangDosen dt = new DataTableDataVJenjangDosen();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            // note: we only sort one column at a time
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());

            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            using (var db = new stmtsdmEntities()) {
                var a = db.view_jenjang_dosen.AsQueryable();
                dt.recordsTotal = a.Count();
                if (KdNik != string.Empty) a = a.Where(x => x.kd_nik.Contains(KdNik));
                if (NamaDosen != string.Empty) a = a.Where(x => x.nama_dosen.Contains(NamaDosen));
                if (StatusDosen != string.Empty) a = a.Where(x => x.status_dosen.Contains(StatusDosen));
                if (sortColumn == 1) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik);
                }
                if (sortColumn == 2) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.nama_dosen);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_dosen);
                }

                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public JsonResult AjaxAddDosen(VWDosen vdosen)
        {
            string message = "";
            int status = 0;
            try {
                using (var db = new stmtsdmEntities()) {
                    DateTime Now = DateTime.Now;
                    if (db.view_dosen.Where(d => d.kd_nik == vdosen.kd_nik).FirstOrDefault() != null) {
                        message = helper.ErrorMessageWithElement("Kode NIK sudah ada");
                    } else {
                        tbl_pegawai Pegawai = new tbl_pegawai();
                        Pegawai.agama_id = vdosen.agama_id;
                        Pegawai.gelar_depan = vdosen.gelar_depan;
                        Pegawai.gelar_belakang = vdosen.gelar_belakang;
                        Pegawai.tempat_lahir = vdosen.tempat_lahir;
                        Pegawai.tanggal_lahir = helper.ConvertDate(vdosen.tanggal_lahir, "dd/MM/yyyy"); ;
                        Pegawai.tanggal_masuk = helper.ConvertDate(vdosen.tanggal_masuk, "dd/MM/yyyy");
                        Pegawai.nama = vdosen.nama;
                        Pegawai.gelar_belakang = vdosen.gelar_belakang;
                        Pegawai.gelar_depan = vdosen.gelar_depan;
                        Pegawai.jenis_kelamin_id = vdosen.jenis_kelamin_id;
                        Pegawai.golongan_darah_id = vdosen.golongan_darah_id;
                        Pegawai.tanggal_sk = helper.ConvertDate(vdosen.tanggal_sk, "dd/MM/yyyy");
                        Pegawai.create_on = Now;
                        Pegawai.create_by = user;
                        tbl_rekening TblRekening = new tbl_rekening();

                        TblRekening.no_rekening = vdosen.no_rekening;
                        TblRekening.atas_nama = vdosen.atas_nama;
                        TblRekening.bank_id = vdosen.bank_id;
                        TblRekening.kode_bank = vdosen.kode_bank;
                        TblRekening.create_by = user;
                        TblRekening.create_on = Now;
                        Pegawai.tbl_rekening.Add(TblRekening);

                        tbl_alamat TblAlamat = new tbl_alamat();
                        TblAlamat.jalan = vdosen.jalan;
                        TblAlamat.kelurahan = vdosen.kelurahan;
                        TblAlamat.kecamatan = vdosen.kecamatan;
                        TblAlamat.kota = vdosen.kota;
                        TblAlamat.provinsi_id = vdosen.provinsi_id;
                        TblAlamat.kode_pos = vdosen.kode_pos;
                        TblAlamat.create_by = user;
                        TblAlamat.create_on = Now;
                        Pegawai.tbl_alamat.Add(TblAlamat);

                        tbl_telepon_hp TblTelponHp = new tbl_telepon_hp();
                        TblTelponHp.no_tlp_hp = vdosen.no_tlp_hp;
                        TblTelponHp.create_by = user;
                        TblTelponHp.create_on = Now;

                        db.tbl_pegawai.Add(Pegawai);
                        db.SaveChanges();

                        decimal NilaiPendidikan = db.tbl_ref_pendidikan.Where(d => d.id == vdosen.pendidikan_id).FirstOrDefault().nilai.Value;
                        decimal NilaiJabatanFungsional = db.tbl_ref_jabatan_fungsional.Where(d => d.id == vdosen.jabatan_fungsional_id).FirstOrDefault().nilai.Value;
                        //int LamaPengalaman = vdosen.lama_pengalaman.Value;
                        int lama_pengalaman = 0;
                        DateTime dateNow = DateTime.Now;
                        if (Pegawai.tanggal_sk != null) {
                            DateTime TanggalSK = Pegawai.tanggal_sk.Value;
                            int TglSkTahun = TanggalSK.Year;
                            int TglSKBulan = TanggalSK.Month;

                            int TglNowTahun = dateNow.Year;
                            int TglNowBulan = dateNow.Month;

                            int selishBulan = ((TglNowTahun - TglSkTahun) * 12) + (TglNowBulan - TglSKBulan);


                            lama_pengalaman = selishBulan / 12;//VJenjangDosen.pengalaman.Value
                        }
                        decimal NilaiPengalaman = 0;
                        var a = db.tbl_ref_pengalaman.Where(d => d.min <= lama_pengalaman && d.max >= lama_pengalaman).FirstOrDefault();
                        if (a != null) {
                            NilaiPengalaman = a.nilai.Value;
                        }

                        decimal NilaiJenjang = NilaiPendidikan + NilaiJabatanFungsional + NilaiPengalaman;

                        tbl_ref_jenjang QJenjang = new tbl_ref_jenjang();
                        QJenjang = db.tbl_ref_jenjang.Where(d => d.nilai_min <= NilaiJenjang && d.nilai_max >= NilaiJenjang).FirstOrDefault();

                        tbl_dosen Dosen = new tbl_dosen();
                        Dosen.kd_nik = vdosen.kd_nik;
                        Dosen.pegawai_id = Pegawai.id;
                        Dosen.status_dosen_id = vdosen.status_dosen_id;
                        Dosen.nidn = vdosen.nidn;
                        Dosen.status_aktif_id = vdosen.status_aktif_id;
                        Dosen.record_status = 1;
                        Dosen.homebase = vdosen.homebase;
                        Dosen.create_on = Now;
                        Dosen.create_by = user;

                        tbl_dosen_jenjang DosenJenjang = new tbl_dosen_jenjang();
                        DosenJenjang.dosen_id = Dosen.id;
                        DosenJenjang.pendidikan_id = vdosen.pendidikan_id;
                        DosenJenjang.jabatan_fungsional_id = vdosen.jabatan_fungsional_id;
                        DosenJenjang.nilai_jenjang = NilaiJenjang;
                        DosenJenjang.jenjang_id = QJenjang.id;
                        DosenJenjang.pengalaman_id = a.id;
                        DosenJenjang.create_on = Now;
                        DosenJenjang.create_by = user;
                        Dosen.tbl_dosen_jenjang.Add(DosenJenjang);
                        db.tbl_dosen.Add(Dosen);
                        db.SaveChanges();

                        tbl_dosen_jenjang_inter NewDosenJenjangInter = new tbl_dosen_jenjang_inter();
                        NewDosenJenjangInter.dosen_id = Dosen.id;
                        NewDosenJenjangInter.jenjang_kelas_inter_id = vdosen.jenjang_inter_id;
                        db.tbl_dosen_jenjang_inter.Add(NewDosenJenjangInter);
                        db.SaveChanges();
                        status = 1;
                        message = helper.SaveSuksesHtml();
                    }
                }


            } catch (Exception ex) { message = helper.ErrorMessageWithElement(ex.ToString()); status = 0; } finally { resultMessage.message = message; resultMessage.status = status; }

            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public JsonResult AjaxEditDosen(VWDosen vdosen)
        {
            string message = "";
            int status = 0;
            try {
                using (var db = new stmtsdmEntities()) {
                    DateTime Now = DateTime.Now;

                    tbl_dosen Dosen = db.tbl_dosen.Where(d => d.id == vdosen.dosen_id).FirstOrDefault();
                    if (Dosen == null) {
                        message = helper.ErrorMessageWithElement("Dosen Tidak Ada");
                        resultMessage.message = message;
                        resultMessage.status = status;
                        return Json(resultMessage, JsonRequestBehavior.AllowGet);
                    }
                    tbl_pegawai Pegawai = db.tbl_pegawai.Where(d => d.id == Dosen.pegawai_id).FirstOrDefault();
                    Pegawai.agama_id = vdosen.agama_id;
                    Pegawai.gelar_depan = vdosen.gelar_depan;
                    Pegawai.gelar_belakang = vdosen.gelar_belakang;
                    Pegawai.tempat_lahir = vdosen.tempat_lahir;
                    Pegawai.tanggal_lahir = helper.ConvertDate(vdosen.tanggal_lahir, "dd/MM/yyyy");
                    Pegawai.tanggal_masuk = helper.ConvertDate(vdosen.tanggal_masuk, "dd/MM/yyyy");
                    Pegawai.tanggal_sk = helper.ConvertDate(vdosen.tanggal_sk, "dd/MM/yyyy");
                    Pegawai.nama = vdosen.nama;
                    Pegawai.gelar_belakang = vdosen.gelar_belakang;
                    Pegawai.gelar_depan = vdosen.gelar_depan;
                    Pegawai.jenis_kelamin_id = vdosen.jenis_kelamin_id;
                    Pegawai.golongan_darah_id = vdosen.golongan_darah_id;
                    Pegawai.modified_on = Now;
                    //if (Pegawai.id > 0) db.SaveChanges();
                    if (Pegawai.tbl_rekening.FirstOrDefault() != null) {
                        Pegawai.tbl_rekening.FirstOrDefault().no_rekening = vdosen.no_rekening;
                        Pegawai.tbl_rekening.FirstOrDefault().atas_nama = vdosen.atas_nama;
                        Pegawai.tbl_rekening.FirstOrDefault().bank_id = vdosen.bank_id;
                        Pegawai.tbl_rekening.FirstOrDefault().nama_bank_cabang = vdosen.nama_bank_cabang;
                        Pegawai.tbl_rekening.FirstOrDefault().kode_bank = vdosen.kode_bank;
                        Pegawai.tbl_rekening.FirstOrDefault().modified_by = user;
                        Pegawai.tbl_rekening.FirstOrDefault().modified_on = Now;
                    } else {
                        tbl_rekening tblRekening = new tbl_rekening();
                        tblRekening.no_rekening = vdosen.no_rekening;
                        tblRekening.atas_nama = vdosen.atas_nama;
                        tblRekening.bank_id = vdosen.bank_id;
                        tblRekening.nama_bank_cabang = vdosen.nama_bank_cabang;
                        tblRekening.kode_bank = vdosen.kode_bank;
                        tblRekening.create_by = user;
                        tblRekening.create_on = Now;
                        Pegawai.tbl_rekening.Add(tblRekening);
                    }

                    if (Pegawai.tbl_alamat.FirstOrDefault() != null) {
                        Pegawai.tbl_alamat.FirstOrDefault().jalan = vdosen.jalan;
                        Pegawai.tbl_alamat.FirstOrDefault().kelurahan = vdosen.kelurahan;
                        Pegawai.tbl_alamat.FirstOrDefault().kecamatan = vdosen.kecamatan;
                        Pegawai.tbl_alamat.FirstOrDefault().kota = vdosen.kota;
                        Pegawai.tbl_alamat.FirstOrDefault().provinsi_id = vdosen.provinsi_id;
                        Pegawai.tbl_alamat.FirstOrDefault().kode_pos = vdosen.kode_pos;
                        Pegawai.tbl_alamat.FirstOrDefault().modified_by = user;
                        Pegawai.tbl_alamat.FirstOrDefault().modified_on = Now;
                    } else {
                        tbl_alamat TblAlamat = new tbl_alamat();
                        TblAlamat.jalan = vdosen.jalan;
                        TblAlamat.kelurahan = vdosen.kelurahan;
                        TblAlamat.kecamatan = vdosen.kecamatan;
                        TblAlamat.kota = vdosen.kota;
                        TblAlamat.provinsi_id = vdosen.provinsi_id;
                        TblAlamat.kode_pos = vdosen.kode_pos;
                        TblAlamat.modified_by = user;
                        TblAlamat.modified_on = Now;
                        Pegawai.tbl_alamat.Add(TblAlamat);
                    }
                    decimal NilaiPendidikan = db.tbl_ref_pendidikan.Where(d => d.id == vdosen.pendidikan_id).FirstOrDefault().nilai.Value;
                    decimal NilaiJabatanFungsional = db.tbl_ref_jabatan_fungsional.Where(d => d.id == vdosen.jabatan_fungsional_id).FirstOrDefault().nilai.Value;
                    //int LamaPengalaman = vdosen.lama_pengalaman.Value;
                    DateTime dateNow = DateTime.Now;
                    int lama_pengalaman = 0;

                    if (Pegawai.tanggal_sk != null) {
                        DateTime TanggalSK = Pegawai.tanggal_sk.Value;
                        int TglSkTahun = TanggalSK.Year;
                        int TglSKBulan = TanggalSK.Month;

                        int TglNowTahun = dateNow.Year;
                        int TglNowBulan = dateNow.Month;

                        int selishBulan = ((TglNowTahun - TglSkTahun) * 12) + (TglNowBulan - TglSKBulan);


                        lama_pengalaman = selishBulan / 12;//VJenjangDosen.pengalaman.Value;
                    }
                    decimal NilaiPengalaman = 0;
                    var a = db.tbl_ref_pengalaman.Where(d => d.min <= lama_pengalaman && d.max >= lama_pengalaman).FirstOrDefault();
                    if (a != null) {
                        NilaiPengalaman = a.nilai.Value;
                    }

                    decimal NilaiJenjang = NilaiPendidikan + NilaiJabatanFungsional + NilaiPengalaman;

                    tbl_ref_jenjang QJenjang = new tbl_ref_jenjang();

                    QJenjang = db.tbl_ref_jenjang.Where(d => d.nilai_min <= NilaiJenjang && d.nilai_max >= NilaiJenjang).FirstOrDefault();


                    if (Dosen.tbl_dosen_jenjang.FirstOrDefault() != null) {
                        Dosen.tbl_dosen_jenjang.FirstOrDefault().pendidikan_id = vdosen.pendidikan_id;
                        Dosen.tbl_dosen_jenjang.FirstOrDefault().jabatan_fungsional_id = vdosen.jabatan_fungsional_id;
                        Dosen.tbl_dosen_jenjang.FirstOrDefault().nilai_jenjang = NilaiJenjang;
                        Dosen.tbl_dosen_jenjang.FirstOrDefault().jenjang_id = QJenjang.id;

                        Dosen.tbl_dosen_jenjang.FirstOrDefault().jenjang_id = QJenjang.id;

                        Dosen.tbl_dosen_jenjang.FirstOrDefault().modified_on = Now;
                        Dosen.tbl_dosen_jenjang.FirstOrDefault().modified_by = user;
                    } else {
                        tbl_dosen_jenjang DosenJenjang = new tbl_dosen_jenjang();
                        DosenJenjang.pendidikan_id = vdosen.pendidikan_id;
                        DosenJenjang.jabatan_fungsional_id = vdosen.jabatan_fungsional_id;
                        DosenJenjang.nilai_jenjang = NilaiJenjang;
                        DosenJenjang.jenjang_id = QJenjang.id;
                        DosenJenjang.pendidikan_id = a.id;
                        DosenJenjang.create_on = Now;
                        DosenJenjang.create_by = user;
                        Dosen.tbl_dosen_jenjang.Add(DosenJenjang);
                    }

                    tbl_dosen_jenjang_inter tblDosenJenjangInter = db.tbl_dosen_jenjang_inter.Where(d => d.dosen_id == vdosen.dosen_id).FirstOrDefault();
                    if (tblDosenJenjangInter == null) {
                        tbl_dosen_jenjang_inter NewDosenJenjangInter = new tbl_dosen_jenjang_inter();
                        NewDosenJenjangInter.dosen_id = vdosen.dosen_id;
                        NewDosenJenjangInter.jenjang_kelas_inter_id = vdosen.jenjang_inter_id;
                        db.tbl_dosen_jenjang_inter.Add(NewDosenJenjangInter);
                    } else {
                        tblDosenJenjangInter.jenjang_kelas_inter_id = vdosen.jenjang_inter_id;
                    }
                    Dosen.kd_nik = vdosen.kd_nik;
                    Dosen.status_dosen_id = vdosen.status_dosen_id;
                    Dosen.modified_on = Now;
                    Dosen.modified_by = user;

                    Dosen.nidn = vdosen.nidn;
                    Dosen.status_aktif_id = vdosen.status_aktif_id;
                    Dosen.homebase = vdosen.homebase;

                    //--- riwayat2
                    //bidang keahlian
                    if (vdosen.bidang_keahlian != null) {
                        foreach (var row in vdosen.bidang_keahlian) {
                            tbl_bidang_keahlian item;
                            if (row.id <= 0) {
                                item = db.tbl_bidang_keahlian.Add(new tbl_bidang_keahlian() { dosen_id = Dosen.id, create_by = user, create_on = NOW });
                            } else item = db.tbl_bidang_keahlian.Find(row.id);

                            if (row.deleted) db.tbl_bidang_keahlian.Remove(item);
                            else item.bidang_keahlian = row.keterangan;
                        }
                    }

                    if (vdosen.pengalaman_transport != null) {
                        foreach (var row in vdosen.pengalaman_transport) {
                            tbl_pengalaman_bdg_transport item;
                            if (row.id <= 0) {
                                item = db.tbl_pengalaman_bdg_transport.Add(new tbl_pengalaman_bdg_transport() { dosen_id = Dosen.id, create_by = user, create_on = NOW });
                            } else item = db.tbl_pengalaman_bdg_transport.Find(row.id);

                            if (row.deleted) db.tbl_pengalaman_bdg_transport.Remove(item);
                            else item.bidang_keahlian = row.keterangan;
                        }
                    }
                    if (vdosen.pengalaman_nontrans != null) {
                        foreach (var row in vdosen.pengalaman_nontrans) {
                            tbl_pengalaman_bdg_nontrans item;
                            if (row.id <= 0) {
                                item = db.tbl_pengalaman_bdg_nontrans.Add(new tbl_pengalaman_bdg_nontrans() { dosen_id = Dosen.id, create_by = user, create_on = NOW });
                            } else item = db.tbl_pengalaman_bdg_nontrans.Find(row.id);

                            if (row.deleted) db.tbl_pengalaman_bdg_nontrans.Remove(item);
                            else item.bidang_keahlian = row.keterangan;
                        }
                    }

                    if (vdosen.ampu_matkul != null) {
                        foreach (var row in vdosen.ampu_matkul) {
                            tbl_ampu_matkul item;
                            if (row.id <= 0) {
                                item = db.tbl_ampu_matkul.Add(new tbl_ampu_matkul() { dosen_id = Dosen.id, create_by = user, create_on = NOW });
                            } else item = db.tbl_ampu_matkul.Find(row.id);

                            if (row.deleted) db.tbl_ampu_matkul.Remove(item);
                            else item.mata_kuliah = row.keterangan;
                        }
                    }

                    if (vdosen.pelatihan != null) {
                        foreach (var row in vdosen.pelatihan) {
                            tbl_pelatihan_dosen item;
                            if (row.id <= 0) {
                                item = db.tbl_pelatihan_dosen.Add(new tbl_pelatihan_dosen() { dosen_id = Dosen.id, create_by = user, create_on = NOW });
                            } else item = db.tbl_pelatihan_dosen.Find(row.id);

                            if (row.deleted) db.tbl_pelatihan_dosen.Remove(item);
                            else item.pelatihan = row.keterangan;
                        }
                    }

                    if (vdosen.seminar != null) {
                        foreach (var row in vdosen.seminar) {
                            tbl_seminar_dosen item;
                            if (row.id <= 0) {
                                item = db.tbl_seminar_dosen.Add(new tbl_seminar_dosen() { dosen_id = Dosen.id, create_by = user, create_on = NOW });
                            } else item = db.tbl_seminar_dosen.Find(row.id);

                            if (row.deleted) db.tbl_seminar_dosen.Remove(item);
                            else item.seminar = row.keterangan;
                        }
                    }

                    if (vdosen.penelitian != null) {
                        foreach (var row in vdosen.penelitian) {
                            tbl_penelitian_dosen item;
                            if (row.id <= 0) {
                                item = db.tbl_penelitian_dosen.Add(new tbl_penelitian_dosen() { dosen_id = Dosen.id, create_by = user, create_on = NOW });
                            } else item = db.tbl_penelitian_dosen.Find(row.id);

                            if (row.deleted) db.tbl_penelitian_dosen.Remove(item);
                            else item.penelitian = row.keterangan;
                        }
                    }


                    //if (DosenJenjang.id > 0) db.SaveChanges();

                    if (Dosen.id > 0) {

                        db.SaveChanges();
                    }
                    message = helper.UpdateSuksesHtml();
                }

            } catch (Exception ex) {
                message = helper.ErrorMessageWithElement(ex.ToString()); status = 0;
            } finally { resultMessage.message = message; resultMessage.status = status; }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public JsonResult DeleteDosen(int ID)
        {
            string result = "Data Berhasil di Hapus";
            using (var db = new stmtsdmEntities()) {
                tbl_dosen Dosen = db.tbl_dosen.Where(d => d.id == ID).FirstOrDefault();
                Dosen.record_status = 0;
                if (Dosen.id > 0) db.SaveChanges();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDosen(int KodeNIK)
        {
            view_dosen vdosen = db.view_dosen.Where(d => d.dosen_id == KodeNIK).FirstOrDefault();
            return Json(vdosen, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public JsonResult DeleteJenjangDosen(int ID)
        {
            string result = "Data Berhasil di Hapus";
            using (var db = new stmtsdmEntities()) {
                tbl_dosen Dosen = db.tbl_dosen.Where(d => d.id == ID).FirstOrDefault();
                Dosen.record_status = 0;
                if (Dosen.id > 0) db.SaveChanges();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetJenjangDosen(int KodeNIK)
        {
            view_dosen vdosen = db.view_dosen.Where(d => d.dosen_id == KodeNIK).FirstOrDefault();
            return Json(vdosen, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefJenjang()
        {
            return Json(db.tbl_ref_jenjang.Select(d => new {
                d.id,
                d.jenjang_kelompok,
                d.nilai_min,
                d.nilai_max
            }).ToList()
                , JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefSelectedJenjang()
        {
            return Json(db.tbl_ref_jenjang.Select(d => new {
                d.id,
                value = d.jenjang_kelompok,
                d.nilai_min,
                d.nilai_max
            }).ToList()
                , JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefJenjangById(int Id)
        {
            return Json(db.tbl_ref_jenjang.Where(d => d.id == Id).Select(d => new {
                d.id,
                d.jenjang_kelompok,
                d.nilai_min,
                d.nilai_max
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public class DataTableDataRefJenjang : DataTableData
        {
            public List<RefJenjang> data { get; set; }
        }

        public JsonResult GetListJenjang(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            DataTableDataRefJenjang dt = new DataTableDataRefJenjang();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                var a = db.tbl_ref_jenjang.AsQueryable();
                if (search != string.Empty) a = a.Where(x => x.jenjang_kelompok.Contains(search));
                if (sortColumn == 0) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.id);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.id);
                }
                if (sortColumn == 1) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.jenjang_kelompok);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.jenjang_kelompok);
                }

                dt.data = a.Skip(start).Take(length).Select(d => new RefJenjang {
                    id = d.id,
                    jenjang_kelompok = d.jenjang_kelompok,
                    nilai_min = d.nilai_min,
                    nilai_max = d.nilai_max,
                    create_by = d.create_by,
                    create_on = d.create_on,
                    modified_by = d.modified_by,
                    modified_on = d.modified_on
                }).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefPendidikan()
        {
            return Json(db.tbl_ref_pendidikan.Select(d => new {
                d.id,
                d.tingkat_pendidikan,
                d.nilai,
                d.bobot
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefSelectedPendidikan()
        {
            return Json(db.tbl_ref_pendidikan.Select(d => new {
                d.id,
                value = d.tingkat_pendidikan,
                d.nilai,
                d.bobot
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefPendidikanById(int Id)
        {
            return Json(db.tbl_ref_pendidikan.Select(d => new {
                d.id,
                d.tingkat_pendidikan,
                d.nilai,
                d.bobot
            }).Where(d => d.id == Id).ToList(), JsonRequestBehavior.AllowGet);
        }

        public class DataTableDataRefPendidikan : DataTableData
        {
            public List<RefPendidikan> data { get; set; }
        }

        public JsonResult GetListMasterPendidikan(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            DataTableDataRefPendidikan dt = new DataTableDataRefPendidikan();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                var a = db.tbl_ref_pendidikan.AsQueryable();
                if (search != string.Empty) a = a.Where(x => x.tingkat_pendidikan.Contains(search));
                if (sortColumn == 0) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.id);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.id);
                }
                if (sortColumn == 1) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.tingkat_pendidikan);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.tingkat_pendidikan);
                }
                if (sortColumn == 2) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.bobot);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.bobot);
                }

                if (sortColumn == 3) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.nilai);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.nilai);
                }

                dt.data = a.Skip(start).Select(d => new RefPendidikan {
                    id = d.id,
                    nilai = d.nilai,
                    bobot = d.bobot,
                    tingkat_pendidikan = d.tingkat_pendidikan,
                    create_on = d.create_on,
                    create_by = d.create_by,
                    modified_by = d.modified_by,
                    modified_on = d.modified_on
                }).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefJabatanFungsional()
        {
            return Json(db.tbl_ref_jabatan_fungsional.Select(d => new {
                d.id,
                d.jabatan_fungsional,
                d.nilai,
                d.bobot
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefSelectedJabatanFungsional()
        {
            return Json(db.tbl_ref_jabatan_fungsional.Select(d => new {
                d.id,
                value = d.jabatan_fungsional,
                d.nilai,
                d.bobot
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefJabatanFungsionalById(int Id)
        {
            return Json(db.tbl_ref_jabatan_fungsional.Select(d => new {
                d.id,
                d.jabatan_fungsional,
                d.nilai,
                d.bobot
            }).Where(d => d.id == Id).ToList(), JsonRequestBehavior.AllowGet);
        }

        public class DataTableDataRefJabatanFungsional : DataTableData
        {
            public List<RefJabatanFungsional> data { get; set; }
        }

        public JsonResult GetListMasterJabatanFungsional(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            DataTableDataRefJabatanFungsional dt = new DataTableDataRefJabatanFungsional();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                var a = db.tbl_ref_jabatan_fungsional.Select(d => new RefJabatanFungsional {
                    id = d.id,
                    jabatan_fungsional = d.jabatan_fungsional,
                    nilai = d.nilai,
                    bobot = d.bobot,
                    create_on = d.create_on,
                    create_by = d.create_by,
                    modified_on = d.modified_on,
                    modified_by = d.modified_by
                }).AsQueryable();
                if (search != string.Empty) a = a.Where(x => x.jabatan_fungsional.Contains(search));
                if (sortColumn == 0) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.id);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.id);
                }
                if (sortColumn == 1) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.jabatan_fungsional);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.jabatan_fungsional);
                }
                if (sortColumn == 2) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.bobot);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.bobot);
                }

                if (sortColumn == 3) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.nilai);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.nilai);
                }

                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefPengalaman()
        {
            return Json(db.tbl_ref_pengalaman.Select(d => new RefPengalaman {
                id = d.id,
                nilai = d.nilai,
                min = d.min,
                max = d.max
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefPengalamanById(int Id)
        {
            return Json(db.tbl_ref_pengalaman.Select(d => new RefPengalaman {
                id = d.id,
                bobot = d.bobot,
                nilai = d.nilai,
                min = d.min,
                max = d.max
            }).Where(d => d.id == Id).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefPengalamanByLamaPengalaman(int LamaPengalaman)
        {
            return Json(db.tbl_ref_pengalaman.Select(d => new RefPengalaman {
                id = d.id,
                bobot = d.bobot,
                nilai = d.nilai,
                min = d.min,
                max = d.max
            }).Where(d => d.min <= LamaPengalaman && d.max >= LamaPengalaman).ToList(), JsonRequestBehavior.AllowGet);
        }


        public class DataTableDataRefPengalaman : DataTableData
        {
            public List<RefPengalaman> data { get; set; }
        }

        public JsonResult GetListMasterPengalaman(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            DataTableDataRefPengalaman dt = new DataTableDataRefPengalaman();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                var a = db.tbl_ref_pengalaman.Select(d => new RefPengalaman {
                    id = d.id,
                    min = d.min,
                    max = d.max,
                    bobot = d.bobot,
                    nilai = d.nilai,
                    create_on = d.create_on,
                    create_by = d.create_by,
                    modified_by = d.modified_by,
                    modified_on = d.modified_on
                }).OrderBy(e => e.id).AsQueryable();

                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefStatusDosen()
        {
            return Json(db.tbl_ref_status_dosen.Select(d => new {
                d.id,
                d.status_dosen,
                d.keterangan
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefStatusDosenById(int Id)
        {
            return Json(db.tbl_ref_status_dosen.Select(d => new {
                d.id,
                d.status_dosen,
                d.keterangan
            }).Where(d => d.id == Id).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefSelectedStatusDosen()
        {
            return Json(db.tbl_ref_status_dosen.Select(d => new {
                d.id,
                value = d.status_dosen,
            }).ToList(), JsonRequestBehavior.AllowGet);
        }


        public class DataTableDataRefStatusDosen : DataTableData
        {
            public List<RefStatusDosen> data { get; set; }
        }

        public JsonResult GetListRefStatusDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            DataTableDataRefStatusDosen dt = new DataTableDataRefStatusDosen();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                var a = db.tbl_ref_status_dosen.Select(d => new RefStatusDosen {
                    id = d.id,
                    status_dosen = d.status_dosen,
                    keterangan = d.keterangan,
                    create_on = d.create_on,
                    create_by = d.create_by,
                    modified_by = d.modified_by,
                    modified_on = d.modified_on
                }).OrderBy(d => d.id).AsQueryable();
                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefHonorariumDosen()
        {
            return Json(db.tbl_ref_honorarium.Select(d => new {
                d.id,
                d.honor_per_sks,
                d.jenjang_id,
                d.transpor_per_hari
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefHonorariumDosenById(int Id)
        {
            return Json(db.tbl_ref_honorarium.Select(d => new {
                d.id,
                d.honor_per_sks,
                d.jenjang_id,
                d.transpor_per_hari
            }).Where(d => d.id == Id).ToList(), JsonRequestBehavior.AllowGet);
        }

        public class DataTableDataRefHonorariumDosen : DataTableData
        {
            public List<RefHonorarium> data { get; set; }
        }

        public JsonResult GetListHonorariumDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            DataTableDataRefHonorariumDosen dt = new DataTableDataRefHonorariumDosen();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                var a = db.tbl_ref_honorarium.Join(db.tbl_ref_jenjang,
                        refHonor => refHonor.jenjang_id,
                        refJenjang => refJenjang.id,
                        (refHonor, refJenjang) => new { RefHonor = refHonor, RefJenjang = refJenjang }
                    ).Select(d => new RefHonorarium {
                        id = d.RefHonor.id,
                        jenjang_id = d.RefHonor.jenjang_id,
                        jenjang_nama = d.RefJenjang.jenjang_kelompok,
                        honor_per_sks = d.RefHonor.honor_per_sks,
                        transpor_per_hadir = d.RefHonor.transpor_per_hari,
                        create_on = d.RefHonor.create_on,
                        create_by = d.RefHonor.create_by,
                        modified_by = d.RefHonor.modified_by,
                        modified_on = d.RefHonor.modified_on
                    }).OrderBy(d => d.id).AsQueryable();
                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefHonorTunjangan()
        {
            return Json(db.tbl_ref_honor_tunjangan.Select(d => new {
                d.id,
                d.honor_tunjangan,
                d.keterangan,
                d.status_dosen_id
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefHonorTunjanganById(int Id)
        {
            return Json(db.tbl_ref_honor_tunjangan.Select(d => new {
                d.id,
                d.honor_tunjangan,
                d.keterangan,
                d.status_dosen_id
            }).Where(d => d.id == Id).ToList(), JsonRequestBehavior.AllowGet);
        }

        public class DataTableDataRefHonorTunjangan : DataTableData
        {
            public List<RefHonorTunjangan> data { get; set; }
        }

        public JsonResult GetListRefHonorTunjanganDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            DataTableDataRefHonorTunjangan dt = new DataTableDataRefHonorTunjangan();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                var a = db.tbl_ref_honor_tunjangan.Join(db.tbl_ref_status_dosen,
                honorTunjangan => honorTunjangan.status_dosen_id,
                statusDosen => statusDosen.id,
                (honorTunjangan, statusDosen) => new { HonorTunjangan = honorTunjangan, StatusDosen = statusDosen }
                ).Select(d => new RefHonorTunjangan {
                    id = d.HonorTunjangan.id,
                    honor_tunjangan = d.HonorTunjangan.honor_tunjangan,
                    keterangan = d.HonorTunjangan.keterangan,
                    status_dosen_id = d.HonorTunjangan.status_dosen_id,
                    status_dosen = d.StatusDosen.status_dosen,
                    create_by = d.HonorTunjangan.create_by,
                    create_on = d.HonorTunjangan.create_on
                }).OrderBy(d => d.id).AsQueryable();
                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
                dt.recordsTotal = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult AddEditJenjangDosen(view_jenjang_dosen VJenjangDosen)
        {

            if (VJenjangDosen.dosen_jenjang_id > 0) {
                resultMessage = EditjenjangDosen(VJenjangDosen);
            } else {
                resultMessage = AddjenjangDosen(VJenjangDosen);
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        //[Authorize(Roles = "super,sdm")]
        public ResultMessage AddjenjangDosen(view_jenjang_dosen VJenjangDosen)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    if (db.tbl_dosen_jenjang.Where(d => d.dosen_id == VJenjangDosen.dosen_id).Count() > 0) {
                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data Sudah Ada");
                    } else {
                        #region hitung jenjang
                        int lama_pengalaman = VJenjangDosen.pengalaman.Value;
                        tbl_ref_pengalaman RefPengalaman = db.tbl_ref_pengalaman.Where(d => d.min <= lama_pengalaman && d.max >= lama_pengalaman).FirstOrDefault();

                        //hitung jenjang dosen= nilai_pendidikan+nilai_jabatan_fungsional+nilai_pengalaman
                        //parameter pertama buat ngitung jenjang dosen/golongan
                        decimal nilai_pendidikan = db.tbl_ref_pendidikan.Where(d => d.id == VJenjangDosen.pendidikan_id).FirstOrDefault().nilai.Value;
                        decimal nilai_jabatan_fungsional = db.tbl_ref_jabatan_fungsional.Where(d => d.id == VJenjangDosen.jabatan_fungsional_id).FirstOrDefault().nilai.Value;
                        decimal nilai_pengalaman = db.tbl_ref_pengalaman.Where(d => d.min <= lama_pengalaman && d.max >= lama_pengalaman).FirstOrDefault().nilai.Value;
                        decimal nila_jenjang = nilai_pendidikan + nilai_jabatan_fungsional + nilai_pengalaman;

                        tbl_dosen_jenjang tblDosenJenjang = new tbl_dosen_jenjang();
                        tblDosenJenjang.dosen_id = VJenjangDosen.dosen_id;
                        tblDosenJenjang.pendidikan_id = VJenjangDosen.pendidikan_id;
                        tblDosenJenjang.jabatan_fungsional_id = VJenjangDosen.jabatan_fungsional_id;
                        tblDosenJenjang.pengalaman_id = RefPengalaman.id;
                        tblDosenJenjang.pengalaman = lama_pengalaman;
                        tblDosenJenjang.nilai_jenjang = nila_jenjang;
                        tblDosenJenjang.jenjang_id = db.tbl_ref_jenjang.Where(d => d.nilai_min <= nila_jenjang && d.nilai_max >= nila_jenjang).FirstOrDefault().id;
                        tblDosenJenjang.create_on = NOW;
                        tblDosenJenjang.create_by = user;
                        db.tbl_dosen_jenjang.Add(tblDosenJenjang);
                        db.SaveChanges();
                        #endregion

                        resultMessage.status = 1;
                        resultMessage.message = helper.UpdateSuksesHtml();
                    }
                }
            } catch (Exception ex) {

                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            }


            return resultMessage;
        }
        //[Authorize(Roles = "super,sdm")]
        public ResultMessage EditjenjangDosen(view_jenjang_dosen VJenjangDosen)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    if (db.tbl_dosen_jenjang.Where(d => d.dosen_id == VJenjangDosen.dosen_id).Count() > 0) {
                        #region hitung jenjang
                        DateTime dateNow = DateTime.Now;
                        DateTime TanggalSK = Convert.ToDateTime(VJenjangDosen.tanggal_sk);
                        int TglSkTahun = TanggalSK.Year;
                        int TglSKBulan = TanggalSK.Month;

                        int TglNowTahun = dateNow.Year;
                        int TglNowBulan = dateNow.Month;

                        int selishBulan = ((TglNowTahun - TglSkTahun) * 12) + (TglNowBulan - TglSKBulan);


                        int lama_pengalaman = selishBulan / 12;//VJenjangDosen.pengalaman.Value;

                        decimal nilai_pendidikan = db.tbl_ref_pendidikan.Where(d => d.id == VJenjangDosen.pendidikan_id).FirstOrDefault().nilai.Value;
                        decimal nilai_jabatan_fungsional = db.tbl_ref_jabatan_fungsional.Where(d => d.id == VJenjangDosen.jabatan_fungsional_id).FirstOrDefault().nilai.Value;
                        tbl_ref_pengalaman pengalaman = db.tbl_ref_pengalaman.Where(d => d.min <= lama_pengalaman && d.max >= lama_pengalaman).FirstOrDefault();
                        decimal nilai_pengalaman = pengalaman.nilai.Value;
                        decimal nila_jenjang = nilai_pendidikan + nilai_jabatan_fungsional + nilai_pengalaman;



                        tbl_dosen_jenjang tblDosenJenjang = db.tbl_dosen_jenjang.Where(d => d.dosen_id == VJenjangDosen.dosen_id).FirstOrDefault();
                        tblDosenJenjang.pendidikan_id = VJenjangDosen.pendidikan_id;
                        tblDosenJenjang.jabatan_fungsional_id = VJenjangDosen.jabatan_fungsional_id;
                        tblDosenJenjang.pengalaman_id = pengalaman.id;
                        tblDosenJenjang.pengalaman = lama_pengalaman;
                        tblDosenJenjang.nilai_jenjang = nila_jenjang;
                        tblDosenJenjang.jenjang_id = db.tbl_ref_jenjang.Where(d => d.nilai_min <= nila_jenjang && d.nilai_max >= nila_jenjang).FirstOrDefault().id;
                        tblDosenJenjang.modified_on = NOW;
                        tblDosenJenjang.modified_by = user;
                        db.SaveChanges();

                        tbl_dosen tblDosen = db.tbl_dosen.Where(d => d.id == VJenjangDosen.dosen_id).FirstOrDefault();
                        tbl_pegawai tblPegawai = db.tbl_pegawai.Where(d => d.id == tblDosen.pegawai_id).FirstOrDefault();
                        tblPegawai.tanggal_sk = VJenjangDosen.tanggal_sk;
                        db.SaveChanges();

                        #endregion

                        resultMessage.status = 1;
                        resultMessage.message = helper.UpdateSuksesHtml();
                    } else {

                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data Belum Ada");
                    }
                }
            } catch (Exception ex) {
                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            }


            return resultMessage;
        }
        //[Authorize(Roles = "super,sdm")]
        public JsonResult AddEditRefJabatanFungsional(tbl_ref_jabatan_fungsional RefJabatanFungsional)
        {
            int status = 0;
            string result = "";
            using (var db = new stmtsdmEntities()) {
                try {
                    if (RefJabatanFungsional.id > 0) {
                        tbl_ref_jabatan_fungsional tblRefJabatanfungsional = db.tbl_ref_jabatan_fungsional.Where(d => d.id == RefJabatanFungsional.id).FirstOrDefault();
                        tblRefJabatanfungsional.jabatan_fungsional = RefJabatanFungsional.jabatan_fungsional;
                        tblRefJabatanfungsional.bobot = RefJabatanFungsional.bobot;
                        tblRefJabatanfungsional.nilai = RefJabatanFungsional.nilai;
                        tblRefJabatanfungsional.modified_on = NOW;
                        tblRefJabatanfungsional.modified_by = user;
                        db.SaveChanges();
                        status = 1;
                        result = helper.UpdateSuksesHtml();
                    } else {
                        RefJabatanFungsional.create_on = NOW;
                        RefJabatanFungsional.create_by = user;
                        db.tbl_ref_jabatan_fungsional.Add(RefJabatanFungsional);
                        db.SaveChanges();
                        status = 1;
                        result = helper.SaveSuksesHtml();
                    }
                } catch (Exception ex) {
                    status = 0;
                    result = helper.ErrorMessageWithElement(ex.ToString());
                } finally {
                    resultMessage.status = status;
                    resultMessage.message = result;
                }
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult DeleteRefJabatanFungsional(int ID)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    tbl_ref_jabatan_fungsional tblJabatanFungsional = db.tbl_ref_jabatan_fungsional.Where(d => d.id == ID).FirstOrDefault();
                    if (tblJabatanFungsional != null) {
                        db.tbl_ref_jabatan_fungsional.Remove(tblJabatanFungsional);
                        db.SaveChanges();
                        resultMessage.status = 1;
                        resultMessage.message = helper.RemoveSuksesHtml();
                    } else {
                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data Suda Ada");
                    }
                }
            } catch (Exception ex) {
                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            } finally { }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        //[Authorize(Roles = "super,sdm")]
        public JsonResult AddEditRefPendidikan(tbl_ref_pendidikan RefPendidikan)
        {
            int status = 0;
            string result = "";
            using (var db = new stmtsdmEntities()) {
                try {
                    if (RefPendidikan.id > 0) {
                        tbl_ref_pendidikan tblRefPendidikan = db.tbl_ref_pendidikan.Where(d => d.id == RefPendidikan.id).FirstOrDefault();
                        tblRefPendidikan.tingkat_pendidikan = RefPendidikan.tingkat_pendidikan;
                        tblRefPendidikan.bobot = RefPendidikan.bobot;
                        tblRefPendidikan.nilai = RefPendidikan.nilai;
                        tblRefPendidikan.modified_on = NOW;
                        tblRefPendidikan.modified_by = user;
                        db.SaveChanges();
                        status = 1;
                        result = helper.UpdateSuksesHtml();
                    } else {
                        RefPendidikan.create_on = NOW;
                        RefPendidikan.create_by = user;
                        db.tbl_ref_pendidikan.Add(RefPendidikan);
                        db.SaveChanges();
                        status = 1;
                        result = helper.SaveSuksesHtml();
                    }
                } catch (Exception ex) {
                    status = 0;
                    result = helper.ErrorMessageWithElement(ex.ToString());
                } finally {
                    resultMessage.status = status;
                    resultMessage.message = result;
                }
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        //[Authorize(Roles = "super,sdm")]
        public JsonResult DeleteRefPendidikan(int ID)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    tbl_ref_pendidikan tblRefPendidikan = db.tbl_ref_pendidikan.Where(d => d.id == ID).FirstOrDefault();
                    if (tblRefPendidikan != null) {
                        db.tbl_ref_pendidikan.Remove(tblRefPendidikan);
                        db.SaveChanges();
                        resultMessage.status = 1;
                        resultMessage.message = helper.RemoveSuksesHtml();
                    } else {
                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data Suda Ada");
                    }
                }
            } catch (Exception ex) {
                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            } finally { }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult AddEditRefPengalaman(tbl_ref_pengalaman RefPengalaman)
        {
            int status = 0;
            string result = "";
            using (var db = new stmtsdmEntities()) {
                try {
                    if (RefPengalaman.id > 0) {
                        tbl_ref_pengalaman tblRefPengalaman = db.tbl_ref_pengalaman.Where(d => d.id == RefPengalaman.id).FirstOrDefault();
                        tblRefPengalaman.max = RefPengalaman.max;
                        tblRefPengalaman.min = RefPengalaman.min;
                        tblRefPengalaman.bobot = RefPengalaman.bobot;
                        tblRefPengalaman.nilai = RefPengalaman.nilai;
                        tblRefPengalaman.modified_on = NOW;
                        tblRefPengalaman.modified_by = user;
                        status = 1;
                        result = helper.UpdateSuksesHtml();
                    } else {
                        RefPengalaman.create_on = NOW;
                        RefPengalaman.create_by = user;
                        db.tbl_ref_pengalaman.Add(RefPengalaman);
                        db.SaveChanges();
                        status = 1;
                        result = helper.SaveSuksesHtml();
                    }
                } catch (Exception ex) {
                    status = 0;
                    result = helper.ErrorMessageWithElement(ex.ToString());
                } finally {
                    resultMessage.status = status;
                    resultMessage.message = result;
                }
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        //[Authorize(Roles = "super,sdm")]
        public JsonResult DeleteRefPengalaman(int ID)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    tbl_ref_pengalaman tblRefPengalaman = db.tbl_ref_pengalaman.Where(d => d.id == ID).FirstOrDefault();
                    if (tblRefPengalaman != null) {
                        db.tbl_ref_pengalaman.Remove(tblRefPengalaman);
                        db.SaveChanges();
                        resultMessage.status = 1;
                        resultMessage.message = helper.RemoveSuksesHtml();
                    } else {
                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data Suda Ada");
                    }
                }
            } catch (Exception ex) {
                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            } finally { }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult AddEditRefJenjang(tbl_ref_jenjang RefJenjang)
        {
            int status = 0;
            string result = "";
            using (var db = new stmtsdmEntities()) {
                try {
                    if (RefJenjang.id > 0) {
                        tbl_ref_jenjang tblRefJenjang = db.tbl_ref_jenjang.Where(d => d.id == RefJenjang.id).FirstOrDefault();
                        tblRefJenjang.jenjang_kelompok = RefJenjang.jenjang_kelompok;
                        tblRefJenjang.nilai_min = RefJenjang.nilai_min;
                        tblRefJenjang.nilai_max = RefJenjang.nilai_max;
                        tblRefJenjang.modified_on = NOW;
                        tblRefJenjang.modified_by = user;
                        db.SaveChanges();
                        status = 1;
                        result = helper.UpdateSuksesHtml();
                    } else {
                        RefJenjang.create_on = NOW;
                        RefJenjang.create_by = user;
                        db.tbl_ref_jenjang.Add(RefJenjang);
                        db.SaveChanges();
                        status = 1;
                        result = helper.SaveSuksesHtml();
                    }
                } catch (Exception ex) {
                    status = 0;
                    result = helper.ErrorMessageWithElement(ex.ToString());
                } finally {
                    resultMessage.status = status;
                    resultMessage.message = result;
                }
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult DeleteRefJenjang(int ID)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    tbl_ref_jenjang tblRefJenjang = db.tbl_ref_jenjang.Where(d => d.id == ID).FirstOrDefault();
                    if (tblRefJenjang != null) {
                        db.tbl_ref_jenjang.Remove(tblRefJenjang);
                        db.SaveChanges();
                        resultMessage.status = 1;
                        resultMessage.message = helper.RemoveSuksesHtml();
                    } else {
                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data Suda Ada");
                    }
                }
            } catch (Exception ex) {
                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            } finally { }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult AddEditRefStatusDosen(tbl_ref_status_dosen RefStatusDosen)
        {
            int status = 0;
            string result = "";
            using (var db = new stmtsdmEntities()) {
                try {
                    if (RefStatusDosen.id > 0) {
                        tbl_ref_status_dosen tblRefStatusDosen = db.tbl_ref_status_dosen.Where(d => d.id == RefStatusDosen.id).FirstOrDefault();
                        tblRefStatusDosen.status_dosen = RefStatusDosen.status_dosen;
                        tblRefStatusDosen.keterangan = RefStatusDosen.keterangan;
                        tblRefStatusDosen.modified_on = NOW;
                        tblRefStatusDosen.modified_by = user;
                        status = 1;
                        result = helper.UpdateSuksesHtml();
                    } else {
                        RefStatusDosen.create_on = NOW;
                        RefStatusDosen.create_by = user;
                        db.tbl_ref_status_dosen.Add(RefStatusDosen);
                        db.SaveChanges();
                        status = 1;
                        result = helper.SaveSuksesHtml();
                    }
                } catch (Exception ex) {
                    status = 0;
                    result = helper.ErrorMessageWithElement(ex.ToString());
                } finally {
                    resultMessage.status = status;
                    resultMessage.message = result;
                }
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult DeleteRefStatusDosen(int ID)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    tbl_ref_status_dosen tblRefStatusDosen = db.tbl_ref_status_dosen.Where(d => d.id == ID).FirstOrDefault();
                    if (tblRefStatusDosen != null) {
                        db.tbl_ref_status_dosen.Remove(tblRefStatusDosen);
                        db.SaveChanges();
                        resultMessage.status = 1;
                        resultMessage.message = helper.RemoveSuksesHtml();
                    } else {
                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data Suda Ada");
                    }
                }
            } catch (Exception ex) {
                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            } finally { }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult AddEditRefHonorariumDosen(tbl_ref_honorarium RefHonorariumDosen)
        {
            int status = 0;
            string result = "";
            using (var db = new stmtsdmEntities()) {
                try {
                    if (RefHonorariumDosen.id > 0) {
                        tbl_ref_honorarium tblRefHonorariumDosen = db.tbl_ref_honorarium.Where(d => d.id == RefHonorariumDosen.id).FirstOrDefault();
                        tblRefHonorariumDosen.honor_per_sks = RefHonorariumDosen.honor_per_sks;
                        tblRefHonorariumDosen.transpor_per_hari = RefHonorariumDosen.transpor_per_hari;
                        tblRefHonorariumDosen.jenjang_id = RefHonorariumDosen.jenjang_id;
                        tblRefHonorariumDosen.modified_on = NOW;
                        tblRefHonorariumDosen.modified_by = user;
                        db.SaveChanges();
                        status = 1;
                        result = helper.UpdateSuksesHtml();
                    } else {
                        RefHonorariumDosen.create_on = NOW;
                        RefHonorariumDosen.create_by = user;
                        db.tbl_ref_honorarium.Add(RefHonorariumDosen);
                        db.SaveChanges();
                        status = 1;
                        result = helper.SaveSuksesHtml();
                    }
                } catch (Exception ex) {
                    status = 0;
                    result = helper.ErrorMessageWithElement(ex.ToString());
                } finally {
                    resultMessage.status = status;
                    resultMessage.message = result;
                }
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult DeleteRefHonorariumDosen(int ID)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    tbl_ref_honorarium tblHonorariumDosen = db.tbl_ref_honorarium.Where(d => d.id == ID).FirstOrDefault();
                    if (tblHonorariumDosen != null) {
                        db.tbl_ref_honorarium.Remove(tblHonorariumDosen);
                        db.SaveChanges();
                        resultMessage.status = 1;
                        resultMessage.message = helper.RemoveSuksesHtml();
                    } else {
                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data Suda Ada");
                    }
                }
            } catch (Exception ex) {
                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            } finally { }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult AddEditRefHonorTunjangan(tbl_ref_honor_tunjangan RefHonorTunjangan)
        {
            int status = 0;
            string result = "";
            using (var db = new stmtsdmEntities()) {
                try {
                    if (RefHonorTunjangan.id > 0) {
                        tbl_ref_honor_tunjangan tblRefHonorTunjangan = db.tbl_ref_honor_tunjangan.Where(d => d.id == RefHonorTunjangan.id).FirstOrDefault();
                        tblRefHonorTunjangan.honor_tunjangan = RefHonorTunjangan.honor_tunjangan;
                        tblRefHonorTunjangan.keterangan = RefHonorTunjangan.keterangan;
                        tblRefHonorTunjangan.status_dosen_id = RefHonorTunjangan.status_dosen_id;
                        tblRefHonorTunjangan.modified_on = NOW;
                        tblRefHonorTunjangan.modified_by = user;
                        db.SaveChanges();
                        status = 1;
                        result = helper.UpdateSuksesHtml();
                    } else {
                        RefHonorTunjangan.create_on = NOW;
                        RefHonorTunjangan.create_by = user;
                        db.tbl_ref_honor_tunjangan.Add(RefHonorTunjangan);
                        db.SaveChanges();
                        status = 1;
                        result = helper.SaveSuksesHtml();
                    }
                } catch (Exception ex) {
                    status = 0;
                    result = helper.ErrorMessageWithElement(ex.ToString());
                } finally {
                    resultMessage.status = status;
                    resultMessage.message = result;
                }
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        // [Authorize(Roles = "super,sdm")]
        public JsonResult DeleteRefHonorTunjangan(int ID)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    tbl_ref_honor_tunjangan tblHonorTunjangan = db.tbl_ref_honor_tunjangan.Where(d => d.id == ID).FirstOrDefault();
                    if (tblHonorTunjangan != null) {
                        db.tbl_ref_honor_tunjangan.Remove(tblHonorTunjangan);
                        db.SaveChanges();
                        resultMessage.status = 1;
                        resultMessage.message = helper.RemoveSuksesHtml();
                    } else {
                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data Suda Ada");
                    }
                }
            } catch (Exception ex) {
                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            } finally { }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HitungJenjang(int pendidikan_id, int jabatan_id, string pengalaman)
        {

            return Json(HitungGetJenjang(pendidikan_id, jabatan_id, pengalaman), JsonRequestBehavior.AllowGet);
        }

        public RefJenjang HitungGetJenjang(int pendidikan_id, int jabatan_id, string pengalaman)
        {
            decimal nilai_pendidikan = db.tbl_ref_pendidikan.Where(d => d.id == pendidikan_id).FirstOrDefault().nilai.Value;
            decimal nilai_jabatan = db.tbl_ref_jabatan_fungsional.Where(d => d.id == jabatan_id).FirstOrDefault().nilai.Value;
            DateTime dateNow = DateTime.Now;
            DateTime TanggalSK = DateTime.ParseExact(pengalaman, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            int TglSkTahun = TanggalSK.Year;
            int TglSKBulan = TanggalSK.Month;

            int TglNowTahun = dateNow.Year;
            int TglNowBulan = dateNow.Month;

            int selishBulan = ((TglNowTahun - TglSkTahun) * 12) + (TglNowBulan - TglSKBulan);


            int lama_pengalaman = selishBulan / 12;//VJenjangDosen.pengalaman.Value;
            decimal nilai_pengalaman = db.tbl_ref_pengalaman.Where(d => d.min <= lama_pengalaman && d.max >= lama_pengalaman).FirstOrDefault().nilai.Value;
            RefJenjang jenjang = db.tbl_ref_jenjang.Where(d => d.nilai_min <= (nilai_jabatan + nilai_pendidikan + nilai_pengalaman) && d.nilai_max >= (nilai_jabatan + nilai_pendidikan + nilai_pengalaman)).
                        Select(d => new RefJenjang {
                            id = d.id,
                            jenjang_kelompok = d.jenjang_kelompok,
                            nilai_max = d.nilai_max,
                            nilai_min = d.nilai_min,
                            create_on = d.create_on,
                            create_by = d.create_by,
                            modified_on = d.modified_on,
                            modified_by = d.modified_by
                        }).FirstOrDefault();
            return jenjang;
        }

        public ValidasiModel ValidasiJenjangDosen(tbl_dosen_jenjang DosenJenjang)
        {

            return validasiModel;
        }

        public bool validateStringNoEmpty(string str)
        {
            bool bl = false;
            if (!string.IsNullOrEmpty(str)) bl = true;
            return bl;
        }

        #region honor dosen kelas international
        public ActionResult DosenInternational()
        {
            // ViewBag.baseUrl = baseUrl;
            return View();
        }


        public class DataTableDataRefJenjangInter : DataTableData
        {
            public List<RefHonorJenjangInter> data { get; set; }
        }

        public JsonResult GetListJenjangInter(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            DataTableDataRefJenjangInter dt = new DataTableDataRefJenjangInter();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                // var a = db.tbl_ref_jenjang.AsQueryable();
                var a = db.tbl_ref_jenjang_inter.Join(db.tbl_ref_honor_inter,
                JenjangInter => JenjangInter.id,
                HonorInter => HonorInter.jenjang_inter_id,
                (JenjangInter, HonorInter) => new { JenjangInter = JenjangInter, HonorInter = HonorInter }
                ).Select(d => new RefHonorJenjangInter {
                    jenjang_inter_id = d.JenjangInter.id,
                    jenjang_inter = d.JenjangInter.jenjang_inter,
                    honor_per_hadir = d.HonorInter.honor_per_hadir.Value,
                    create_on = d.HonorInter.create_on,
                    create_by = d.HonorInter.create_by,
                    modified_on = d.HonorInter.modified_on,
                    modified_by = d.HonorInter.modified_by
                }).OrderBy(d => d.jenjang_inter_id).AsQueryable();

                if (search != string.Empty) a = a.Where(x => x.jenjang_inter.Contains(search));
                if (sortColumn == 0) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.jenjang_inter_id);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.jenjang_inter_id);
                }
                if (sortColumn == 1) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.jenjang_inter);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.jenjang_inter);
                }

                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetJenjangInter(int id)
        {
            var getJenjangInter = db.tbl_ref_jenjang_inter.Join(db.tbl_ref_honor_inter,
                JenjangInter => JenjangInter.id,
                HonorInter => HonorInter.jenjang_inter_id,
                (JenjangInter, HonorInter) => new { JenjangInter = JenjangInter, HonorInter = HonorInter }
                ).Select(d => new RefHonorJenjangInter {
                    jenjang_inter_id = d.JenjangInter.id,
                    jenjang_inter = d.JenjangInter.jenjang_inter,
                    honor_per_hadir = d.HonorInter.honor_per_hadir.Value,
                    create_on = d.HonorInter.create_on,
                    create_by = d.HonorInter.create_by,
                    modified_on = d.HonorInter.modified_on,
                    modified_by = d.HonorInter.modified_by
                }).Where(d => d.jenjang_inter_id == id).FirstOrDefault();

            return Json(getJenjangInter, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region bank
        public ActionResult iBank()
        {
            return View("~/Views/Iframe/bank.cshtml");
        }

        public class DataTableDataRefBank : DataTableData
        {
            public List<ViewRefBank> data { get; set; }
        }

        public JsonResult GetListRefBank(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            DataTableDataRefBank dt = new DataTableDataRefBank();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                // var a = db.tbl_ref_jenjang.AsQueryable();
                var a = db.tbl_ref_bank.AsQueryable();
                dt.recordsTotal = a.Count();
                if (search != string.Empty) a = a.Where(x => x.nama_bank.Contains(search));
                if (sortColumn == 0) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.id);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.id);
                }
                if (sortColumn == 1) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.nama_bank);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_bank);
                }
                dt.recordsFiltered = a.Count();
                a = a.Skip(start).Take(length);

                dt.data = a.Select(d => new ViewRefBank {
                    id = d.id,
                    nama_bank = d.nama_bank,
                    kode_bank = d.kode_bank,
                    biaya_bank = d.biaya_bank
                }).ToList();


            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddBank(string nama_bank, string kode_bank, decimal biaya_bank)
        {
            string message = "";
            int status = 0;
            tbl_ref_bank cekRefNamaBank = db.tbl_ref_bank.Where(d => d.nama_bank == nama_bank).FirstOrDefault();
            if (cekRefNamaBank != null) {
                resultMessage.message = helper.ErrorMessageWithElement("Nama Bank '" + nama_bank + "' Sudah Ada");
                resultMessage.status = 0;
                return Json(resultMessage, JsonRequestBehavior.AllowGet);
            }
            tbl_ref_bank newRefBank = new tbl_ref_bank();
            newRefBank.nama_bank = nama_bank;
            newRefBank.kode_bank = kode_bank;
            newRefBank.biaya_bank = biaya_bank;
            db.tbl_ref_bank.Add(newRefBank);

            try {
                db.SaveChanges();
                message = helper.SaveSuksesHtml();
                status = 1;
            } catch (Exception ex) { message = helper.ErrorMessageWithElement(ex.ToString()); status = 0; } finally { resultMessage.message = message; resultMessage.status = status; }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EditBank(int id, string nama_bank, string kode_bank, decimal biaya_bank)
        {
            string message = "";
            int status = 0;
            tbl_ref_bank newRefBank = db.tbl_ref_bank.Where(d => d.id == id).FirstOrDefault();
            if (newRefBank == null) {
                return Json(new ResultMessage { message = helper.ErrorMessageWithElement("Tidak Ada Bank"), status = 0 }, JsonRequestBehavior.AllowGet);
            }
            tbl_ref_bank cekRefNamaBank = db.tbl_ref_bank.Where(d => d.nama_bank == nama_bank && d.id != id).FirstOrDefault();
            if (cekRefNamaBank != null) {
                resultMessage.message = helper.ErrorMessageWithElement("Nama Bank '" + nama_bank + "' Sudah Ada");
                resultMessage.status = 0;
                return Json(resultMessage, JsonRequestBehavior.AllowGet);
            }
            newRefBank.nama_bank = nama_bank;
            newRefBank.kode_bank = kode_bank;
            newRefBank.biaya_bank = biaya_bank;
            try {
                db.SaveChanges();
                message = helper.SaveSuksesHtml();
                status = 1;
            } catch (Exception ex) { message = helper.ErrorMessageWithElement(ex.ToString()); status = 0; } finally { resultMessage.message = message; resultMessage.status = status; }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteBank(int id)
        {
            tbl_ref_bank DelRefBank = db.tbl_ref_bank.Where(d => d.id == id).FirstOrDefault();
            if (DelRefBank != null) {
                db.tbl_ref_bank.Remove(DelRefBank);
            }
            try {
                db.SaveChanges();
                resultMessage = new ResultMessage { message = helper.RemoveSuksesHtml(), status = 1 };
            } catch (Exception ex) {
                resultMessage = new ResultMessage { message = helper.ErrorMessageWithElement(ex.ToString()), status = 0 };
            } finally { }

            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRefSelectedBank()
        {

            return Json(db.tbl_ref_bank.Select(d => new {
                d.id,
                value = d.nama_bank,
                d.kode_bank,
                d.biaya_bank
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult BiayaBank()
        {
            return View();
        }
        #endregion

        public class DataTableDataContainer<T> : DataTableData
        {
            public List<T> data { get; set; }
        }

        #region riwayat pendidikan
        [HttpPost]
        public JsonResult GetListRiwayatPendidikan(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            var dt = new DataTableDataContainer<v_riwayat_pendidikan>();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1) {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities()) {
                var a = db.tbl_riwayat_pendidikan.AsQueryable();
                if (search != string.Empty) a = a.Where(x => x.nama_institusi.Contains(search));
                if (sortColumn == 0) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.id);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.id);
                }
                if (sortColumn == 1) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.jenjang);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.jenjang);
                }
                if (sortColumn == 2) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.nama_institusi);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_institusi);
                }

                if (sortColumn == 3) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.program_studi);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.program_studi);
                }
                if (sortColumn == 4) {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.gelar);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.gelar);
                }

                dt.data = a.Skip(start).Select(d => new v_riwayat_pendidikan {
                    id = d.id,
                    jenjang = d.jenjang,
                    nama_institusi = d.nama_institusi,
                    program_studi = d.program_studi,
                    gelar = d.gelar,
                    create_on = d.create_on,
                    create_by = d.create_by,
                    modified_by = d.modified_by,
                    modified_on = d.modified_on
                }).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddEditRiwayatPendidikan(tbl_riwayat_pendidikan model)
        {
            int status = 0;
            string result = "";
            using (var db = new stmtsdmEntities()) {
                try {
                    if (model.id > 0) {
                        var dbObj = db.tbl_riwayat_pendidikan.Find(model.id);
                        dbObj.jenjang = model.jenjang;
                        dbObj.nama_institusi = model.nama_institusi;
                        dbObj.program_studi = model.program_studi;
                        dbObj.gelar = model.gelar;

                        dbObj.modified_on = NOW;
                        dbObj.modified_by = user;
                        db.SaveChanges();
                        status = 1;
                        result = helper.UpdateSuksesHtml();
                    } else {
                        model.create_on = NOW;
                        model.create_by = user;
                        db.tbl_riwayat_pendidikan.Add(model);
                        db.SaveChanges();

                        status = 1;
                        result = helper.SaveSuksesHtml();
                    }
                } catch (Exception ex) {
                    status = 0;
                    result = helper.ErrorMessageWithElement(ex.ToString());
                } finally {
                    resultMessage.status = status;
                    resultMessage.message = result;
                }
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        //        [HttpPost]
        public JsonResult DeleteRiwayatPendidikan(int id)
        {
            try {
                using (var db = new stmtsdmEntities()) {
                    var dbObj = db.tbl_riwayat_pendidikan.Find(id);
                    if (dbObj != null) {
                        db.tbl_riwayat_pendidikan.Remove(dbObj);
                        db.SaveChanges();
                        resultMessage.status = 1;
                        resultMessage.message = helper.RemoveSuksesHtml();
                    } else {
                        resultMessage.status = 0;
                        resultMessage.message = helper.ErrorMessageWithElement("Data tidak ditemukan");
                    }
                }
            } catch (Exception ex) {
                resultMessage.status = 0;
                resultMessage.message = helper.ErrorMessageWithElement(ex.ToString());
            } finally { }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region buat auto complate
        public JsonResult ajaxListKDNIKDosen(int take)
        {
            var db = new stmtsdmEntities();
            var term = System.Web.HttpContext.Current.Request["term"].ToString();

            var VDosen = db.view_siakad_dosen
                .Select(d => new { d.fullname_user, d.code_lecturer }).Where(d => d.code_lecturer.Contains(term) || d.fullname_user.Contains(term)).Distinct().Take(take).ToList();
            //    Select(d=>new{
            //        label=d.kd_nik,
            //        value=d.kd_nik
            //}).Take(10).ToList();
            return Json(VDosen, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxListKelas(int take)
        {
            var db = new stmtsdmEntities();
            var term = System.Web.HttpContext.Current.Request["term"].ToString();
            var VKelas = db.view_kelas.Where(d => d.kelas.Contains(term)).
                Select(d => new {
                    label = d.kelas,
                    value = d.kelas,
                    sks = d.credit_course
                    // id_lecture=d.id_lecture
                }).Distinct().Take(take).ToList();
            return Json(VKelas, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxListMataKuliah(int take)
        {
            var db = new stmtsdmEntities();
            var term = System.Web.HttpContext.Current.Request["term"].ToString();
            //var VKelas = db.view_kelas.Where(d => d.name_course.Contains(term)).Select(d => new
            //                   {
            //                       label=d.name_course,
            //                       value=d.name_course,
            //                       code_course=d.code_course
            //                   })
            //    .GroupBy(p => p.label,
            //                   p => p.code_course,
            //                   (key, g) => new
            //                   {
            //                       code_course = key,
            //                       label = g.ToList()
            //                   });
            var VKelas = (from b in db.view_kelas
                          where b.name_course.Contains(term) || b.kelas.Contains(term) || b.code_course.Contains(term)
                          group b by new {
                              b.code_course,
                              b.name_course,
                              b.kelas,
                              b.credit_course
                          } into z
                          select new {
                              name_course = z.Key.name_course,
                              code_course = z.Key.code_course,
                              kelas = z.Key.kelas,
                              credit_course = z.Key.credit_course
                          }).ToList();

            var kelas = VKelas.Distinct().Take(take).ToList();
            return Json(kelas, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region report daftar dosen
        public FileStreamResult DaftarStatusDosen()
        {
            var query = from d in db.tbl_dosen
                        join p in db.tbl_pegawai on d.pegawai_id equals p.id

                        join st in db.tbl_ref_status_aktif on d.status_aktif_id equals st.id into st_d
                        from _st_d in st_d.DefaultIfEmpty()

                        select new DosenSummary<string>() {
                            nik = d.kd_nik,
                            nidn = d.nidn,
                            nama = p.gelar_depan + " " + p.nama + " " + p.gelar_belakang,
                            data = _st_d.keterangan
                        };

            return GenerateSimpleDosenList(query.ToList(), "STATUS", "Daftar Status Dosen");
        }

        public FileStreamResult DaftarJenisDosen()
        {
            var query = from d in db.tbl_dosen
                        join p in db.tbl_pegawai on d.pegawai_id equals p.id

                        join st in db.tbl_ref_status_dosen on d.status_dosen_id equals st.id into st_d
                        from _st_d in st_d.DefaultIfEmpty()

                        select new DosenSummary<string>() {
                            nik = d.kd_nik,
                            nidn = d.nidn,
                            nama = p.gelar_depan + " " + p.nama + " " + p.gelar_belakang,
                            data = _st_d.status_dosen
                        };

            return GenerateSimpleDosenList(query.ToList(), "JENIS", "Daftar Jenis Dosen");
        }

        public FileStreamResult DaftarPendidikanDosen()
        {
            var raw = (from d in db.tbl_dosen
                       join p in db.tbl_pegawai on d.pegawai_id equals p.id

                       join j in db.tbl_dosen_jenjang on d.id equals j.dosen_id into d_j
                       from _d_j in d_j.DefaultIfEmpty()

                       join k in db.tbl_ref_pendidikan on _d_j.pendidikan_id equals k.id into _dj_k
                       from j_k in _dj_k.DefaultIfEmpty()

                       select new DosenSummary<string>() {
                           nik = d.kd_nik,
                           nidn = d.nidn,
                           nama = p.gelar_depan + " " + p.nama + " " + p.gelar_belakang,
                           data = j_k.tingkat_pendidikan
                       }).ToList();

            return GenerateSimpleDosenList(raw, "PENDIDIKAN", "Daftar Pendidikan Dosen");
        }

        public FileStreamResult DaftarJabatanDosen()
        {
            var raw = (from d in db.tbl_dosen
                       join p in db.tbl_pegawai on d.pegawai_id equals p.id

                       join j in db.tbl_dosen_jenjang on d.id equals j.dosen_id into d_j
                       from _d_j in d_j.DefaultIfEmpty()

                       join k in db.tbl_ref_jabatan_fungsional on _d_j.jabatan_fungsional_id equals k.id into _dj_k
                       from j_k in _dj_k.DefaultIfEmpty()

                       select new DosenSummary<string>() {
                           nik = d.kd_nik,
                           nidn = d.nidn,
                           nama = p.gelar_depan + " " + p.nama + " " + p.gelar_belakang,
                           data = j_k.jabatan_fungsional
                       }).ToList();

            return GenerateSimpleDosenList(raw, "JABATAN AKADEMIK", "Daftar Jabatan Akademik Dosen");
        }

        public FileStreamResult DaftarUsiaDosen()
        {
            var raw = (from d in db.tbl_dosen
                       join p in db.tbl_pegawai on d.pegawai_id equals p.id

                       select new DosenSummary<Nullable<DateTime>>() {
                           nik = d.kd_nik,
                           nidn = d.nidn,
                           nama = p.gelar_depan + " " + p.nama + " " + p.gelar_belakang,
                           data = p.tanggal_lahir
                       }).ToList();
            var source = new List<DosenSummary<string>>();

            DateTime today = DateTime.Today;
            foreach (var dosen in raw) {
                var copy = new DosenSummary<string>() {
                    nik = dosen.nik,
                    nidn = dosen.nidn,
                    nama = dosen.nama,
                    data = ""
                };

                if (dosen.data.HasValue) {
                    if (dosen.data.Value > today) {
                        copy.data = "";
                    }else{
                        int age = today.Year - dosen.data.Value. Year;
                        if (dosen.data.Value > today.AddYears(-age))
                            age--;

                        copy.data = age.ToString();
                    }
                }

                source.Add(copy);
            }

            return GenerateSimpleDosenList(source, "USIA", "Daftar Usia Dosen");
        }

        public FileStreamResult DaftarHomebaseDosen()
        {
            var query = from d in db.tbl_dosen
                        join p in db.tbl_pegawai on d.pegawai_id equals p.id

                        join st in db.view_daftar_prodi on d.homebase equals st.kode_dikti into st_d
                        from _st_d in st_d.DefaultIfEmpty()

                        select new DosenSummary<string>() {
                            nik = d.kd_nik,
                            nidn = d.nidn,
                            nama = p.gelar_depan + " " + p.nama + " " + p.gelar_belakang,
                            data = _st_d.jenjang + " - " + _st_d.prodi
                        };

            return GenerateSimpleDosenList(query.ToList(), "HOMEBASE", "Daftar Homebase Dosen");
        }

        public class DosenSummary<T>
        {
            public string nik { get; set; }
            public string nidn { get; set; }
            public string nama { get; set; }
            public T data { get; set; }
        }

        private FileStreamResult GenerateSimpleDosenList(List<DosenSummary<string>> source, string dataTitle, string filename = "Daftar Dosen")
        {
            var ms = new System.IO.MemoryStream();
            using (var sl = new SpreadsheetLight.SLDocument()) {
                var rowNum = 1;

                //write header
                sl.SetCellValue(rowNum, 1, "NO");
                sl.SetCellValue(rowNum, 2, "NIK");
                sl.SetCellValue(rowNum, 3, "NIDN");
                sl.SetCellValue(rowNum, 4, "NAMA");
                sl.SetCellValue(rowNum, 5, dataTitle);

                //write data

                for (int i = 0; i < source.Count; i++) {
                    rowNum++;//nextrow
                    var item = source[i];

                    sl.SetCellValue(rowNum, 1, i + 1);
                    sl.SetCellValue(rowNum, 2, item.nik);
                    sl.SetCellValue(rowNum, 3, item.nidn);
                    sl.SetCellValue(rowNum, 4, item.nama);

                    sl.SetCellValue(rowNum, 5, item.data);
                }

                //add filter
                sl.Filter(1, 1, rowNum, 5);
                sl.SetColumnWidth(3, 12.0);
                sl.SetColumnWidth(4, 45.0);
                sl.SetColumnWidth(5, 45.0);

                sl.SaveAs(ms);
            }
            // this is important. Otherwise you get an empty file
            // (because you'd be at EOF after the stream is written to, I think...).
            ms.Position = 0;

            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename + ".xlsx");
        }
        #endregion
    }
}

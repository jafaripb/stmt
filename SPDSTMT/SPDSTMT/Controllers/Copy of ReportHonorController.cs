﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPDSTMT.Models;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using SPDSTMT.Helper;
using System.IO;
using System.Data;
using Microsoft.Reporting.WinForms;




namespace SPDSTMT.Controllers
{
    public class ReportHonorController : Controller
    {
        //
        // GET: /ReportHonor/
        private string user = "User";
        string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
        DateTime NOW = DateTime.Now;
        internal string folderTemplateUrl = @"~/template";
        internal string folderTempUrl = @"~/temp";
        private int reportid = 0;
        ResultMessage resultMessage = new ResultMessage();
        public ActionResult Index()
        {
            ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public ActionResult KelasReguler()
        {
            ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public ActionResult KelasBP3IP()
        {
            ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public ActionResult KelasInternasional()
        {
            ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public class DataTableDataKehadiranDosen : DataTableData
        {
            public List<view_honor_dosen> data { get; set; }
        }

        public JsonResult GetListRekapitulasiHonorDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string KdNik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string NamaDosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
           // string StatusDosen = System.Web.HttpContext.Current.Request["status_dosen"].ToString();
            string StartDate = System.Web.HttpContext.Current.Request["start_date"].ToString();
            string EndDate = System.Web.HttpContext.Current.Request["end_date"].ToString();
           DataTableDataKehadiranDosen dt = new DataTableDataKehadiranDosen();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }

            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());            
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    var a = db.view_honor_dosen.AsQueryable();
                    dt.recordsTotal = a.Count();
                    if (KdNik != string.Empty) a = a.Where(x => x.kd_nik.Contains(KdNik));
                    if (NamaDosen != string.Empty) a = a.Where(x => x.nama_dosen.Contains(NamaDosen));
                   // if (StatusDosen != string.Empty) a = a.Where(x => x.status_dosen_id == Convert.ToInt32(StatusDosen));
                    if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    {
                        var from = DateTime.Parse(StartDate);
                        var to = DateTime.Parse(EndDate);
                        a = a.Where(x => x.date_document >= from && x.date_document <= to);
                    }
                    if (sortColumn == 1)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik);
                    }
                     if (sortColumn == 2)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.nama_dosen);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_dosen);
                    }

                    dt.data = a.Skip(start).Take(length).ToList();
                    dt.recordsFiltered = a.Count();

                }
            }
            catch { }
            finally
            {}
                return Json(dt, JsonRequestBehavior.AllowGet);
            
        }

        public JsonResult GetListSPCHonorDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string KdNik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string NamaDosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
            // string StatusDosen = System.Web.HttpContext.Current.Request["status_dosen"].ToString();
            string StartDate = System.Web.HttpContext.Current.Request["start_date"].ToString();
            string EndDate = System.Web.HttpContext.Current.Request["end_date"].ToString();
            DataTableDataKehadiranDosen dt = new DataTableDataKehadiranDosen();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }

            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    var a = db.view_honor_dosen.AsQueryable();
                    dt.recordsTotal = a.Count();
                    if (KdNik != string.Empty) a = a.Where(x => x.kd_nik.Contains(KdNik));
                    if (NamaDosen != string.Empty) a = a.Where(x => x.nama_dosen.Contains(NamaDosen));
                    // if (StatusDosen != string.Empty) a = a.Where(x => x.status_dosen_id == Convert.ToInt32(StatusDosen));
                    if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    {
                        var from = DateTime.Parse(StartDate);
                        var to = DateTime.Parse(EndDate);
                        a = a.Where(x => x.date_document >= from && x.date_document <= to);
                    }
                    if (sortColumn == 1)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik);
                    }
                    if (sortColumn == 2)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.nama_dosen);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_dosen);
                    }

                    dt.data = a.Skip(start).Take(length).ToList();
                    dt.recordsFiltered = a.Count();

                }
            }
            catch { }
            finally
            { }
            return Json(dt, JsonRequestBehavior.AllowGet);

        }

        public string ConvertToNamaBulan(int i)
        {
            string val="";
            if (i == 1) val = "Januari";
            else if (i == 2) val = "Febuari";
            else if (i == 3) val = "Maret";
            else if (i == 4) val = "April";
            else if (i == 5) val = "Mei";
            else if (i == 6) val = "Juni";
            else if (i == 7) val = "Juli";
            else if (i == 8) val = "Agustus";
            else if (i == 9) val = "September";
            else if (i == 10) val = "Oktober";
            else if (i == 11) val = "November";
            else if (i == 12) val = "Desember";
            return val;
        }

        public JsonResult GetExcel(string StartDate,string EndDate)
        {
            var from2 = DateTime.Parse(StartDate);
            var to = DateTime.Parse(EndDate);
            string fullPath = Path.Combine(Server.MapPath(folderTemplateUrl), "template1.xlsx");
            string dateNow = DateTime.Now.ToString("yyyyMMddhhmmss");
            //string fileName = "Report_" + user + "_" + from2.ToString("d").Replace("/", "-") + "_" + to.ToString("d").Replace("/", "-") + ".xlsx";
            string fileName = "Report_" + user + "_" + dateNow + ".xlsx";
            
            int statusCreate = 0;
            Session["reportid"] = 0;
            string fileTempReport = Path.Combine(Server.MapPath(folderTempUrl), fileName  );
            try
            {

                System.IO.File.Copy(fullPath, fileTempReport, true);
            
            var db = new stmtsdmEntities();
           // MemoryStream ms = new MemoryStream();
            //FileStream file = new FileStream(fileTempReport, FileMode.Open, FileAccess.Read);
            //file.CopyTo(ms);
            //var from2 = DateTime.Parse("10/1/2015");
            //var to = DateTime.Parse("10/28/2015");
            

            var lstDosen = (from c in db.view_honor_dosen
                            where (c.date_document >= from2 && c.date_document <= to)
                            group c by new
                            {
                                c.kd_nik,
                                c.nama_dosen,
                                c.jenjang_kelompok,
                                c.status_dosen,
                                c.status_dosen_id,
                                c.transpor_per_hari,
                                c.honor_per_sks,
                                c.pegawai_id
                            } into h
                            select new
                            {
                                h.Key.kd_nik,
                                h.Key.nama_dosen,
                                h.Key.jenjang_kelompok,
                                h.Key.status_dosen,
                                h.Key.status_dosen_id,
                                h.Key.transpor_per_hari,
                                h.Key.honor_per_sks,
                                h.Key.pegawai_id
                            }).ToList();
            SLDocument sl = new SLDocument(fileTempReport, "Sheet1");
            int no = 1;
            int row = 9;
            int rowSheet2 = 8;
            string A5 = "Bulan " + ConvertToNamaBulan(to.Month) + " " + to.Year + "(" + from2.ToShortDateString() + " s/d " + to.ToShortDateString() + ")";
            string Sheet2A4 = "Priode I " + "(" + from2.ToShortDateString() + " s/d " + to.ToShortDateString() + ")";
            sl.SetCellValue("A5", A5);
            sl.SelectWorksheet("Sheet2");
            sl.SetCellValue("A4", Sheet2A4);
            foreach (var item in lstDosen)
            {
                sl.SelectWorksheet("Sheet1");
                string NIK = item.kd_nik;
                string NamaDosen = item.nama_dosen;
                string Jenjang = item.jenjang_kelompok;
                string StatusDosen = item.status_dosen;
                sl.SetCellValue("A"+row, no);
                sl.SetCellValue("B" + row, NIK);
                sl.SetCellValue("C" + row, NamaDosen);
                sl.SetCellValue("D" + row, Jenjang);
                sl.SetCellValue("E" + row, StatusDosen);
                
                var tanggalKehadiran = (from c in db.view_honor_dosen
                                     where (c.date_document >= from2 && c.date_document <= to && c.kd_nik==item.kd_nik)
                                     group c by new { c.date_document } into h
                                      select new { h.Key.date_document }).ToList();
                int TotalKehadiran = tanggalKehadiran.Count();
                decimal totalHonorMengajar = 0;
                decimal totalHonor = 0;
                var lstMataKuliahDosen = (from c in db.view_honor_dosen
                                          where (c.date_document >= from2 && c.date_document <= to && c.kd_nik == item.kd_nik)
                                          group c by new { c.kelas,c.name_course,
                                              c.credit_course,c.honor_per_sks,
                                              c.honor_tunjangan                         
                                          } into h
                                          select new { h.Key.kelas,
                                                       h.Key.name_course,
                                                       h.Key.credit_course,
                                                       h.Key.honor_per_sks,
                                                       h.Key.honor_tunjangan,
                                                       jumlahHadir= h.Count()
                                          }).ToList();
                int TotalSksDosenFungsional = 0;//minal 9 sks
                int transportHadir = 0;
                foreach (var itemMK in lstMataKuliahDosen)
                {                    
                    var lstTglMK = (from c in db.view_honor_dosen
                                 where (c.date_document >= from2 && c.date_document <= to && c.kd_nik == item.kd_nik && c.kelas==itemMK.kelas)
                                 select new
                                 {
                                    c.date_document
                                 }).ToList();
                    string tglMk = "";
                    
                    TotalSksDosenFungsional = TotalSksDosenFungsional + itemMK.credit_course??0;
                    foreach (var itmTglMk in lstTglMK)
                    {
                        tglMk =tglMk+itmTglMk.date_document.Value.ToShortDateString()+" ";

                        if (item.status_dosen_id == 1 || item.status_dosen_id == 2 || item.status_dosen_id == 3)
                        {
                            string zz = itmTglMk.date_document.Value.DayOfWeek.ToString();
                            string sabtu = DayOfWeek.Saturday.ToString();
                            string minggu = DayOfWeek.Sunday.ToString();
                            if (zz == sabtu || zz == minggu)
                            {
                                transportHadir = transportHadir + 1;
                            }
                        }
                        else
                        {
                            transportHadir = transportHadir + 1;
                        }
                    }

                    if ( item.status_dosen_id == 2 || item.status_dosen_id == 3)
                    {
                        sl.SetCellValue("F" + row, itemMK.name_course);
                        sl.SetCellValue("G" + row, itemMK.credit_course.Value);
                        sl.SetCellValue("H" + row, itemMK.kelas);
                        sl.SetCellValue("I" + row, tglMk);
                        sl.SetCellValue("J" + row, itemMK.jumlahHadir);
                        sl.SetCellValue("K" + row, itemMK.honor_per_sks ?? 0);
                        sl.SetCellValue("L" + row, item.transpor_per_hari ?? 0);
                    }
                    else
                    {
                        sl.SetCellValue("F" + row, itemMK.name_course);
                        sl.SetCellValue("G" + row, itemMK.credit_course.Value);
                        sl.SetCellValue("H" + row, itemMK.kelas);
                        sl.SetCellValue("I" + row, tglMk);
                        sl.SetCellValue("J" + row, itemMK.jumlahHadir);
                        sl.SetCellValue("K" + row, itemMK.honor_per_sks??0);
                        sl.SetCellValue("L" + row, item.transpor_per_hari ?? 0);
                        //sl.SetCellValue("M" + row, transportHadir);
                        sl.SetCellValue("N" + row, itemMK.honor_per_sks*itemMK.credit_course??0*itemMK.jumlahHadir);
                        sl.SetCellValue("O" + row, itemMK.honor_tunjangan??0* itemMK.jumlahHadir);
                        sl.SetCellValue("P" + row, "=N" + row + "+O" + row);
                        decimal hitungHonorMengajar = itemMK.honor_per_sks * itemMK.credit_course ?? 0 * itemMK.jumlahHadir;
                        decimal hitungHonorTunjangan=itemMK.honor_tunjangan ?? 0 * itemMK.jumlahHadir;
                        totalHonorMengajar = totalHonorMengajar + hitungHonorMengajar + hitungHonorTunjangan;
                       // sl.SetCellValue("Q" + row, transportHadir*item.transpor_per_hari??0);
                    }
                    row++;
                }
                totalHonor =  totalHonorMengajar + (transportHadir * item.transpor_per_hari ?? 0);
                if ( item.status_dosen_id == 2 || item.status_dosen_id == 3)
                {
                    decimal hitungHonorMengajar = 0;

                    if (TotalSksDosenFungsional - 9 > 0)
                    {
                        sl.SetCellValue("N" + (row - lstMataKuliahDosen.Count()), (TotalSksDosenFungsional - 9) * item.honor_per_sks ?? 0);
                        hitungHonorMengajar = (TotalSksDosenFungsional - 9) * item.honor_per_sks??0;
                    }
                    else
                    {
                        sl.SetCellValue("N" + row,0);
                    }
                    sl.SetCellValue("O" + (row - lstMataKuliahDosen.Count()), 0);
                    sl.SetCellValue("P" + (row - lstMataKuliahDosen.Count()), "=N" + (row - lstMataKuliahDosen.Count()) + "+O" + (row - lstMataKuliahDosen.Count()));
                    sl.SetCellValue("Q" + (row - lstMataKuliahDosen.Count()), transportHadir * item.transpor_per_hari ?? 0);
                    decimal transpor=transportHadir * item.transpor_per_hari ?? 0;
                    totalHonor = hitungHonorMengajar + transpor;
                }
                sl.SetCellValue("M" + (row - lstMataKuliahDosen.Count()), transportHadir);
                sl.SetCellValue("Q" + (row - lstMataKuliahDosen.Count()), transportHadir * item.transpor_per_hari ?? 0);
                sl.SetCellValue("R" + (row - lstMataKuliahDosen.Count()), totalHonor);

                sl.SelectWorksheet("Sheet2");

                sl.SetCellValue("A" + rowSheet2, no);
                sl.SetCellValue("B" + rowSheet2, NIK);
                sl.SetCellValue("C" + rowSheet2, NamaDosen);
                sl.SetCellValue("E" + rowSheet2, totalHonor);
                if (item.pegawai_id != null)
                {
                    tbl_rekening rekening = db.tbl_rekening.Where(d => d.pegawai_id == item.pegawai_id).FirstOrDefault();
                    if (rekening != null)
                    {
                        sl.SetCellValue("D" + rowSheet2, rekening.no_rekening.ToString());
                        sl.SetCellValue("G" + rowSheet2, totalHonor);
                        sl.SetCellValue("H" + rowSheet2, rekening.nama_bank_cabang.ToString());
                        sl.SetCellValue("I" + rowSheet2, rekening.atas_nama.ToString());
                        sl.SetCellValue("J" + rowSheet2, rekening.kode_bank.ToString());
                    }
                }
                rowSheet2++;
                no++;
            }
            

            sl.SaveAs(fileTempReport);
            statusCreate = 1;
            }
            catch (Exception ex) { statusCreate = 0; }

            using (var db = new stmtsdmEntities())
            {
                tbl_generate_report GenReport = new tbl_generate_report();
                GenReport.nama_file = fileName;
                GenReport.create_status = statusCreate;
                GenReport.create_by = user;
                GenReport.create_on = NOW.Date;
                db.tbl_generate_report.Add(GenReport);
                db.SaveChanges();
                Session["reportid"] = GenReport.id;
                reportid = GenReport.id;
            }
            //sl.SetCellValue("E6", "Let's party!!!!111!!!1");

            //sl.SelectWorksheet("Sheet3");
            //sl.SetCellValue("E6", "Before anyone calls the popo!");

            //sl.AddWorksheet("DanceFloor");
            //sl.SetCellValue("B4", "Who let the dogs out?");
            //sl.SetCellValue("B5", "Woof!");
            //sl.SaveAs(ms)
            
           /// return File(fileTempReport, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "mytestfile.xlsx");
           // return File(ms, "application/vnd.ms-excel", "mytestfile.xls");
            return Json(fileName, JsonRequestBehavior.AllowGet);
        }

        public class MessageStatusGenerate
        {
            public tbl_generate_report tblGenReport { get; set; }
            public int status { get; set; }
        }
        public JsonResult GenerateStatus()
        {
            MessageStatusGenerate stt = new MessageStatusGenerate();
            stt.status = 0;
            using (var db = new stmtsdmEntities())
            {
                int wow = reportid;
                var sss = Session["reportid"];
                if (Session["reportid"] != null)
                {
                    int reportId =Convert.ToInt32( Session["reportid"]);
                    tbl_generate_report Report = db.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();
                    if (Report.id > 0)
                    {
                        stt.status = 1;
                        stt.tblGenReport = Report;
                    }
                }
            }

            return Json(stt, JsonRequestBehavior.AllowGet);
        }
      
        public void generateReport()
        {
            string fullPath = Path.Combine(Server.MapPath(folderTemplateUrl), "Book1.xlsx");           
            string day = NOW.Day.ToString();
            string month = NOW.Month.ToString();
            string year = NOW.Year.ToString();
            string fileName = "Book1" + "-" + day + "-" + month + "-" + year;

            string fileTempReport = Path.Combine(Server.MapPath(folderTempUrl), fileName + ".xlsx");
            try
            {

                System.IO.File.Copy(fullPath, fileTempReport, true);
            }
            catch (Exception ex) { }
            generateReportXLS(fileTempReport, fileName, "Sheet1");
            
             
        }

        public void generateReportXLS(string urlfileXls, string fileName, string sheetName)
        {
            Session["StatusReport1"] = null;
            resultMessage = null;
            int status = 0;
            string message = "";
            try
            {
                //FileStream file = new FileStream(urlfileXls, FileMode.Open, FileAccess.Read);

                SLDocument sl = new SLDocument(urlfileXls, sheetName);
                var db = new stmtsdmEntities();
                

                sl.SetCellValue("E6", "Let's party!!!!111!!!1");

                sl.SelectWorksheet("Sheet3");
                sl.SetCellValue("E6", "Before anyone calls the popo!");

                sl.AddWorksheet("DanceFloor");
                sl.SetCellValue("B4", "Who let the dogs out?");
                sl.SetCellValue("B5", "Woof!");



               // FileStream file2 = new FileStream(fileTempReport, FileMode.Create);
                sl.SaveAs(urlfileXls);
                //file2.Close();

                status = 1;
                message = fileName;
            }
            catch (Exception ex)
            {
                status = 0;
                message = helper.ErrorMessageWithElement(ex.ToString());
            }
            finally
            {
                resultMessage.status = status;
                resultMessage.message = message;
                Response.AddHeader("Content-Disposition", "inline;filename=" + fileName);
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.WriteFile("");
                Response.End();
            }
            Session["StatusReport1"] = resultMessage;
        }

        public ActionResult ReportRdlc()
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "Report1.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<view_honor_dosen> cm = new List<view_honor_dosen>();
            using (stmtsdmEntities dc = new stmtsdmEntities())
            {
                var from = DateTime.Parse("10/1/2015");
                var to = DateTime.Parse("10/28/2015");

                cm = dc.view_honor_dosen.OrderBy(d=>d.kd_nik).Where(d => d.date_document >= from
                    && d.date_document <= to).ToList();
            }
            ReportDataSource rd = new ReportDataSource("DataSet1", cm);
            lr.DataSources.Add(rd);
            string reportType = "pdf";
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

           "<DeviceInfo>" +
          "  <OutputFormat>PDF</OutputFormat>" +
          "  <PageWidth>11in</PageWidth>" +
          "  <PageHeight>8.5in</PageHeight>" +
          "  <MarginTop>0.25in</MarginTop>" +
          "  <MarginLeft>0.25in</MarginLeft>" +
          "  <MarginRight>0.25in</MarginRight>" +
          "  <MarginBottom>0.25in</MarginBottom>" +
          "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }
    }
}

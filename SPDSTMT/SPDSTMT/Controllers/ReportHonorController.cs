﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPDSTMT.Models;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using SPDSTMT.Helper;
using System.IO;
using System.Data;
using Microsoft.Reporting.WinForms;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using SPDSTMT.Models.DataSetSTMTTableAdapters;
using System.Reflection;
using System.Text;



namespace SPDSTMT.Controllers
{
    public class ReportHonorController : BaseController
    {
        internal string folderTemplateUrl = @"~/template";
        internal string folderTemplateUrlPdf = @"~/temp/cache";
        internal string folderTempUrl = @"~/temp";
        private int reportid = 0;
        ResultMessage resultMessage = new ResultMessage();
        public ActionResult Index()
        {
            //ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public ActionResult KelasReguler()
        {
            decimal value=10000000;           
            var f = new NumberFormatInfo { NumberGroupSeparator = "." };
            var s = value.ToString("C", CultureInfo.CreateSpecificCulture("fr-FR")); 
           // ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public ActionResult KelasBP3IP()
        {
           // ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public ActionResult KelasInternasional()
        {
            ///ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public class DataTableDataKehadiranDosen : DataTableData
        {
            public List<view_honor_dosen> data { get; set; }
        }

        public JsonResult GetListRekapitulasiHonorDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string KdNik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string NamaDosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
           // string StatusDosen = System.Web.HttpContext.Current.Request["status_dosen"].ToString();
            string StartDate = System.Web.HttpContext.Current.Request["start_date"].ToString();
            string EndDate = System.Web.HttpContext.Current.Request["end_date"].ToString();
           DataTableDataKehadiranDosen dt = new DataTableDataKehadiranDosen();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }

            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());            
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    var a = db.view_honor_dosen.AsQueryable();
                    dt.recordsTotal = a.Count();
                    if (KdNik != string.Empty) a = a.Where(x => x.kd_nik.Contains(KdNik));
                    if (NamaDosen != string.Empty) a = a.Where(x => x.nama_dosen.Contains(NamaDosen));
                   // if (StatusDosen != string.Empty) a = a.Where(x => x.status_dosen_id == Convert.ToInt32(StatusDosen));
                    if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    {
                        var from = DateTime.Parse(StartDate);
                        var to = DateTime.Parse(EndDate);
                        a = a.Where(x => x.date_document >= from && x.date_document <= to);
                    }
                    if (sortColumn == 1)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik);
                    }
                     if (sortColumn == 2)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.nama_dosen);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_dosen);
                    }

                    dt.data = a.Skip(start).Take(length).ToList();
                    dt.recordsFiltered = a.Count();

                }
            }
            catch { }
            finally
            {}
                return Json(dt, JsonRequestBehavior.AllowGet);
            
        }

        public JsonResult GetListSPCHonorDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string KdNik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string NamaDosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
            // string StatusDosen = System.Web.HttpContext.Current.Request["status_dosen"].ToString();
            string StartDate = System.Web.HttpContext.Current.Request["start_date"].ToString();
            string EndDate = System.Web.HttpContext.Current.Request["end_date"].ToString();
            DataTableDataKehadiranDosen dt = new DataTableDataKehadiranDosen();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }

            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    var a = db.view_honor_dosen.AsQueryable();
                    dt.recordsTotal = a.Count();
                    if (KdNik != string.Empty) a = a.Where(x => x.kd_nik.Contains(KdNik));
                    if (NamaDosen != string.Empty) a = a.Where(x => x.nama_dosen.Contains(NamaDosen));
                    // if (StatusDosen != string.Empty) a = a.Where(x => x.status_dosen_id == Convert.ToInt32(StatusDosen));
                    if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                    {
                        var from = DateTime.Parse(StartDate);
                        var to = DateTime.Parse(EndDate);
                        a = a.Where(x => x.date_document >= from && x.date_document <= to);
                    }
                    if (sortColumn == 1)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik);
                    }
                    if (sortColumn == 2)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.nama_dosen);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_dosen);
                    }

                    dt.data = a.Skip(start).Take(length).ToList();
                    dt.recordsFiltered = a.Count();

                }
            }
            catch { }
            finally
            { }
            return Json(dt, JsonRequestBehavior.AllowGet);

        }

        public string ConvertToNamaBulan(int i)
        {
            string val="";
            if (i == 1) val = "Januari";
            else if (i == 2) val = "Febuari";
            else if (i == 3) val = "Maret";
            else if (i == 4) val = "April";
            else if (i == 5) val = "Mei";
            else if (i == 6) val = "Juni";
            else if (i == 7) val = "Juli";
            else if (i == 8) val = "Agustus";
            else if (i == 9) val = "September";
            else if (i == 10) val = "Oktober";
            else if (i == 11) val = "November";
            else if (i == 12) val = "Desember";
            return val;
        }

        public string ConvertToNamaBulanSingkat(int i)
        {
            string val = "";
            if (i == 1) val = "Jan";
            else if (i == 2) val = "Feb";
            else if (i == 3) val = "Mar";
            else if (i == 4) val = "Apr";
            else if (i == 5) val = "Mei";
            else if (i == 6) val = "Jun";
            else if (i == 7) val = "Jul";
            else if (i == 8) val = "Agus";
            else if (i == 9) val = "Sept";
            else if (i == 10) val = "Okt";
            else if (i == 11) val = "Nov";
            else if (i == 12) val = "Des";
            return val;
        }

        public JsonResult GetExcel(string StartDate,string EndDate)
        {
            var from2 = DateTime.Parse(StartDate);
            var to = DateTime.Parse(EndDate);
            string fullPath = Path.Combine(Server.MapPath(folderTemplateUrl), "template1.xlsx");
            string dateNow = DateTime.Now.ToString("yyyyMMddhhmmss");

            string CodeValue = user + dateNow;
            //string fileName = "Report_" + user + "_" + from2.ToString("d").Replace("/", "-") + "_" + to.ToString("d").Replace("/", "-") + ".xlsx";
            string fileName = "Report_" + CodeValue + ".xlsx";
            tbl_generate_report GenReport = new tbl_generate_report();
            var db = new stmtsdmEntities();
            GenReport.nama_file = "Report_" + CodeValue;
            GenReport.create_status = 0;
            GenReport.create_by = user;
            GenReport.report_start = from2;
            GenReport.report_end = to;
            GenReport.create_on = NOW.Date;
            GenReport.code_value = CodeValue;
            db.tbl_generate_report.Add(GenReport);
            db.SaveChanges();
            
            int statusCreate = 0;
            Session["reportid"] = 0;
            Session["message"] = "";
            Session["jumReport"] = 0;
            Session["loadingReport"] = 0;
            string fileTempReport = Path.Combine(Server.MapPath(folderTempUrl), fileName  );
            try
            {

                System.IO.File.Copy(fullPath, fileTempReport, true);


           var lstDosen = (from c in db.view_honor_dosen
                            where (c.date_document >= from2 && c.date_document <= to)
                                               group c by new
                            {
                                c.kd_nik,
                                c.nama_dosen,
                                c.jenjang_kelompok,
                                c.status_dosen,
                                c.status_dosen_id,
                                c.transpor_per_hari,
                                c.honor_per_sks,
                                c.pegawai_id,
                                c.tanggal_masuk,
                                c.jabatan_fungsional,
                                c.tingkat_pendidikan,
                                c.tanggal_sk
                            } into h
                           select new 
                            {
                                h.Key.kd_nik,
                                h.Key.nama_dosen,
                                h.Key.jenjang_kelompok,
                                h.Key.status_dosen,
                                h.Key.status_dosen_id,
                                h.Key.transpor_per_hari,
                                h.Key.honor_per_sks,
                                h.Key.pegawai_id,
                                h.Key.tanggal_masuk,
                                h.Key.jabatan_fungsional,
                                h.Key.tingkat_pendidikan,
                                h.Key.tanggal_sk
                            }).ToList();
            SLDocument sl = new SLDocument(fileTempReport, "Sheet1");
            int no = 1;
            int row = 9;
            int rowSheet2 = 8;
            string A5 = "Bulan " + ConvertToNamaBulan(to.Month) + " " + to.Year + "(" + from2.ToShortDateString() + " s/d " + to.ToShortDateString() + ")";
            string Sheet2A4 = "Priode I " + "(" + from2.ToShortDateString() + " s/d " + to.ToShortDateString() + ")";
            sl.SetCellValue("A5", A5);
            sl.SelectWorksheet("Sheet2");
            sl.SetCellValue("A4", Sheet2A4);
            Session["jumReport"] = lstDosen.Count();
           
            foreach (var item in lstDosen)
            {
                Session["loadingReport"] = no;
                sl.SelectWorksheet("Sheet1");
                string NIK = item.kd_nik;
                string NamaDosen = item.nama_dosen;
                string Jenjang = item.jenjang_kelompok;
                string StatusDosen = item.status_dosen;
                sl.SetCellValue("A"+row, no);
                sl.SetCellValue("B" + row, NIK);
                sl.SetCellValue("C" + row, NamaDosen);
                sl.SetCellValue("D" + row, Jenjang);
                sl.SetCellValue("E" + row, StatusDosen);
                
                var tanggalKehadiran = (from c in db.view_honor_dosen
                                     where (c.date_document >= from2 && c.date_document <= to && c.kd_nik==item.kd_nik)
                                     group c by new { c.date_document } into h
                                      select new { h.Key.date_document }).ToList();
                int TotalKehadiran = tanggalKehadiran.Count();
                decimal totalHonorMengajar = 0;
                decimal totalHonor = 0;
                var lstMataKuliahDosen = (from c in db.view_honor_dosen
                                          where (c.date_document >= from2 && c.date_document <= to && c.kd_nik == item.kd_nik)
                                          group c by new  { 
                                              c.kelas,
                                              c.name_course,
                                              c.code_course,
                                              c.credit_course,
                                              c.honor_per_sks,
                                              c.honor_tunjangan                         
                                          }into h
                                          select new 
                                          {
                                                h.Key.kelas,
                                                h.Key.name_course,
                                                h.Key.credit_course,
                                                h.Key.honor_per_sks,
                                                h.Key.honor_tunjangan,
                                                h.Key.code_course,
                                                jumlahHadir= h.Count()
                                          }).ToList();
                int TotalSksDosenFungsional = 0;//minal 9 sks
                int transportHadir = 0;
                foreach (var itemMK in lstMataKuliahDosen)
                {                    
                    var lstTglMK = (from c in db.view_honor_dosen
                                 where (c.date_document >= from2 && c.date_document <= to && c.kd_nik == item.kd_nik && c.kelas==itemMK.kelas)
                                 select new
                                 {
                                    c.date_document
                                 }).ToList();
                    string tglMk = "";
                    
                    TotalSksDosenFungsional = TotalSksDosenFungsional + itemMK.credit_course??0;
                    foreach (var itmTglMk in lstTglMK)
                    {
                        tglMk =tglMk+itmTglMk.date_document.Value.ToShortDateString()+" ";

                        if (item.status_dosen_id == 1 || item.status_dosen_id == 2 || item.status_dosen_id == 3)
                        {
                            string zz = itmTglMk.date_document.Value.DayOfWeek.ToString();
                            string sabtu = DayOfWeek.Saturday.ToString();
                            string minggu = DayOfWeek.Sunday.ToString();
                            if (zz == sabtu || zz == minggu)
                            {
                                transportHadir = transportHadir + 1;
                            }
                        }
                        else
                        {
                            transportHadir = transportHadir + 1;
                        }
                    }

                    if ( item.status_dosen_id == 2 || item.status_dosen_id == 3)
                    {
                        sl.SetCellValue("F" + row, itemMK.name_course);
                        sl.SetCellValue("G" + row, itemMK.credit_course.Value);
                        sl.SetCellValue("H" + row, itemMK.kelas);
                        sl.SetCellValue("I" + row, tglMk);
                        sl.SetCellValue("J" + row, itemMK.jumlahHadir);
                        sl.SetCellValue("K" + row, itemMK.honor_per_sks ?? 0);
                        sl.SetCellValue("L" + row, item.transpor_per_hari ?? 0);
                    }
                    else
                    {
                        sl.SetCellValue("F" + row, itemMK.name_course);
                        sl.SetCellValue("G" + row, itemMK.credit_course.Value);
                        sl.SetCellValue("H" + row, itemMK.kelas);
                        sl.SetCellValue("I" + row, tglMk);
                        sl.SetCellValue("J" + row, itemMK.jumlahHadir);
                        sl.SetCellValue("K" + row, itemMK.honor_per_sks??0);
                        sl.SetCellValue("L" + row, item.transpor_per_hari ?? 0);
                        //sl.SetCellValue("M" + row, transportHadir);
                        sl.SetCellValue("N" + row, itemMK.honor_per_sks*itemMK.credit_course??0*itemMK.jumlahHadir);
                        sl.SetCellValue("O" + row, itemMK.honor_tunjangan??0* itemMK.jumlahHadir);
                        sl.SetCellValue("P" + row, "=N" + row + "+O" + row);
                        decimal hitungHonorMengajar = itemMK.honor_per_sks * itemMK.credit_course ?? 0 * itemMK.jumlahHadir;
                        decimal hitungHonorTunjangan=itemMK.honor_tunjangan ?? 0 * itemMK.jumlahHadir;
                        totalHonorMengajar = totalHonorMengajar + hitungHonorMengajar + hitungHonorTunjangan;
                       // sl.SetCellValue("Q" + row, transportHadir*item.transpor_per_hari??0);
                    }
                    row++;

                }


                totalHonor =  totalHonorMengajar + (transportHadir * item.transpor_per_hari ?? 0);
                if ( item.status_dosen_id == 2 || item.status_dosen_id == 3)
                {
                    decimal hitungHonorMengajar = 0;

                    if (TotalSksDosenFungsional - 9 > 0)
                    {
                        sl.SetCellValue("N" + (row - lstMataKuliahDosen.Count()), (TotalSksDosenFungsional - 9) * item.honor_per_sks ?? 0);
                        hitungHonorMengajar = (TotalSksDosenFungsional - 9) * item.honor_per_sks??0;
                    }
                    else
                    {
                        sl.SetCellValue("N" + row,0);
                    }
                    sl.SetCellValue("O" + (row - lstMataKuliahDosen.Count()), 0);
                    sl.SetCellValue("P" + (row - lstMataKuliahDosen.Count()), "=N" + (row - lstMataKuliahDosen.Count()) + "+O" + (row - lstMataKuliahDosen.Count()));
                    sl.SetCellValue("Q" + (row - lstMataKuliahDosen.Count()), transportHadir * item.transpor_per_hari ?? 0);
                    decimal transpor=transportHadir * item.transpor_per_hari ?? 0;
                    totalHonor = hitungHonorMengajar + transpor;
                }
                sl.SetCellValue("M" + (row - lstMataKuliahDosen.Count()), transportHadir);
                sl.SetCellValue("Q" + (row - lstMataKuliahDosen.Count()), transportHadir * item.transpor_per_hari ?? 0);
                sl.SetCellValue("R" + (row - lstMataKuliahDosen.Count()), totalHonor);

                sl.SelectWorksheet("Sheet2");

                sl.SetCellValue("A" + rowSheet2, no);
                sl.SetCellValue("B" + rowSheet2, NIK);
                sl.SetCellValue("C" + rowSheet2, NamaDosen);
                sl.SetCellValue("E" + rowSheet2, totalHonor);
                if (item.pegawai_id != null)
                {
                    tbl_rekening rekening = db.tbl_rekening.Where(d => d.pegawai_id == item.pegawai_id).FirstOrDefault();
                    if (rekening != null)
                    {
                        sl.SetCellValue("D" + rowSheet2, rekening.no_rekening.ToString());
                        sl.SetCellValue("G" + rowSheet2, totalHonor);
                        sl.SetCellValue("H" + rowSheet2, rekening.nama_bank_cabang.ToString());
                        sl.SetCellValue("I" + rowSheet2, rekening.atas_nama.ToString());
                        sl.SetCellValue("J" + rowSheet2, rekening.kode_bank.ToString());
                    }
                }
                rowSheet2++;
                no++;

                //buat slip gaji convert html ke pdf 
                //file buat pdf
                string strExeFile = string.Format(@"{0}\PDFTools\{1}", System.Web.HttpContext.Current.Request.MapPath("~"), "wkhtmltopdf.exe");  // Request.PhysicalApplicationPath + "PDFTools\\wkhtmltopdf.exe";
                string filenamepdf = "Slip_Honor_" + item.kd_nik + "_" + CodeValue + ".pdf";
                string fname = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filenamepdf);

                string fnameFolder = string.Format(@"{0}\temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"));

                view_honor_dosen vhDosen = new view_honor_dosen();
                vhDosen.kd_nik = item.kd_nik;
                vhDosen.nama_dosen = item.nama_dosen;
                vhDosen.tanggal_masuk = item.tanggal_masuk;
                vhDosen.status_dosen = item.status_dosen;
                vhDosen.tingkat_pendidikan = item.tingkat_pendidikan;
                vhDosen.jabatan_fungsional = item.jabatan_fungsional;
                vhDosen.jenjang_kelompok = item.jenjang_kelompok;
                vhDosen.transpor_per_hari = item.transpor_per_hari;
                vhDosen.tanggal_sk = item.tanggal_sk;

               List<viewHonorMatakKuliahDosen> lstvhMKDosen = new List<viewHonorMatakKuliahDosen>();
               foreach (var itemMKDosen in lstMataKuliahDosen)
               {
                   viewHonorMatakKuliahDosen MKDosen = new viewHonorMatakKuliahDosen();
                   //MKDosen.honor_per_sks = itemMKDosen.honor_per_sks;
                   MKDosen.jumlahHadir = itemMKDosen.jumlahHadir;
                  // MKDosen.honor_tunjangan = itemMKDosen.honor_tunjangan;
                   MKDosen.kelas = itemMKDosen.kelas;
                   MKDosen.code_course = itemMKDosen.code_course;
                   MKDosen.credit_course = itemMKDosen.credit_course;
                   lstvhMKDosen.Add(MKDosen);
               }

               string html = templateSlipHtml(vhDosen, lstvhMKDosen, transportHadir, from2, to, "2014/2015", "Semster Genap");
               string filename1 = "Slip_Honor_" + item.kd_nik + "_" + CodeValue + ".html";
                //string fname_html = string.Format(@"temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"), filename1);
                string fname_html = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filename1);
                System.IO.File.WriteAllText(fname_html, html);

                bool isFileExist = System.IO.File.Exists(fname);
                if (isFileExist)
                    System.IO.File.Delete(fname);
                helper.WebToPDFStreamLandScape(strExeFile,
                    filename1,
                    fnameFolder,
                    filenamepdf);


                int intTimeOut = 0;
                while (!System.IO.File.Exists(fname))
                {
                    System.Threading.Thread.Sleep(1000);
                    intTimeOut++;
                    if (intTimeOut == 10)
                    {
                        break;
                    }
                }

                tbl_generate_slip_honor genSlipHonor = new tbl_generate_slip_honor();
                genSlipHonor.create_on = NOW;
                genSlipHonor.create_by = user;
                genSlipHonor.generate_report_id = GenReport.id;
                genSlipHonor.nama_file = filenamepdf;
                genSlipHonor.nik = item.kd_nik;
                db.tbl_generate_slip_honor.Add(genSlipHonor);
                db.SaveChanges();
                //bool isFileHtmlExist = System.IO.File.Exists(fname_html);
                //if (isFileHtmlExist)
                //    System.IO.File.Delete(fname_html);
                Session["message"] = "ok";
            }
            

            sl.SaveAs(fileTempReport);
            statusCreate = 1;
            }
            catch (Exception ex) { statusCreate = 0; Session["message"] = ex.ToString(); Session["reportid"] = -1;  }

            using (var db2 = new stmtsdmEntities())
            {
                if (statusCreate == 1)
                {
                    tbl_generate_report upGenReport = db2.tbl_generate_report.Where(d => d.id == GenReport.id).FirstOrDefault();
                    upGenReport.create_status = statusCreate;
                    db2.SaveChanges();
                    Session["reportid"] = GenReport.id;
                    Session["message"] = "Generate Complate";
                }
                else
                {
                    Session["reportid"] = -1; 
                    Session["message"] = "Generate Fail";
                }

            }
           /// return File(fileTempReport, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "mytestfile.xlsx");
           // return File(ms, "application/vnd.ms-excel", "mytestfile.xls");
            return Json(fileName, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GenerateReport(string StartDate, string EndDate, int StatusDosenId, string TahunAjaran,string Semester)
        {
            var from2 = DateTime.Parse(StartDate);
            var to = DateTime.Parse(EndDate);
            string fullPath = Path.Combine(Server.MapPath(folderTemplateUrl), "template1.xlsx");
            string dateNow = DateTime.Now.ToString("yyyyMMddhhmmss");

            string CodeValue = user + dateNow;
            //string fileName = "Report_" + user + "_" + from2.ToString("d").Replace("/", "-") + "_" + to.ToString("d").Replace("/", "-") + ".xlsx";
            string fileName = "Report_" + CodeValue + ".xlsx";
            tbl_generate_report GenReport = new tbl_generate_report();
            var db = new stmtsdmEntities();
            GenReport.nama_file = fileName;
            GenReport.create_status = 0;
            GenReport.create_by = user;
            GenReport.report_start = from2;
            GenReport.report_end = to;
            GenReport.create_on = NOW.Date;
            GenReport.code_value = CodeValue;
            db.tbl_generate_report.Add(GenReport);
            db.SaveChanges();

            int statusCreate = 0;
            Session["reportid"] = 0;
            Session["message"] = "";
            Session["jumReport"] = 0;
            Session["loadingReport"] = 0;
            string fileTempReport = Path.Combine(Server.MapPath(folderTempUrl), fileName);
            try
            {

                System.IO.File.Copy(fullPath, fileTempReport, true);
                var lstDosen = (from c in db.view_honor_dosen
                                where (c.date_document >= from2 && c.date_document <= to )
                                group c by new
                                {
                                    c.kd_nik,
                                    c.nama_dosen,
                                    c.jenjang_kelompok,
                                    c.status_dosen,
                                    c.status_dosen_id,
                                    c.transpor_per_hari,
                                    c.honor_per_sks,
                                    c.pegawai_id,
                                    c.tanggal_masuk,
                                    c.jabatan_fungsional,
                                    c.tingkat_pendidikan,
                                    c.tanggal_sk
                                } into h
                                select new
                                {
                                    h.Key.kd_nik,
                                    h.Key.nama_dosen,
                                    h.Key.jenjang_kelompok,
                                    h.Key.status_dosen,
                                    h.Key.status_dosen_id,
                                    h.Key.transpor_per_hari,
                                    h.Key.honor_per_sks,
                                    h.Key.pegawai_id,
                                    h.Key.tanggal_masuk,
                                    h.Key.jabatan_fungsional,
                                    h.Key.tingkat_pendidikan,
                                    h.Key.tanggal_sk
                                }).ToList();
                SLDocument sl = new SLDocument(fileTempReport, "Sheet1");
                int no = 1;
                int row = 9;
                int rowSheet2 = 8;
                string A5 = "Bulan " + ConvertToNamaBulan(to.Month) + " " + to.Year + "(" + from2.ToShortDateString() + " s/d " + to.ToShortDateString() + ")";
                string Sheet2A4 = "Priode I " + "(" + from2.ToShortDateString() + " s/d " + to.ToShortDateString() + ")";
                sl.SetCellValue("A5", A5);
                sl.SelectWorksheet("Sheet2");
                sl.SetCellValue("A4", Sheet2A4);
                Session["jumReport"] = lstDosen.Count();

                foreach (var item in lstDosen)
                {
                    Session["loadingReport"] = no;
                    sl.SelectWorksheet("Sheet1");
                    string NIK = item.kd_nik;
                    string NamaDosen = item.nama_dosen;
                    string Jenjang = item.jenjang_kelompok;
                    string StatusDosen = item.status_dosen;
                    sl.SetCellValue("A" + row, no);
                    sl.SetCellValue("B" + row, NIK);
                    sl.SetCellValue("C" + row, NamaDosen);
                    sl.SetCellValue("D" + row, Jenjang);
                    sl.SetCellValue("E" + row, StatusDosen);

                    var tanggalKehadiran = (from c in db.view_honor_dosen
                                            where (c.date_document >= from2 && c.date_document <= to && c.kd_nik == item.kd_nik)
                                            group c by new { c.date_document } into h
                                            select new { h.Key.date_document }).ToList();
                    int TotalKehadiran = tanggalKehadiran.Count();
                    decimal totalHonorMengajar = 0;
                    decimal totalHonor = 0;
                    var lstMataKuliahDosen = (from c in db.view_honor_dosen
                                              where (c.date_document >= from2 && c.date_document <= to && c.kd_nik == item.kd_nik)
                                              group c by new
                                              {
                                                  c.kelas,
                                                  c.name_course,
                                                  c.code_course,
                                                  c.credit_course,
                                                  c.honor_per_sks,
                                                  c.honor_tunjangan
                                              } into h
                                              select new
                                              {
                                                  h.Key.kelas,
                                                  h.Key.name_course,
                                                  h.Key.credit_course,
                                                  h.Key.honor_per_sks,
                                                  h.Key.honor_tunjangan,
                                                  h.Key.code_course,
                                                  jumlahHadir = h.Count()
                                              }).ToList();
                    int TotalSksDosenFungsional = 0;//minal 9 sks
                    int transportHadir = 0;
                    foreach (var itemMK in lstMataKuliahDosen)
                    {
                        var lstTglMK = (from c in db.view_honor_dosen
                                        where (c.date_document >= from2 && c.date_document <= to && c.kd_nik == item.kd_nik && c.kelas == itemMK.kelas)
                                        select new
                                        {
                                            c.date_document
                                        }).ToList();
                        string tglMk = "";

                        TotalSksDosenFungsional = TotalSksDosenFungsional + itemMK.credit_course ?? 0;
                        foreach (var itmTglMk in lstTglMK)
                        {
                            tglMk = tglMk + itmTglMk.date_document.Value.ToShortDateString() + " ";

                            if (item.status_dosen_id == 1 || item.status_dosen_id == 2 || item.status_dosen_id == 3)
                            {
                                string zz = itmTglMk.date_document.Value.DayOfWeek.ToString();
                                string sabtu = DayOfWeek.Saturday.ToString();
                                string minggu = DayOfWeek.Sunday.ToString();
                                if (zz == sabtu || zz == minggu)
                                {
                                    transportHadir = transportHadir + 1;
                                }
                            }
                            else
                            {
                                transportHadir = transportHadir + 1;
                            }
                        }

                        if (item.status_dosen_id == 2 || item.status_dosen_id == 3)
                        {
                            sl.SetCellValue("F" + row, itemMK.name_course);
                            sl.SetCellValue("G" + row, itemMK.credit_course.Value);
                            sl.SetCellValue("H" + row, itemMK.kelas);
                            sl.SetCellValue("I" + row, tglMk);
                            sl.SetCellValue("J" + row, itemMK.jumlahHadir);
                            sl.SetCellValue("K" + row, itemMK.honor_per_sks ?? 0);
                            sl.SetCellValue("L" + row, item.transpor_per_hari ?? 0);
                        }
                        else
                        {
                            sl.SetCellValue("F" + row, itemMK.name_course);
                            sl.SetCellValue("G" + row, itemMK.credit_course.Value);
                            sl.SetCellValue("H" + row, itemMK.kelas);
                            sl.SetCellValue("I" + row, tglMk);
                            sl.SetCellValue("J" + row, itemMK.jumlahHadir);
                            sl.SetCellValue("K" + row, itemMK.honor_per_sks ?? 0);
                            sl.SetCellValue("L" + row, item.transpor_per_hari ?? 0);
                            //sl.SetCellValue("M" + row, transportHadir);
                            sl.SetCellValue("N" + row, itemMK.honor_per_sks * itemMK.credit_course ?? 0 * itemMK.jumlahHadir);
                            sl.SetCellValue("O" + row, itemMK.honor_tunjangan ?? 0 * itemMK.jumlahHadir);
                            sl.SetCellValue("P" + row, "=N" + row + "+O" + row);
                            decimal hitungHonorMengajar = itemMK.honor_per_sks * itemMK.credit_course ?? 0 * itemMK.jumlahHadir;
                            decimal hitungHonorTunjangan = itemMK.honor_tunjangan ?? 0 * itemMK.jumlahHadir;
                            totalHonorMengajar = totalHonorMengajar + hitungHonorMengajar + hitungHonorTunjangan;
                            // sl.SetCellValue("Q" + row, transportHadir*item.transpor_per_hari??0);
                        }
                        row++;

                    }


                    totalHonor = totalHonorMengajar + (transportHadir * item.transpor_per_hari ?? 0);
                    if (item.status_dosen_id == 2 || item.status_dosen_id == 3)
                    {
                        decimal hitungHonorMengajar = 0;

                        if (TotalSksDosenFungsional - 9 > 0)
                        {
                            sl.SetCellValue("N" + (row - lstMataKuliahDosen.Count()), (TotalSksDosenFungsional - 9) * item.honor_per_sks ?? 0);
                            hitungHonorMengajar = (TotalSksDosenFungsional - 9) * item.honor_per_sks ?? 0;
                        }
                        else
                        {
                            sl.SetCellValue("N" + row, 0);
                        }
                        sl.SetCellValue("O" + (row - lstMataKuliahDosen.Count()), 0);
                        sl.SetCellValue("P" + (row - lstMataKuliahDosen.Count()), "=N" + (row - lstMataKuliahDosen.Count()) + "+O" + (row - lstMataKuliahDosen.Count()));
                        sl.SetCellValue("Q" + (row - lstMataKuliahDosen.Count()), transportHadir * item.transpor_per_hari ?? 0);
                        decimal transpor = transportHadir * item.transpor_per_hari ?? 0;
                        totalHonor = hitungHonorMengajar + transpor;
                    }
                    sl.SetCellValue("M" + (row - lstMataKuliahDosen.Count()), transportHadir);
                    sl.SetCellValue("Q" + (row - lstMataKuliahDosen.Count()), transportHadir * item.transpor_per_hari ?? 0);
                    sl.SetCellValue("R" + (row - lstMataKuliahDosen.Count()), totalHonor);

                    sl.SelectWorksheet("Sheet2");

                    sl.SetCellValue("A" + rowSheet2, no);
                    sl.SetCellValue("B" + rowSheet2, NIK);
                    sl.SetCellValue("C" + rowSheet2, NamaDosen);
                    sl.SetCellValue("E" + rowSheet2, totalHonor);
                    if (item.pegawai_id != null)
                    {
                        tbl_rekening rekening = db.tbl_rekening.Where(d => d.pegawai_id == item.pegawai_id).FirstOrDefault();
                        if (rekening != null)
                        {
                            sl.SetCellValue("D" + rowSheet2, rekening.no_rekening.ToString());
                            sl.SetCellValue("G" + rowSheet2, totalHonor);
                            sl.SetCellValue("H" + rowSheet2, rekening.nama_bank_cabang.ToString());
                            sl.SetCellValue("I" + rowSheet2, rekening.atas_nama.ToString());
                            sl.SetCellValue("J" + rowSheet2, rekening.kode_bank.ToString());
                        }
                    }
                    rowSheet2++;
                    no++;

                    //buat slip gaji convert html ke pdf 
                    //file buat pdf
                    string strExeFile = string.Format(@"{0}\PDFTools\{1}", System.Web.HttpContext.Current.Request.MapPath("~"), "wkhtmltopdf.exe");  // Request.PhysicalApplicationPath + "PDFTools\\wkhtmltopdf.exe";
                    string filenamepdf = "Slip_Honor_" + item.kd_nik + "_" + CodeValue + ".pdf";
                    string fname = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filenamepdf);

                    string fnameFolder = string.Format(@"{0}\temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"));

                    view_honor_dosen vhDosen = new view_honor_dosen();
                    vhDosen.kd_nik = item.kd_nik;
                    vhDosen.nama_dosen = item.nama_dosen;
                    vhDosen.tanggal_masuk = item.tanggal_masuk;
                    vhDosen.status_dosen = item.status_dosen;
                    vhDosen.tingkat_pendidikan = item.tingkat_pendidikan;
                    vhDosen.jabatan_fungsional = item.jabatan_fungsional;
                    vhDosen.jenjang_kelompok = item.jenjang_kelompok;
                    vhDosen.transpor_per_hari = item.transpor_per_hari;
                    vhDosen.tanggal_sk = item.tanggal_sk;

                    List<viewHonorMatakKuliahDosen> lstvhMKDosen = new List<viewHonorMatakKuliahDosen>();
                    foreach (var itemMKDosen in lstMataKuliahDosen)
                    {
                        viewHonorMatakKuliahDosen MKDosen = new viewHonorMatakKuliahDosen();
                       // MKDosen.honor_per_sks = itemMKDosen.honor_per_sks;
                        MKDosen.jumlahHadir = itemMKDosen.jumlahHadir;
                       // MKDosen.honor_tunjangan = itemMKDosen.honor_tunjangan;
                        MKDosen.kelas = itemMKDosen.kelas;
                        MKDosen.code_course = itemMKDosen.code_course;
                        MKDosen.credit_course = itemMKDosen.credit_course;
                        lstvhMKDosen.Add(MKDosen);
                    }

                    string html = templateSlipHtml(vhDosen, lstvhMKDosen, transportHadir, from2, to, TahunAjaran, Semester);
                    string filename1 = "Slip_Honor_" + item.kd_nik + "_" + CodeValue + ".html";
                    //string fname_html = string.Format(@"temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"), filename1);
                    string fname_html = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filename1);
                    System.IO.File.WriteAllText(fname_html, html);

                    bool isFileExist = System.IO.File.Exists(fname);
                    if (isFileExist)
                        System.IO.File.Delete(fname);
                    helper.WebToPDFStreamLandScape(strExeFile,
                        filename1,
                        fnameFolder,
                        filenamepdf);


                    int intTimeOut = 0;
                    while (!System.IO.File.Exists(fname))
                    {
                        System.Threading.Thread.Sleep(1000);
                        intTimeOut++;
                        if (intTimeOut == 10)
                        {
                            break;
                        }
                    }

                    tbl_generate_slip_honor genSlipHonor = new tbl_generate_slip_honor();
                    genSlipHonor.create_on = NOW;
                    genSlipHonor.create_by = user;
                    genSlipHonor.generate_report_id = GenReport.id;
                    genSlipHonor.nama_file = filenamepdf;
                    genSlipHonor.nik = item.kd_nik;
                    db.tbl_generate_slip_honor.Add(genSlipHonor);
                    db.SaveChanges();
                    bool isFileHtmlExist = System.IO.File.Exists(fname_html);
                    if (isFileHtmlExist)
                        System.IO.File.Delete(fname_html);
                    Session["message"] = "ok";
                }


                sl.SaveAs(fileTempReport);
                statusCreate = 1;
            }
            catch (Exception ex) { statusCreate = 0; Session["message"] = ex.ToString(); Session["reportid"] = -1; }

            using (var db2 = new stmtsdmEntities())
            {
                if (statusCreate == 1)
                {
                    tbl_generate_report upGenReport = db2.tbl_generate_report.Where(d => d.id == GenReport.id).FirstOrDefault();
                    upGenReport.create_status = statusCreate;
                    db2.SaveChanges();
                    Session["reportid"] = GenReport.id;
                    Session["message"] = "Generate Complate";
                }
                else
                {
                    Session["reportid"] = -1;
                    Session["message"] = "Generate Fail";
                }
            }
            return Json(fileName, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GenerateReportSp(string StartDate, string EndDate, string TahunAjaran, string Semester)
        {
            var from2 = DateTime.Parse(StartDate);
            var to = DateTime.Parse(EndDate);
            string fullPath = Path.Combine(Server.MapPath(folderTemplateUrl), "template1.xlsx");
            string dateNow = DateTime.Now.ToString("yyyyMMddhhmmss");

            string CodeValue = user+"_" + dateNow;
            //string fileName = "Report_" + user + "_" + from2.ToString("d").Replace("/", "-") + "_" + to.ToString("d").Replace("/", "-") + ".xlsx";
            string fileName = "Report_" + CodeValue + ".xlsx";
            tbl_generate_report GenReport = new tbl_generate_report();
            var db = new stmtsdmEntities();
            GenReport.nama_file = "Report_" + CodeValue;
            GenReport.create_status = 0;
            GenReport.create_by = user;
            GenReport.report_start = from2;
            GenReport.report_end = to;
            GenReport.create_on = NOW.Date;
            GenReport.code_value = CodeValue;
            db.tbl_generate_report.Add(GenReport);
            db.SaveChanges();

            int statusCreate = 0;
            Session["reportid"] = 0;
            string fileTempReport = Path.Combine(Server.MapPath(folderTempUrl), fileName);
            try
            {

                System.IO.File.Copy(fullPath, fileTempReport, true);
                List<sp_get_honor_dosen_Result> SPReportHonorDosen = db.sp_get_honor_dosen(from2, to).ToList();
                

                SLDocument sl = new SLDocument(fileTempReport, "Sheet1");
                int no = 1;
                int row = 9;
                int rowSheet2 = 8;
                string A5 = "Bulan " + ConvertToNamaBulan(to.Month) + " " + to.Year + "(" + from2.ToShortDateString() + " s/d " + to.ToShortDateString() + ")";
                string Sheet2A4 = "Priode I " + "(" + from2.ToShortDateString() + " s/d " + to.ToShortDateString() + ")";
                sl.SetCellValue("A5", A5);
                sl.SelectWorksheet("Sheet2");
                sl.SetCellValue("A4", Sheet2A4);
                Session["jumReport"] = SPReportHonorDosen.Count();
                string htmlTemplateReportDetails = htmlReportDetailHeader();
                string htmlTemplateReportTotal = htmlReportTotalHeader();
                if (SPReportHonorDosen.Count() > 0)
                {
                    foreach (var item in SPReportHonorDosen)
                    {
                        sl.SelectWorksheet("Sheet1");
                        sl.SetCellValue("A" +row, item.no.ToString());
                        sl.SetCellValue("B" + row, item.kd_nik);
                        sl.SetCellValue("C" + row, item.nama_dosen);
                        sl.SetCellValue("D" + row, item.golongan);
                        sl.SetCellValue("E" + row, item.status_dosen);
                        sl.SetCellValue("F" + row, item.mata_kuliah);
                        sl.SetCellValue("G" + row, item.sks.ToString());
                        sl.SetCellValue("H" + row, item.kelas);
                        sl.SetCellValue("I" + row, item.tanggal);
                        sl.SetCellValue("J" + row, item.jum_hadir.ToString());
                        sl.SetCellValue("K" + row, item.honor_per_sks ?? 0);
                        sl.SetCellValue("L" + row, item.trans_per_hari ?? 0);
                        sl.SetCellValue("M" + row, item.jum_transpor.ToString());
                        sl.SetCellValue("N" + row, item.total_honor_per_kelas.ToString());
                        sl.SetCellValue("O" + row, item.total_honor_tunjangan_per_kelas.ToString());
                        sl.SetCellValue("P" + row, item.total_honor_dan_tunjangan_per_kelas.ToString());
                        sl.SetCellValue("Q" + row, item.total_transport.ToString());
                        sl.SetCellValue("R" + row, item.total_keseluruhan.ToString());
                        if (item.no != null)
                        {
                            sl.SelectWorksheet("Sheet2");
                            sl.SetCellValue("A" + rowSheet2, no);
                            sl.SetCellValue("B" + rowSheet2, item.kd_nik);
                            sl.SetCellValue("C" + rowSheet2, item.nama_dosen);
                            sl.SetCellValue("E" + rowSheet2, item.total_keseluruhan.ToString());
                            sl.SetCellValue("D" + rowSheet2, Convert.ToString(item.no_rekening));
                            sl.SetCellValue("G" + rowSheet2, Convert.ToString(item.total_keseluruhan));
                            sl.SetCellValue("H" + rowSheet2, Convert.ToString(item.nama_bank_cabang));
                            sl.SetCellValue("I" + rowSheet2, Convert.ToString(item.atas_nama));
                            sl.SetCellValue("J" + rowSheet2, Convert.ToString(item.kode_bank));
                                
                            rowSheet2++;
                         #region buat html Report Total Honor pdf
                            htmlTemplateReportTotal = htmlTemplateReportTotal + htmlReportTotalDetails(item);
                         #endregion

                         #region buat slip gaji convert html ke pdf 
                        //file buat pdf
                        string strExeFile = string.Format(@"{0}\PDFTools\{1}", System.Web.HttpContext.Current.Request.MapPath("~"), "wkhtmltopdf.exe");  // Request.PhysicalApplicationPath + "PDFTools\\wkhtmltopdf.exe";
                        string filenamepdf = "Slip_Honor_" + item.kd_nik + "_" + CodeValue + ".pdf";
                        string fname = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filenamepdf);

                        string fnameFolder = string.Format(@"{0}\temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"));

                        view_honor_dosen vhDosen = db.view_honor_dosen.Where(d => d.kd_nik == item.kd_nik).FirstOrDefault();

                        var lstMataKuliahDosen = SPReportHonorDosen.Where(d => d.kd_nik_xls == item.kd_nik_xls).ToList();
                        List<viewHonorMatakKuliahDosen> lstvhMKDosen = new List<viewHonorMatakKuliahDosen>();
                        foreach (var itemMKDosen in lstMataKuliahDosen)
                        {
                            viewHonorMatakKuliahDosen MKDosen = new viewHonorMatakKuliahDosen();
                            MKDosen.honor_per_sks = itemMKDosen.honor_per_sks;
                            MKDosen.jumlahHadir = itemMKDosen.jum_hadir;
                            MKDosen.honor_tunjangan = itemMKDosen.honor_tunjangan;
                            MKDosen.kelas = itemMKDosen.kelas;
                            MKDosen.code_course = itemMKDosen.code_course;
                            MKDosen.credit_course = itemMKDosen.sks;
                            MKDosen.total_honor_per_kelas = itemMKDosen.total_honor_per_kelas;
                            MKDosen.total_tunjangan_per_kelas = itemMKDosen.total_honor_tunjangan_per_kelas;
                            lstvhMKDosen.Add(MKDosen);
                        }

                        string html = templateSlipHtml(vhDosen, lstvhMKDosen, item.jum_transpor==null?0:item.jum_transpor.Value, from2, to, TahunAjaran, Semester);
                        string filename1 = "Slip_Honor_" + item.kd_nik + "_" + CodeValue + ".html";
                        //string fname_html = string.Format(@"temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"), filename1);
                        string fname_html = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filename1);
                        System.IO.File.WriteAllText(fname_html, html);

                        bool isFileExist = System.IO.File.Exists(fname);
                        if (isFileExist)
                            System.IO.File.Delete(fname);
                        helper.WebToPDFStreamLandScape(strExeFile,
                            filename1,
                            fnameFolder,
                            filenamepdf);


                        int intTimeOut = 0;
                        while (!System.IO.File.Exists(fname))
                        {
                            System.Threading.Thread.Sleep(1000);
                            intTimeOut++;
                            if (intTimeOut == 10)
                            {
                                break;
                            }
                        }

                        tbl_generate_slip_honor genSlipHonor = new tbl_generate_slip_honor();
                        genSlipHonor.create_on = NOW;
                        genSlipHonor.create_by = user;
                        genSlipHonor.generate_report_id = GenReport.id;
                        genSlipHonor.nama_file = "Slip_Honor_" + item.kd_nik + "_" + CodeValue;
                        genSlipHonor.nik = item.kd_nik;
                        db.tbl_generate_slip_honor.Add(genSlipHonor);
                        db.SaveChanges();
                        //bool isFileHtmlExist = System.IO.File.Exists(fname_html);
                        //if (isFileHtmlExist)
                        //    System.IO.File.Delete(fname_html);
                            //
                         #endregion
                        }
                        #region buat html Report details Honor pdf
                        htmlTemplateReportDetails = htmlTemplateReportDetails + htmlReportDetailDetails(item);
                        #endregion
                        row++;                        
                    }                    
                }

                sl.SaveAs(fileTempReport);
                #region Buat PDF REport Details
                htmlTemplateReportDetails = htmlTemplateReportDetails + htmlReportDetailFooter();
                string strExeFileReportDetails = string.Format(@"{0}\PDFTools\{1}", System.Web.HttpContext.Current.Request.MapPath("~"), "wkhtmltopdf.exe");  // Request.PhysicalApplicationPath + "PDFTools\\wkhtmltopdf.exe";
                string filenamepdfReportDetails = "Details_Report_" + CodeValue + ".pdf";
                string fnameReportDetails = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filenamepdfReportDetails);

                string fnameFolderReportDetails = string.Format(@"{0}\temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"));

                string filename1ReportDetails = "Details_Report_" + CodeValue + ".html";
                //string fname_html = string.Format(@"temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"), filename1);
                string fname_htmlReportDetails = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filename1ReportDetails);
                System.IO.File.WriteAllText(fname_htmlReportDetails, htmlTemplateReportDetails);

                bool isFileExistReportDetails = System.IO.File.Exists(filenamepdfReportDetails);
                if (isFileExistReportDetails)
                    System.IO.File.Delete(filenamepdfReportDetails);
                helper.WebToPDFStreamLandScape(strExeFileReportDetails,
                    filename1ReportDetails,
                    fnameFolderReportDetails,
                    filenamepdfReportDetails);


                int intReportDetails = 0;
                while (!System.IO.File.Exists(fnameReportDetails))
                {
                    System.Threading.Thread.Sleep(1000);
                    intReportDetails++;
                    if (intReportDetails == 10)
                    {
                        break;
                    }
                }
                #endregion

                #region Buat PDF Total Details
                htmlTemplateReportTotal = htmlTemplateReportTotal + htmlReportTotalFooter();
                string strExeFileReportTotal = string.Format(@"{0}\PDFTools\{1}", System.Web.HttpContext.Current.Request.MapPath("~"), "wkhtmltopdf.exe");  // Request.PhysicalApplicationPath + "PDFTools\\wkhtmltopdf.exe";
                string filenamepdfReportTotal = "Total_Report_" + CodeValue + ".pdf";
                string fnameReportTotal = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filenamepdfReportTotal);

                string fnameFolderReportTotal = string.Format(@"{0}\temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"));

                string filename1ReportTotal = "Total_Report_" + CodeValue + ".html";
                //string fname_html = string.Format(@"temp\cache\", System.Web.HttpContext.Current.Request.MapPath("~"), filename1);
                string fname_htmlReportTotal = Path.Combine(Server.MapPath(folderTemplateUrlPdf), filename1ReportTotal);
                System.IO.File.WriteAllText(fname_htmlReportTotal, htmlTemplateReportTotal);

                bool isFileExistReportTotal = System.IO.File.Exists(filenamepdfReportTotal);
                if (isFileExistReportTotal)
                    System.IO.File.Delete(filenamepdfReportTotal);
                helper.WebToPDFStreamLandScape(strExeFileReportTotal,
                    filename1ReportTotal,
                    fnameFolderReportTotal,
                    filenamepdfReportTotal);


                int intReportTotal = 0;
                while (!System.IO.File.Exists(fnameReportTotal))
                {
                    System.Threading.Thread.Sleep(1000);
                    intReportTotal++;
                    if (intReportTotal == 10)
                    {
                        break;
                    }
                }
                #endregion

                statusCreate = 1;
            }
            catch (Exception ex) { statusCreate = 0; Session["message"] = ex.ToString(); Session["reportid"] = -1; }

            using (var db2 = new stmtsdmEntities())
            {
                if (statusCreate == 1)
                {
                    tbl_generate_report upGenReport = db2.tbl_generate_report.Where(d => d.id == GenReport.id).FirstOrDefault();
                    upGenReport.create_status = statusCreate;
                    db2.SaveChanges();
                    Session["reportid"] = GenReport.id;
                }
                else
                {
                    Session["reportid"] = -1;
                }
            }
            return Json(fileName, JsonRequestBehavior.AllowGet);
        }

       // [Authorize(Roles = "admin,adum,super")]
        public JsonResult GenerateReportReguler(string StartDate, string EndDate, string TahunAjaran, string Semester,string periode)
        {
            string message = "";
            int status = 0;

            try
            {
                var from2 = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var to = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string dateNow = DateTime.Now.ToString("yyyyMMddhhmmss");
                string CodeValue = user + "_" + dateNow;
                var db = new stmtsdmEntities();
                var db2 = new stmtsdmEntities();
                tbl_generate_report GenReport = new tbl_generate_report();
                GenReport.nama_file = "Report_" + CodeValue;
                GenReport.create_status = 0;
                GenReport.create_by = user;
                GenReport.report_start = from2;
                GenReport.report_end = to;
                GenReport.create_on = NOW.Date;
                GenReport.code_value = CodeValue;
                GenReport.tahun_ajaran = TahunAjaran;
                GenReport.semester = Semester;
                GenReport.report_type_id = 1;
                GenReport.periode = periode;
                db.tbl_generate_report.Add(GenReport);
                db.SaveChanges();
               List<sp_get_honor_dosen_to_table_Result>ListHonor= db.sp_get_honor_dosen_to_table(from2, to, GenReport.id).Where(d=>d.no!=null).ToList();

               foreach (var item in ListHonor)
               {
                   tbl_generate_slip_honor genSlipHonor = new tbl_generate_slip_honor();
                   genSlipHonor.create_on = NOW;
                   genSlipHonor.create_by = user;
                   genSlipHonor.generate_report_id = GenReport.id;
                   genSlipHonor.nama_file = "Slip_Honor_" + item.kd_nik + "_" + CodeValue;
                   genSlipHonor.nik = item.kd_nik;
                   db.tbl_generate_slip_honor.Add(genSlipHonor);
                   db.SaveChanges();
               }
                tbl_generate_report GenReport2 = db2.tbl_generate_report.Where(d => d.id == GenReport.id).FirstOrDefault();
                GenReport2.create_status = 1;
                db2.SaveChanges();
                status = 1;
                message = helper.messageSuksesHtml("Berhasil Generate Report Honor Kelas Reguler");
            }
            catch (Exception ex){
                message = helper.ErrorMessageWithElement(ex.ToString());                
            }
            finally{
                resultMessage.message = message;
                resultMessage.status = status;
            }

            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        //[Authorize(Roles = "admin,adum,super")]
        public JsonResult GenerateReportInter(string StartDate, string EndDate, string TahunAjaran, string Semester, string periode)
        {
            string message = "";
            int status = 0;

            try
            {
                var from2 = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var to = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string dateNow = DateTime.Now.ToString("yyyyMMddhhmmss");
                string CodeValue = user + "_" + dateNow;
                var db = new stmtsdmEntities();
                var db2 = new stmtsdmEntities();
                tbl_generate_report GenReport = new tbl_generate_report();
                GenReport.nama_file = "Report_" + CodeValue;
                GenReport.create_status = 0;
                GenReport.create_by = user;
                GenReport.report_start = from2;
                GenReport.report_end = to;
                GenReport.create_on = NOW.Date;
                GenReport.code_value = CodeValue;
                GenReport.tahun_ajaran = TahunAjaran;
                GenReport.semester = Semester;
                GenReport.report_type_id = 3;
                GenReport.periode = periode;
                db.tbl_generate_report.Add(GenReport);
                db.SaveChanges();
                List<sp_get_honor_dosen_inter_to_table_Result> ListHonor = db.sp_get_honor_dosen_inter_to_table(from2, to, GenReport.id).Where(d => d.no != null).ToList();

                foreach (var item in ListHonor)
                {
                    tbl_generate_slip_honor genSlipHonor = new tbl_generate_slip_honor();
                    genSlipHonor.create_on = NOW;
                    genSlipHonor.create_by = user;
                    genSlipHonor.generate_report_id = GenReport.id;
                    genSlipHonor.nama_file = "Slip_Honor_" + item.kd_nik + "_" + CodeValue;
                    genSlipHonor.nik = item.kd_nik;
                    db.tbl_generate_slip_honor.Add(genSlipHonor);
                    db.SaveChanges();
                }
                tbl_generate_report GenReport2 = db2.tbl_generate_report.Where(d => d.id == GenReport.id).FirstOrDefault();
                GenReport2.create_status = 1;
                db2.SaveChanges();
                status = 1;
                message = helper.messageSuksesHtml("Berhasil Generate Report Honor Kelas Reguler");
            }
            catch (Exception ex)
            {
                message = helper.ErrorMessageWithElement(ex.ToString());
            }
            finally
            {
                resultMessage.message = message;
                resultMessage.status = status;
            }

            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        public class MessageStatusGenerate
        {
            public tbl_generate_report tblGenReport { get; set; }
            public int status { get; set; }
            public string message { get; set; }
            public int jumReport { get; set; }
            public int loadingReport { get; set; }
        }
     
        public JsonResult StatusReport()
        {
            MessageStatusGenerate stt = new MessageStatusGenerate();
            if (Session["reportid"] != null)
            {
                int reportId = Convert.ToInt32(Session["reportid"]);
                if (reportId > 0)
                {
                    using (var db = new stmtsdmEntities())
                    {
                        tbl_generate_report Report = db.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();
                        if (Report.id > 0)
                        {
                            stt.status = 1;
                            stt.tblGenReport = Report;
                            Session["reportid"] = 0;
                        }
                        else
                        {
                            stt.status = -1;
                            stt.tblGenReport = Report;
                        }
                    }
                }
                else if (reportId == 0)
                {
                    stt.status = 0;
                }
                else
                {
                    stt.status = reportId;
                }
            }
            return Json(stt, JsonRequestBehavior.AllowGet);
        }
             
        public JsonResult GenerateStatus()
        {
            MessageStatusGenerate stt = new MessageStatusGenerate();
            stt.status = 0;
            using (var db = new stmtsdmEntities())
            {
                int wow = reportid;
                var sss = Session["reportid"];
                if (Session["loadingReport"] != null)
                {
                    stt.loadingReport = Convert.ToInt32(Session["loadingReport"]);
                }
                if (Session["message"] != null)
                {
                    stt.message = Session["message"].ToString();
                }
                if (Session["jumReport"] != null)
                {
                    stt.jumReport = Convert.ToInt32(Session["jumReport"]);
                }
                if (Session["reportid"] != null)
                {                    
                    int reportId = Convert.ToInt32(Session["reportid"]);
                    if (reportId > 0)
                    {
                        tbl_generate_report Report = db.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();
                        if (Report.id > 0)
                        {
                            stt.status = 1;
                            stt.tblGenReport = Report;
                            Session["reportid"] = 0;
                        }
                    }
                    else
                    {
                        if (Session["message"] != null)
                        {
                            stt.status = -1;
                            stt.tblGenReport = null;
                            stt.message = Session["message"].ToString();
                            Session["reportid"] = 0;
                        }
                        else
                        {
                            stt.status = -1;
                            stt.tblGenReport = null;
                            stt.message = "fail";
                            Session["reportid"] = 0;
                        }
                    }
                }
            }

            return Json(stt, JsonRequestBehavior.AllowGet);
        }
      
        public void generateReport()
        {
            string fullPath = Path.Combine(Server.MapPath(folderTemplateUrl), "Book1.xlsx");           
            string day = NOW.Day.ToString();
            string month = NOW.Month.ToString();
            string year = NOW.Year.ToString();
            string fileName = "Book1" + "-" + day + "-" + month + "-" + year;

            string fileTempReport = Path.Combine(Server.MapPath(folderTempUrl), fileName + ".xlsx");
            try
            {

                System.IO.File.Copy(fullPath, fileTempReport, true);
            }
            catch (Exception ex) { }
            generateReportXLS(fileTempReport, fileName, "Sheet1");
            
             
        }

        public void generateReportXLS(string urlfileXls, string fileName, string sheetName)
        {
            Session["StatusReport1"] = null;
            resultMessage = null;
            int status = 0;
            string message = "";
            try
            {
                //FileStream file = new FileStream(urlfileXls, FileMode.Open, FileAccess.Read);

                SLDocument sl = new SLDocument(urlfileXls, sheetName);
                var db = new stmtsdmEntities();
                

                sl.SetCellValue("E6", "Let's party!!!!111!!!1");

                sl.SelectWorksheet("Sheet3");
                sl.SetCellValue("E6", "Before anyone calls the popo!");

                sl.AddWorksheet("DanceFloor");
                sl.SetCellValue("B4", "Who let the dogs out?");
                sl.SetCellValue("B5", "Woof!");



               // FileStream file2 = new FileStream(fileTempReport, FileMode.Create);
                sl.SaveAs(urlfileXls);
                //file2.Close();

                status = 1;
                message = fileName;
            }
            catch (Exception ex)
            {
                status = 0;
                message = helper.ErrorMessageWithElement(ex.ToString());
            }
            finally
            {
                resultMessage.status = status;
                resultMessage.message = message;
                Response.AddHeader("Content-Disposition", "inline;filename=" + fileName);
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.WriteFile("");
                Response.End();
            }
            Session["StatusReport1"] = resultMessage;
        }

        public class DataTableHistroyReport : DataTableData
        {
            public List<tbl_generate_report> data { get; set; }
        }

        public ActionResult FileReport(int id, int type)
        {
            string fullPath = "";// Path.Combine(Server.MapPath(folderTempUrl), "template1.xlsx");
            var db = new stmtsdmEntities();
            tbl_generate_report genReport = db.tbl_generate_report.Where(d => d.id == id).FirstOrDefault();            
            if (type == 1)
            {
                fullPath = Path.Combine(Server.MapPath(folderTempUrl), genReport.nama_file + ".xlsx");
                return File(fullPath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", genReport.nama_file + "xlsx");
            }
            else
            {
                fullPath = Path.Combine(Server.MapPath(folderTempUrl), genReport.nama_file + ".pdf");
                return File(fullPath, System.Net.Mime.MediaTypeNames.Application.Pdf, genReport.nama_file + "pdf");
            }
        }

        public ActionResult FileReportDetails(int id)
        {
            string fullPath = "";// Path.Combine(Server.MapPath(folderTempUrl), "template1.xlsx");
            var db = new stmtsdmEntities();
            tbl_generate_report genReport = db.tbl_generate_report.Where(d => d.id == id).FirstOrDefault();
            if (genReport != null)
            {
                //Slip_Honor_A007_User20151105042249

                fullPath = Path.Combine(Server.MapPath(folderTemplateUrlPdf), "Details_" + genReport.nama_file + ".pdf");
                return File(fullPath, System.Net.Mime.MediaTypeNames.Application.Pdf, genReport.nama_file + "pdf");
            }
            else
            {
                return Content("File Tidak Ada");
            }
        }

        public ActionResult FileReportTotal(int id)
        {
            string fullPath = "";// Path.Combine(Server.MapPath(folderTempUrl), "template1.xlsx");
            var db = new stmtsdmEntities();
            tbl_generate_report genReport = db.tbl_generate_report.Where(d => d.id == id).FirstOrDefault();
            if (genReport != null)
            {
                //Slip_Honor_A007_User20151105042249

                fullPath = Path.Combine(Server.MapPath(folderTemplateUrlPdf), "Total_" + genReport.nama_file + ".pdf");
                return File(fullPath, System.Net.Mime.MediaTypeNames.Application.Pdf, genReport.nama_file + "pdf");
            }
            else
            {
                return Content("File Tidak Ada");
            }
        }

        public ActionResult FileReportSlipHonor(int id)
        {
            string fullPath = "";// Path.Combine(Server.MapPath(folderTempUrl), "template1.xlsx");
            var db = new stmtsdmEntities();
            tbl_generate_slip_honor genReport = db.tbl_generate_slip_honor.Where(d => d.id == id).FirstOrDefault();
            if (genReport != null)
            {
                //Slip_Honor_A007_User20151105042249

                fullPath = Path.Combine(Server.MapPath(folderTemplateUrlPdf), genReport.nama_file+".pdf");
                return File(fullPath, System.Net.Mime.MediaTypeNames.Application.Pdf, genReport.nama_file + "pdf");
            }
            else
            {
                return Content("File Tidak Ada");
            }
        }

        public JsonResult GetListHistroyReport(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string Start = System.Web.HttpContext.Current.Request["start_date_history"].ToString();
            string End = System.Web.HttpContext.Current.Request["end_date_history"].ToString();
            string ReportTypeId = System.Web.HttpContext.Current.Request["report_type_id"].ToString();
            DataTableHistroyReport dt = new DataTableHistroyReport();
            int sortColumn = 0;
            
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }

            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    var a = db.tbl_generate_report.Where(d=>d.create_status==1).AsQueryable();
                    dt.recordsTotal = a.Count();
                    if (Start != string.Empty)
                    {
                       var from= DateTime.ParseExact(Start, "dd/MM/yyyy",CultureInfo.InvariantCulture);
                       // var from = DateTime.Parse(Start);                        
                        a = a.Where(x => x.report_start == from);
                    }

                    if (End != string.Empty)
                    {
                        var to = DateTime.ParseExact(End, "dd/MM/yyyy",CultureInfo.InvariantCulture);
                        a = a.Where(x => x.report_end== to);
                    }

                    if (ReportTypeId != string.Empty)
                    {
                        int ReportTypeIdint = Convert.ToInt32(ReportTypeId);

                        a = a.Where(d => d.report_type_id == ReportTypeIdint);
                    }
                    
                    if (sortColumn == 0)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.id);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.id);
                    }
                    
                    if (sortColumn == 1)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.nama_file);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_file);
                    }
                    if (sortColumn == 5)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.create_on);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.create_on);
                    }

                    dt.data = a.Skip(start).Take(length).ToList();
                    dt.recordsFiltered = a.Count();

                }
            }
            catch { }
            finally
            { }
            return Json(dt, JsonRequestBehavior.AllowGet);

        }

        public class DataTableReportSlipGaji : DataTableData
        {
            public List<view_generate_slip_honor> data { get; set; }
        }

        public JsonResult GetListSlipReport(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
           /// string Start = System.Web.HttpContext.Current.Request["start_date_history"].ToString();
           // string End = System.Web.HttpContext.Current.Request["end_date_history"].ToString();
            string NamaDosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
            string KodeNik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string ReportId = System.Web.HttpContext.Current.Request["report_id"].ToString();
            string status_dosen_id = System.Web.HttpContext.Current.Request["status_dosen_id"].ToString();
            DataTableReportSlipGaji dt = new DataTableReportSlipGaji();
            int sortColumn = 0;

            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }

            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    int GenerateReportId=Convert.ToInt32(ReportId);
                    var a = db.view_generate_slip_honor.Where(d=>d.generate_report_id== GenerateReportId).AsQueryable();

                   
                    if (ReportId != string.Empty)
                    {
                        
                        a = a.Where(x => x.generate_report_id==GenerateReportId);
                    }

                    if (NamaDosen != string.Empty)
                    {
                        a = a.Where(x => x.nama_dosen.Contains(NamaDosen));
                    }
                    if (KodeNik != string.Empty)
                    {
                        a = a.Where(x => x.kd_nik.Contains(KodeNik));
                    }

                    if (status_dosen_id != string.Empty)
                    {
                        int statusID=Convert.ToInt32(status_dosen_id);
                        a = a.Where(x => x.status_dosen_id == statusID);
                    }

                    if (sortColumn == 1)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik);
                    }
                    if (sortColumn == 2)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.nama_dosen);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_dosen);
                    }

                    dt.recordsTotal = a.Count();
                    dt.data = a.Skip(start).Take(length).ToList();
                    dt.recordsFiltered = a.Count();

                }
            }
            catch { }
            finally
            { }
            return Json(dt, JsonRequestBehavior.AllowGet);

        }

        public JsonResult DeleteReport(int ReportId)
        {
            string message = "";
            int status = 0;
            using (var db = new stmtsdmEntities())
            {
                try
                {
                    tbl_generate_report tblGenerateReport = db.tbl_generate_report.Find(ReportId);
                    tblGenerateReport.create_status = 0;
                    db.SaveChanges();
                    string rootFolderPathSlip = Server.MapPath(folderTemplateUrlPdf);
                    string rootFolderPathHonor = Server.MapPath(folderTempUrl);
                    string filesToDelete = @"*" + tblGenerateReport .code_value+ "*.*";   // Only delete DOC files containing "DeleteMe" in their filenames
                    string[] fileList = System.IO.Directory.GetFiles(rootFolderPathSlip, filesToDelete);
                    string[] fileList2 = System.IO.Directory.GetFiles(rootFolderPathHonor, filesToDelete);
                    foreach (string file in fileList)
                    {
                        //System.Diagnostics.Debug.WriteLine(file + "will be deleted");
                        System.IO.File.Delete(file);
                    }
                    foreach (string file in fileList2)
                    {
                        //System.Diagnostics.Debug.WriteLine(file + "will be deleted");
                        System.IO.File.Delete(file);
                    }

                    message = helper.RemoveSuksesHtml();
                    status = 1;
                }
                catch (Exception ex) { message = helper.ErrorMessageWithElement(ex.ToString()); status = 0; }
                finally
                {
                    resultMessage.message=message;
                    resultMessage.status=status;
                }
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditReport(int ReportId)
        {
            var db=new stmtsdmEntities();
            ViewBag.ReportId = ReportId;
            return View();
        }

        public class DataTableTblReportHonorDosen : DataTableData
        {
            public List<tbl_report_honor_dosen> data { get; set; }
        }

        public JsonResult GetListDetailReportHonorDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string strReportId = System.Web.HttpContext.Current.Request["report_id"].ToString();
            string namaDosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
            string KdNik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string StatusDosen = System.Web.HttpContext.Current.Request["status_dosen"].ToString();

            DataTableTblReportHonorDosen dt = new DataTableTblReportHonorDosen();
            int sortColumn = 0;

            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }

            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    int ReportId = Convert.ToInt32(strReportId);
                    var a = db.tbl_report_honor_dosen.Where(d => d.reportid == ReportId).AsQueryable();

                    if (namaDosen != string.Empty)
                    {
                        a = a.Where(d => d.nama_dosen_xls.Contains(namaDosen)).AsQueryable();
                    }

                    if (KdNik != string.Empty)
                    {
                        a = a.Where(d => d.kd_nik_xls.Contains(KdNik)).AsQueryable();
                    }

                    if (StatusDosen != string.Empty && StatusDosen!="--Select--")
                    {
                        a = a.Where(d => d.status_dosen.Contains(StatusDosen)).AsQueryable();
                    }

                    if (sortColumn == 0)
                    {
                        if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik_xls);
                        if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik_xls);
                    }

                    dt.recordsTotal = a.Count();
                    dt.data = a.Skip(start).Take(length).ToList();
                    dt.recordsFiltered = a.Count();

                }
            }
            catch { }
            finally
            { }
            return Json(dt, JsonRequestBehavior.AllowGet);

        }

        public JsonResult AddDetailReportHonorDosenByDosen(string kd_nik, string kelas, string tanggal, int report_id, int id_lecture, string code_course)
        {
            string messege = "";
            int status = 0;
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    tbl_dosen tblDosen = db.tbl_dosen.Where(d => d.kd_nik == kd_nik).FirstOrDefault();
                    tbl_report_honor_dosen newTblReportHonorDetails = new tbl_report_honor_dosen();
                    tbl_report_honor_dosen totRowReportHonor = db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kd_nik != null && d.kd_nik == kd_nik).FirstOrDefault();
                    decimal? TotalKeseluruhanTanpaTransport = totRowReportHonor.total_keseluruhan - totRowReportHonor.total_transport;
                    decimal? TotalTransport = totRowReportHonor.total_transport;
                    int? JumTranport = totRowReportHonor.jum_transpor;

                    var allDetailReportHonor = db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kd_nik_xls==kd_nik && d.tanggal!=null).ToList();
                    string allTanggal =string.Empty;
                    int i = 0;
                    foreach (var item in allDetailReportHonor)
                    {
                        if (i == 0) allTanggal = item.tanggal;
                        else allTanggal = allTanggal + " " + item.tanggal;//.Replace(" ", "");
                        i++;
                    }
                    string newTgl = tanggal.Replace(" ", "");
                    string[] arrTgl = allTanggal.Split(' ');
                    var findTgl = Array.FindAll(arrTgl, s => s.Equals(newTgl)).Count();

                    if (findTgl == 0)
                    {
                        //lastTgls + " " + newTgl;
                        if (tblDosen.status_dosen_id == 1 || tblDosen.status_dosen_id==3)
                        {
                            DateTime Tanggal = DateTime.ParseExact(newTgl, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                            int dayOfweek = (int)Tanggal.DayOfWeek;
                            if (dayOfweek == 5 || dayOfweek == 6)
                            {
                                JumTranport = JumTranport + 1;
                                TotalTransport = JumTranport * Convert.ToDecimal(totRowReportHonor.trans_per_hari.Value);

                            }
                        }
                        else
                        {
                            JumTranport = JumTranport + 1;
                            TotalTransport = JumTranport * Convert.ToDecimal(totRowReportHonor.trans_per_hari.Value);
                        }
                    }

                    view_kelas Vkelas = db.view_kelas.Where(d => d.id_lecture == id_lecture).FirstOrDefault();
                    var VNamaMataKuiah = db.view_kelas.Where(d => d.code_course == code_course).FirstOrDefault();
                    int jumlahHadir = 1;
                    if (tblDosen.status_dosen_id != 3)
                    {
                        if (db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kelas == kelas && d.kd_nik_xls==kd_nik && d.code_course==code_course).FirstOrDefault() != null)
                        {
                            tbl_report_honor_dosen rowSelectEdit = db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kelas == kelas && d.code_course == code_course).FirstOrDefault();
                            TotalKeseluruhanTanpaTransport = TotalKeseluruhanTanpaTransport - (rowSelectEdit.total_honor_tunjangan_per_kelas + rowSelectEdit.total_honor_per_kelas);
                            //if (findTgl == 0) 
                            rowSelectEdit.tanggal = rowSelectEdit.tanggal+" " + tanggal;
                            rowSelectEdit.jum_hadir = rowSelectEdit.jum_hadir + 1;
                            rowSelectEdit.total_honor_per_kelas = rowSelectEdit.jum_hadir * rowSelectEdit.honor_per_sks;
                            rowSelectEdit.total_honor_tunjangan_per_kelas = rowSelectEdit.jum_hadir * rowSelectEdit.honor_tunjangan == null ? 0 : rowSelectEdit.honor_tunjangan;
                            rowSelectEdit.total_honor_dan_tunjangan = rowSelectEdit.total_honor_per_kelas + rowSelectEdit.total_honor_tunjangan_per_kelas;

                            totRowReportHonor.jum_transpor = JumTranport;
                            totRowReportHonor.total_transport = TotalTransport;
                            totRowReportHonor.total_keseluruhan = (TotalKeseluruhanTanpaTransport == null ? 0 : TotalKeseluruhanTanpaTransport) +
                                rowSelectEdit.total_honor_dan_tunjangan + (TotalTransport == null ? 0 : TotalTransport);
                        }
                        else 
                        {
                            
                            newTblReportHonorDetails.tanggal = newTgl;
                            newTblReportHonorDetails.jum_hadir = jumlahHadir;
                            newTblReportHonorDetails.reportid = report_id;
                            newTblReportHonorDetails.report_type_id = totRowReportHonor.report_type_id;
                            newTblReportHonorDetails.golongan = totRowReportHonor.golongan;
                            newTblReportHonorDetails.kelas = kelas;
                            newTblReportHonorDetails.honor_per_sks = totRowReportHonor.honor_per_sks;
                            newTblReportHonorDetails.total_honor_per_kelas = jumlahHadir * totRowReportHonor.honor_per_sks * Vkelas.credit_course;
                            newTblReportHonorDetails.honor_tunjangan = totRowReportHonor.honor_tunjangan;
                            newTblReportHonorDetails.total_honor_tunjangan_per_kelas = jumlahHadir * totRowReportHonor.honor_tunjangan == null ? 0 : totRowReportHonor.honor_tunjangan;
                            newTblReportHonorDetails.total_honor_dan_tunjangan = newTblReportHonorDetails.total_honor_per_kelas == null ? 0 : newTblReportHonorDetails.total_honor_per_kelas + newTblReportHonorDetails.total_honor_tunjangan_per_kelas == null ? 0 : newTblReportHonorDetails.total_honor_tunjangan_per_kelas;
                            newTblReportHonorDetails.kd_nik_xls = totRowReportHonor.kd_nik;
                            newTblReportHonorDetails.sks = Vkelas.credit_course;
                            newTblReportHonorDetails.status_dosen = totRowReportHonor.status_dosen;
                            newTblReportHonorDetails.honor_per_sks = totRowReportHonor.honor_per_sks;
                            newTblReportHonorDetails.trans_per_hari = totRowReportHonor.trans_per_hari;
                            newTblReportHonorDetails.nama_dosen_xls = totRowReportHonor.nama_dosen_xls;
                            newTblReportHonorDetails.kd_nik_xls = totRowReportHonor.kd_nik_xls;
                            newTblReportHonorDetails.mata_kuliah = VNamaMataKuiah.name_course;
                            newTblReportHonorDetails.code_course = VNamaMataKuiah.code_course;

                            db.tbl_report_honor_dosen.Add(newTblReportHonorDetails);

                            totRowReportHonor.jum_transpor = JumTranport;
                            totRowReportHonor.total_transport = TotalTransport;
                            totRowReportHonor.total_keseluruhan = (TotalKeseluruhanTanpaTransport == null ? 0 : TotalKeseluruhanTanpaTransport) +
                                newTblReportHonorDetails.total_honor_dan_tunjangan + (TotalTransport == null ? 0 : TotalTransport);

                        }
                    }
                    else
                    {
                        var lstDetails = db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kd_nik_xls == kd_nik).ToList();
                        var takeLastRow = lstDetails.OrderByDescending(d => d.id).FirstOrDefault();
                        lstDetails.Remove(takeLastRow);
                        int totSksSebelum = 0;
                        foreach (var item in lstDetails)
                        {
                            totSksSebelum = totSksSebelum + item.sks.Value;
                        }
                        int? totSks = totSksSebelum + Vkelas.credit_course;
                        if (db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kelas == kelas && d.kd_nik_xls == kd_nik && d.code_course==code_course).FirstOrDefault() != null)
                        {
                             totSks = totSksSebelum;
                        }
                        int? minTotSks = db.tbl_dosen_minimum_sks.Where(d => d.status_dosen_id == 3).FirstOrDefault().minimum_sks;


                        int? minJumhadir = db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kd_nik_xls == kd_nik).OrderBy(d => d.jum_hadir).FirstOrDefault().jum_hadir;
                        tbl_report_honor_dosen lastRowTblDosenFung = db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kd_nik_xls == kd_nik).OrderByDescending(d => d.id).FirstOrDefault();
                           
                        if (db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kelas == kelas && d.kd_nik_xls == kd_nik && d.code_course == code_course).FirstOrDefault() != null)
                        {
                            tbl_report_honor_dosen rowSelectEdit = db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kelas == kelas && d.code_course == code_course).FirstOrDefault();
                            if (findTgl == 0) rowSelectEdit.tanggal = rowSelectEdit.tanggal+" " + tanggal;
                            rowSelectEdit.jum_hadir = rowSelectEdit.jum_hadir + 1;
                            var editTableRow = db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kelas == kelas && d.kd_nik_xls == kd_nik).FirstOrDefault();
                            if (minJumhadir > editTableRow.jum_hadir) minJumhadir = editTableRow.jum_hadir;
                        }
                        else
                        {
                            tbl_report_honor_dosen tblReportTotSum = db.tbl_report_honor_dosen.Where(d => d.reportid == report_id && d.kd_nik != null && d.kd_nik == kd_nik).FirstOrDefault();

                            newTblReportHonorDetails.reportid = report_id;
                            newTblReportHonorDetails.report_type_id = totRowReportHonor.report_type_id;
                            newTblReportHonorDetails.tanggal = newTgl;
                            newTblReportHonorDetails.jum_hadir = jumlahHadir;
                            newTblReportHonorDetails.total_honor_per_kelas = 0;
                            newTblReportHonorDetails.honor_tunjangan = totRowReportHonor.honor_tunjangan == null ? 0 : totRowReportHonor.honor_tunjangan;
                            newTblReportHonorDetails.total_honor_tunjangan_per_kelas = 0;
                            newTblReportHonorDetails.total_honor_dan_tunjangan = newTblReportHonorDetails.total_honor_per_kelas + newTblReportHonorDetails.total_honor_tunjangan_per_kelas;
                            newTblReportHonorDetails.kd_nik_xls = totRowReportHonor.kd_nik;
                            newTblReportHonorDetails.sks = Vkelas.credit_course;
                            newTblReportHonorDetails.golongan = totRowReportHonor.golongan;
                            newTblReportHonorDetails.status_dosen = totRowReportHonor.status_dosen;
                            newTblReportHonorDetails.honor_per_sks = totRowReportHonor.honor_per_sks;
                            newTblReportHonorDetails.trans_per_hari = totRowReportHonor.trans_per_hari;
                            newTblReportHonorDetails.nama_dosen_xls = totRowReportHonor.nama_dosen_xls;
                            newTblReportHonorDetails.kd_nik_xls = totRowReportHonor.kd_nik_xls;
                            newTblReportHonorDetails.mata_kuliah = VNamaMataKuiah.name_course;
                            newTblReportHonorDetails.code_course = VNamaMataKuiah.code_course;
                            newTblReportHonorDetails.kelas = Vkelas.kelas;
                            db.tbl_report_honor_dosen.Add(newTblReportHonorDetails);
                            db.SaveChanges();
                        }


                            db.tbl_report_honor_dosen.Remove(lastRowTblDosenFung);
                            db.SaveChanges();

                            int? sisaSks = totSks - minTotSks;

                            tbl_report_honor_dosen newLastRowTblReport = new tbl_report_honor_dosen();
                            newLastRowTblReport.kd_nik_xls = totRowReportHonor.kd_nik_xls;
                            newLastRowTblReport.report_type_id = totRowReportHonor.report_type_id;
                            newLastRowTblReport.reportid = totRowReportHonor.reportid;
                            newLastRowTblReport.status_dosen = totRowReportHonor.status_dosen;
                            newLastRowTblReport.golongan = totRowReportHonor.golongan;
                            newLastRowTblReport.nama_dosen_xls = totRowReportHonor.nama_dosen_xls;
                            newLastRowTblReport.jum_hadir = minJumhadir;
                            newLastRowTblReport.sks = sisaSks;
                            newLastRowTblReport.total_honor_per_kelas = (minJumhadir * totRowReportHonor.honor_per_sks * sisaSks);
                            newLastRowTblReport.total_honor_tunjangan_per_kelas = (minJumhadir * totRowReportHonor.honor_tunjangan == null ? 0 : totRowReportHonor.honor_tunjangan);
                            newLastRowTblReport.total_honor_dan_tunjangan = newLastRowTblReport.total_honor_per_kelas + newLastRowTblReport.total_honor_tunjangan_per_kelas;
                            newLastRowTblReport.total_transport = TotalTransport;
                            newLastRowTblReport.mata_kuliah = lastRowTblDosenFung.mata_kuliah;
                            db.tbl_report_honor_dosen.Add(newLastRowTblReport);
                            db.SaveChanges();

                            totRowReportHonor.jum_transpor = JumTranport;
                            totRowReportHonor.total_transport = TotalTransport;
                            totRowReportHonor.total_keseluruhan = (TotalKeseluruhanTanpaTransport == null ? 0 : TotalKeseluruhanTanpaTransport) +
                                newLastRowTblReport.total_honor_dan_tunjangan + (TotalTransport == null ? 0 : TotalKeseluruhanTanpaTransport);

                            db.SaveChanges();
                    }
                    db.SaveChanges();
                    messege = helper.UpdateSuksesHtml();
                    status = 1;
                }
            }
            catch (Exception ex)
            {
                messege = helper.ErrorMessageWithElement(ex.ToString());
                status = 0;
            }
            finally
            {
                resultMessage.message = messege;
                resultMessage.status = status;
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EditDetailReportHonorDosenByDosen(tbl_report_honor_dosen tblReportHonorDosen)
        {
            string messege = "";
            int status = 0;
            try
            {
                using (var db =new  stmtsdmEntities())
                {
                    if (tblReportHonorDosen.id > 0)
                    {
                        tbl_dosen tblDosen = db.tbl_dosen.Where(d => d.kd_nik == tblReportHonorDosen.kd_nik_xls).FirstOrDefault();
                        tbl_report_honor_dosen newTblReportHonorDetails = new tbl_report_honor_dosen();
                        tbl_report_honor_dosen totRowReportHonor = db.tbl_report_honor_dosen.Where(d => d.reportid == tblReportHonorDosen.reportid && d.kd_nik != null && d.kd_nik == tblReportHonorDosen.kd_nik_xls).FirstOrDefault();
                        decimal? TotalKeseluruhanTanpaTransport = totRowReportHonor.total_keseluruhan - totRowReportHonor.total_transport;
                        decimal? TotalTransport = totRowReportHonor.total_transport;
                        int? JumTranport = totRowReportHonor.jum_transpor;

                        var allDetailReportHonor = db.tbl_report_honor_dosen.Where(d => d.reportid == tblReportHonorDosen.reportid).ToList();
                        string allTanggal = "";
                        foreach (var item in allDetailReportHonor)
                        {
                            allTanggal = allTanggal + item.tanggal;
                        }
                        string newTgl = tblReportHonorDosen.tanggal.ToString().Replace(" ", "");
                        string[] arrTgl = allTanggal.Split(' ');
                        var findTgl = Array.FindAll(arrTgl, s => s.Equals(newTgl)).Count();

                        if (tblDosen.status_dosen_id != 3)
                        {
                            if (db.tbl_report_honor_dosen.Where(d => d.reportid == tblReportHonorDosen.reportid && d.kelas != tblReportHonorDosen.kelas).FirstOrDefault() != null)
                            {
                                if (findTgl == 0)
                                {
                                    //lastTgls + " " + newTgl;
                                    if (tblDosen.status_dosen_id == 1)
                                    {
                                        DateTime Tanggal = DateTime.ParseExact(newTgl, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                        int dayOfweek = (int)Tanggal.DayOfWeek;
                                        if (dayOfweek == 5 || dayOfweek == 6)
                                        {
                                            JumTranport = JumTranport + 1;
                                            TotalTransport = Convert.ToDecimal(newTblReportHonorDetails.jum_transpor.Value) * Convert.ToDecimal(newTblReportHonorDetails.trans_per_hari.Value);

                                        }
                                    }
                                    else
                                    {
                                        JumTranport = JumTranport + 1;
                                        TotalTransport = Convert.ToDecimal(newTblReportHonorDetails.jum_transpor.Value) * Convert.ToDecimal(newTblReportHonorDetails.trans_per_hari.Value);

                                    }
                                }


                                newTblReportHonorDetails.tanggal = newTgl;
                                newTblReportHonorDetails.jum_hadir = newTblReportHonorDetails.jum_hadir + tblReportHonorDosen.jum_hadir;
                                newTblReportHonorDetails.total_honor_per_kelas = tblReportHonorDosen.jum_hadir * newTblReportHonorDetails.honor_per_sks * tblReportHonorDosen.sks;
                                newTblReportHonorDetails.honor_tunjangan = tblReportHonorDosen.jum_hadir * newTblReportHonorDetails.honor_tunjangan;
                                newTblReportHonorDetails.total_honor_dan_tunjangan = newTblReportHonorDetails.total_honor_per_kelas + newTblReportHonorDetails.honor_tunjangan;
                                newTblReportHonorDetails.kd_nik_xls = totRowReportHonor.kd_nik;
                                newTblReportHonorDetails.sks = tblReportHonorDosen.sks;
                                newTblReportHonorDetails.honor_per_sks = totRowReportHonor.honor_per_sks;
                                newTblReportHonorDetails.trans_per_hari = totRowReportHonor.trans_per_hari;
                                db.tbl_report_honor_dosen.Add(newTblReportHonorDetails);

                                totRowReportHonor.jum_transpor = JumTranport;
                                totRowReportHonor.total_transport = TotalTransport;
                                totRowReportHonor.total_keseluruhan = TotalKeseluruhanTanpaTransport +
                                            newTblReportHonorDetails.total_honor_dan_tunjangan + TotalTransport;
                            }
                        }
                        else
                        {
                            if (db.tbl_report_honor_dosen.Where(d => d.reportid == tblReportHonorDosen.reportid && d.kelas != tblReportHonorDosen.kelas).FirstOrDefault() != null)
                            {
                                tbl_report_honor_dosen tblReportTotSum = db.tbl_report_honor_dosen.Where(d => d.reportid == tblReportHonorDosen.reportid && d.kd_nik != null && d.kd_nik == tblReportHonorDosen.kd_nik_xls).FirstOrDefault();
                                if (findTgl == 0)
                                {
                                    //newTblReportHonorDetails.tanggal = lastTgls + " " + newTgl;
                                    if (tblDosen.status_dosen_id == 1)
                                    {
                                        DateTime Tanggal = DateTime.ParseExact(newTgl, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                        int dayOfweek = (int)Tanggal.DayOfWeek;
                                        if (dayOfweek == 5 || dayOfweek == 6)
                                        {
                                            JumTranport = JumTranport + 1;
                                            TotalTransport = Convert.ToDecimal(newTblReportHonorDetails.jum_transpor.Value) * Convert.ToDecimal(newTblReportHonorDetails.trans_per_hari.Value);

                                        }
                                    }
                                    else
                                    {
                                        JumTranport = JumTranport + 1;
                                        TotalTransport = Convert.ToDecimal(newTblReportHonorDetails.jum_transpor.Value) * Convert.ToDecimal(newTblReportHonorDetails.trans_per_hari.Value);

                                    }
                                }

                                newTblReportHonorDetails.tanggal = newTgl;
                                newTblReportHonorDetails.jum_hadir = tblReportHonorDosen.jum_hadir;
                                newTblReportHonorDetails.total_honor_per_kelas = 0;
                                newTblReportHonorDetails.honor_tunjangan = totRowReportHonor.honor_tunjangan;
                                newTblReportHonorDetails.total_honor_dan_tunjangan = newTblReportHonorDetails.total_honor_per_kelas + newTblReportHonorDetails.total_honor_tunjangan_per_kelas;
                                newTblReportHonorDetails.kd_nik_xls = totRowReportHonor.kd_nik;
                                newTblReportHonorDetails.sks = tblReportHonorDosen.sks;
                                newTblReportHonorDetails.honor_per_sks = totRowReportHonor.honor_per_sks;
                                newTblReportHonorDetails.trans_per_hari = totRowReportHonor.trans_per_hari;
                                db.tbl_report_honor_dosen.Add(newTblReportHonorDetails);

                                var sum = db.tbl_report_honor_dosen.Where(d => d.reportid == tblReportHonorDosen.reportid)
                                        .GroupBy(d => d.kelas).Select(cl => new
                                        {
                                            totSks = cl.Sum(c => c.sks),
                                        }).ToList();

                                int? totSks = sum.FirstOrDefault().totSks;
                                int? minTotSks = db.tbl_dosen_minimum_sks.Where(d => d.status_dosen_id == 3).FirstOrDefault().minimum_sks;


                                int? minJumhadir = db.tbl_report_honor_dosen.Where(d => d.reportid == tblReportHonorDosen.reportid).OrderBy(d => d.jum_hadir).FirstOrDefault().jum_hadir;
                                int? JUmHadir = tblReportHonorDosen.jum_hadir;
                                if (minJumhadir < tblReportHonorDosen.jum_hadir) JUmHadir = minJumhadir;

                                if (totSks > minTotSks)
                                {
                                    tbl_report_honor_dosen lastRowTblDosenFung = db.tbl_report_honor_dosen.Where(d => d.reportid == tblReportHonorDosen.reportid).OrderByDescending(d => d.id).FirstOrDefault();
                                    db.tbl_report_honor_dosen.Remove(lastRowTblDosenFung);
                                    db.SaveChanges();

                                    int? sisaSks = totSks - minTotSks;
                                    tbl_report_honor_dosen newLastRowTblReport = new tbl_report_honor_dosen();
                                    newLastRowTblReport.jum_hadir = JUmHadir;
                                    newLastRowTblReport.total_honor_per_kelas = (tblReportHonorDosen.jum_hadir * totRowReportHonor.honor_per_sks * sisaSks);
                                    newLastRowTblReport.total_honor_tunjangan_per_kelas = (minJumhadir * totRowReportHonor.honor_tunjangan);
                                    newLastRowTblReport.total_honor_dan_tunjangan = newTblReportHonorDetails.total_honor_per_kelas + newTblReportHonorDetails.honor_tunjangan;
                                    newLastRowTblReport.total_transport = TotalTransport;
                                    newTblReportHonorDetails.kd_nik_xls = totRowReportHonor.kd_nik;
                                    newTblReportHonorDetails.sks = tblReportHonorDosen.sks;
                                    newTblReportHonorDetails.honor_per_sks = totRowReportHonor.honor_per_sks;
                                    newTblReportHonorDetails.trans_per_hari = totRowReportHonor.trans_per_hari;

                                    totRowReportHonor.jum_transpor = JumTranport;
                                    totRowReportHonor.total_transport = TotalTransport;
                                    totRowReportHonor.total_keseluruhan = TotalKeseluruhanTanpaTransport +
                                                newTblReportHonorDetails.total_honor_dan_tunjangan + TotalTransport;

                                }
                                db.SaveChanges();
                            }
                        }
                        messege = helper.UpdateSuksesHtml();
                        status = 1;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                messege = helper.ErrorMessageWithElement(ex.ToString());
                status = 0;
            }
            finally
            {
                resultMessage.message = messege;
                resultMessage.status = status;
            }
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        public string templateSlipHtml(view_honor_dosen lstDosen,List<viewHonorMatakKuliahDosen>lstMKDosen,int jmlTransport,DateTime from2,DateTime to,string tahunAngkatan,string semester)
        {
            string tgl = NOW.Day.ToString() + ConvertToNamaBulan(NOW.Month)+NOW.Year.ToString();
            return htmlSlipHead() + htmlSlipHeadInfo(lstDosen, from2, to, tahunAngkatan, semester) +
                htmlSlipDetailHonor(lstMKDosen, jmlTransport,lstDosen.transpor_per_hari ?? 0) + htmlSlipFooter(tgl);
        }

        public string htmlSlipHead()
        {
            return @"<!DOCTYPE html>
<html>
<head><title></title>
    <style>
        .listHonor {
            text-align: justify;
            text-justify: inter-word;
        }
        .header {
            width:300px;
            text-align:center
        }
        .inner {
            width: 13%;
            margin: 0 auto;
            text-align:left;
        }
         .inner2 {
            width: 30%;
            margin: 0 auto;
            text-align:center;
        }
        .logo {
          float: left;
          width: 20%;
          background-color: #CCF;
        }
        .wrapper {
          margin-right: -100px;
          margin-left:30px;
        }
        .content {
          float: left;
          width: 20%;
        }
        .contentFirst {
            margin-left:10%;
          float: left;
          width: 20%;
        }
        .content p {
            margin-top:-10px;
        }
        .face {
          float: left;
          width: 5%;
         
        }
        .face p {
        margin-top:-10px;
        }
        .sidebar {
          float: right;
          width: 30%;

        }
        .sidebar p {
            margin-top:-10px;
        }
        .cleared {
          clear: both;
        }

        .left {
            text-align:left;
        }

        .strong {
            text-decoration:solid;
        }
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: solid;
            border-color:black;
            border-width: 1px;
        } 
        .contentLi {
          float: left;
          width: 30%;
          margin-top:-15px;

        }
        .contentTtd {
          float: left;
          width: 30%;
          margin-top:-15px;

        }

    </style>
</head>
<body style='margin-right:10%;margin-left:10%'>
<div class='header'>
<p>SEKOLAH TINGGI MANAJEMEN TRANSPORTASI</p>
<p>TRISAKTI-JAKARTA</p>
</div>
<div class='logo'>
    <label>LOGO</label>
</div>";
        }

        public string htmlSlipHeadInfo(view_honor_dosen vhdosen,DateTime from2,DateTime to,string tahunAngkatan,string semester)
        {
            int tahunAnjaran1 = to.Year;
            int tahunAnjaran2 = to.Year - 1;
            return @"<div class='inner2' style='text-align:center;'>
                    <p style='text-decoration: underline;'><strong>SLIP HONOR MENGAJAR</strong></p>
                    <p >BULAN " + ConvertToNamaBulan(to.Month) + " " + to.Year + @" (" + from2.Day + " " + ConvertToNamaBulanSingkat(from2.Month) +
                     @" s/d " + to.Day + " " + ConvertToNamaBulanSingkat(to.Month) + " " + to.Year + @")</p>
                </div>
                <div class='inner'>
                    <p>T.A&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;" + tahunAngkatan +
                    @"</p>
                    <p>Semester&nbsp;&nbsp;:&nbsp;&nbsp;" + semester + @"</p>
                </div>
                <br />
                <hr style='width:95%'>
                <br />
                <div class='wrapper'>
                    <div class='content'><p>NIK</p><p>Nama</p><p>Tgl. Mulai Mengajar</p><p>Pendidikan</p></div>
                    <div class='face'><p>:</p><p>:</p><p>:</p><p>:</p></div>
                    <div class='content'><p>&nbsp;" + vhdosen.kd_nik + @"</p><p>&nbsp;" + vhdosen.nama_dosen + @"</p><p>&nbsp;" + vhdosen.tanggal_sk + @"</p><p>&nbsp;" + vhdosen.tingkat_pendidikan + @"</p></div>
                    <div class='content'><p>Jabatan Akademik</p><p>Status Dosen</p><p>Kelompok Dosen</p></div>
                    <div class='face'><p>:</p><p>:</p><p>:</p></div>
                    <div class='content'><p>&nbsp;" + vhdosen.jabatan_fungsional + @"</p><p>&nbsp;" + vhdosen.status_dosen + @"</p><p>C</p></div>
                </div>
                <div class='cleared'></div>
                <hr style='width:95%'>";
        }

        public string htmlSlipDetailHonor(List<viewHonorMatakKuliahDosen> lstMKDosen, int jmlTransport, decimal transport)
        {
            int i = 1;
            string html= @"  <div class='wrapper'>
                <p class='left'><strong>Honor Mengajar</strong>  </p>";
            double totalHonorMengajar=0;
            foreach(var item in lstMKDosen){
               double honorMengajarMK=item.jumlahHadir*item.credit_course*item.honor_per_sks??0;
               if(i<lstMKDosen.Count()){
                   html=html+@" <div class='contentLi'><p>&#9632;&nbsp;&nbsp;"+item.code_course+@"&nbsp;&nbsp;"+item.kelas+@"</p></div>
                        <div class='contentLi'> <p>"+item.jumlahHadir+@"&nbsp; X&nbsp;"+item.credit_course+@" SKS X "+item.honor_per_sks+@" </p></div>
                        <div class='contentLi'> <p>=&nbsp; Rp&nbsp;"+honorMengajarMK+@" </p></div>";
               }
               else{
                     html=html+@" <div class='contentLi'><p>&#9632;&nbsp;&nbsp;"+item.code_course+@"&nbsp;&nbsp;"+item.kelas+@"</p></div>
                        <div class='contentLi'> <p>"+item.jumlahHadir+@"&nbsp; X&nbsp;"+item.credit_course+@" SKS X "+item.honor_per_sks+@" </p></div>
                        <div class='contentLi'> <p style='text-decoration:underline'>=&nbsp; Rp&nbsp;"+(item.jumlahHadir*item.credit_course*item.honor_per_sks)+@"+ </p></div>";
               }
                totalHonorMengajar=totalHonorMengajar+honorMengajarMK;
                i++;
            }
            
             html=html+@" <div class='contentLi'><p>Jumlah Honor Mengajar</p>   </div>
                    <div class='contentLi'><p></p></div>
                    <div class='contentLi'><p>=&nbsp; Rp&nbsp;"+totalHonorMengajar+@" </p></div>

                    <div class='contentLi'><p>Transpor&nbsp;&nbsp;" + jmlTransport + @" X " + transport + @" </p>   </div>
                    <div class='contentLi'><p></p></div>
                    <div class='contentLi'><p style='text-decoration:underline'>=&nbsp; Rp&nbsp;" + (jmlTransport * transport) + @" +</p></div>
                    <div class='contentLi'><p><strong> <i>TOTAL HONOR YANG DITERIMA</i></strong>&nbsp;&nbsp; </p>   </div>
                    <div class='contentLi'><p></p></div>
                    <div class='contentLi'><p >=&nbsp; Rp&nbsp;" + (totalHonorMengajar + (jmlTransport * Convert.ToDouble(transport))) + @" </p></div>
                </div>";
            return html;
        }

        public string htmlSlipFooter(string tgl)
        {
            return @"<div class='cleared'></div>
                    <br />
                <div class='wrapper'>
                    <div class='sidebar'><p>Jakarta, "+tgl+@"</p></div>
                </div>
                    <div class='cleared'></div>
                <div class='wrapper'>
                    <div class='contentTtd'><p>Yang Membayar,</p>   </div>
                    <div class='contentTtd'><p></p></div>
                    <div class='sidebar'><p >Yang Menerima </p></div>
                </div> 
                    <br />
                    <br />
                    <br />
                    <br />
                 <div class='wrapper'>   
                    <div class='contentTtd'><p>Bendahara</p>   </div>
                    <div class='contentTtd'><p></p></div>
                    <div class='sidebar'><p >Miskul Firdaus,SE,MM,QIA </p></div>
                </div>
                </body>
                </html>";
        }

        public string templateRekapDetail()
        {
            return "";
        }

        public string htmlReportDetailHeader()
        {
            string html = @"<!DOCTYPE html>
<html>
<head><title></title>
    <style>
        .listHonor {
            text-align: justify;
            text-justify: inter-word;
        }
        .header {
            width:300px;
            text-align:center
        }
        .inner {
            width: 100%;
            margin: 10px auto;
            text-align:center;
        }
         .inner2 {
            width: 30%;
            margin: 0 auto;
            text-align:center;
        }
        .logo {
          float: left;
          width: 20%;
          background-color: #CCF;
        }
        .wrapper {
          /*margin-right: -100px;
          margin-left:30px;*/
        }
        .content {
          float: left;
          width: 20%;
        }
        .contentFirst {
            margin-left:10%;
          float: left;
          width: 20%;
        }
        .content p {
            margin-top:-10px;
        }
        .face {
          float: left;
          width: 5%;
         
        }
        .face p {
        margin-top:-10px;
        }
        .sidebar {
          float: right;
          width: 30%;

        }
        .sidebar p {
            margin-top:-10px;
        }
        .cleared {
          clear: both;
        }

        .left {
            text-align:left;
        }

        .strong {
            text-decoration:solid;
        }
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: solid;
            border-color:black;
            border-width: 1px;
        } 
        .contentLi {
          float: left;
          width: 30%;
          margin-top:-15px;

        }
        .contentTtd {
          float: left;
          width: 30%;
          margin-top:-15px;

        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th {
            font-size:small;
        }
        
        td {
            font-size:x-small;
        }

        .textright {
            text-align:right;
        }

        .width-2 {
            width:2%;
        }
        .width-3 {
            width:3%;
        }
        .width-5 {
            width:5%;
        }
        .width-10 {
            width:10%;
        }
        .width-15 {
            width:15%;
        }
        .width-20 {
            width:20%;
        }

    </style>
</head>

<body style='margin-right:10%;margin-left:10%'>
    <div class='wrapper'>   
       <div class='inner' style='text-align:center;'>
            <p style='text-decoration: underline;'><strong>REKAP HONOR DOSEN</strong></p>
            <p >Bulan November 2015 (11/1/2015 s/d 11/12/2015)</p>
        </div>
        <div class='inner' style='text-align:center;'>
            <table>
                <tr>
                    <th class='width-2'>N0</th>
                    <th class='width-3'>KD NIK</th>
                    <th class='width-10'>NAMA DOSEN</th>
                    <th class='width-2'>Gol</th>
                    <th class='width-10'>STATUS DOSEN</th>
                    <th class='width-15'>MATA KULIAH</th>
                    <th class='width-3'>SKS</th>
                    <th class='width-10'> KELAS </th>
                    <th class='width-10'> TANGGAL </th>
                    <th class='width-2'> JML H </th>
                    <th class='width-10'> HONOR/SKS </th>
                    <th class='width-10'>HONOR TRANSPORT </th>
                    <th class='width-2'> JML T </th>
                    <th class='width-10'>HONOR MENGAJAR</th>
                    <th class='width-10'>PRAKTISI</th>
                    <th class='width-10'>JUM HONOR</th>
                    <th class='width-10'>TOTAL TRANSPORT</th>
                    <th class='width-10'>TOTAL KESELURUHAN</th>
                </tr>";
            return html;
        }

        public string htmlReportDetailDetails(sp_get_honor_dosen_Result item)
        {
            string html = @"<tr><td>" + item.no.ToString() + @"</td><td>" +
                        item.kd_nik + @"</td><td>" +
                        item.nama_dosen + @"</td><td>" +
                        item.golongan + @"</td><td>" +
                        item.status_dosen + @"</td><td>" +
                        item.mata_kuliah + @"</td><td>" +
                        item.sks + @"</td><td>" +
                        item.kelas + @"</td><td>" +
                        item.tanggal + @"</td><td>" +
                        item.jum_hadir + @"</td><td class='textright'>" +
                        item.honor_per_sks + @"</td><td class='textright'>" +
                        item.trans_per_hari + @"</td><td>" +
                        item.jum_transpor + @"</td><td class='textright'>" +
                        item.total_honor_per_kelas + @"</td><td class='textright'>" +
                        item.total_honor_tunjangan_per_kelas + @"</td><td class='textright'>" +
                        item.total_honor_dan_tunjangan_per_kelas + @"</td><td class='textright'>" +
                        item.total_transport + @"</td><td class='textright'>" +
                        item.total_keseluruhan + @"</td class='textright'>" +
                        @"</tr>";
            return html;
        }

        public string htmlReportDetailFooter()
        {
            return @"</table>            
                    </div>
                </div>
            </body>
            </html>";
        }

        public string htmlReportTotalHeader()
        {
            string html = @"
<!DOCTYPE html>
<html>
<head><title></title>
    <style>
        .listHonor {
            text-align: justify;
            text-justify: inter-word;
        }
        .header {
            width:300px;
            text-align:center
        }
        .inner {
            width: 100%;
            margin: 10px auto;
            text-align:center;
        }
         .inner2 {
            width: 30%;
            margin: 0 auto;
            text-align:center;
        }
        .logo {
          float: left;
          width: 20%;
          background-color: #CCF;
        }
        .wrapper {
          /*margin-right: -100px;
          margin-left:30px;*/
        }
        .content {
          float: left;
          width: 20%;
        }
        .contentFirst {
            margin-left:10%;
          float: left;
          width: 20%;
        }
        .content p {
            margin-top:-10px;
        }
        .face {
          float: left;
          width: 5%;
         
        }
        .face p {
        margin-top:-10px;
        }
        .sidebar {
          float: right;
          width: 30%;

        }
        .sidebar p {
            margin-top:-10px;
        }
        .cleared {
          clear: both;
        }

        .left {
            text-align:left;
        }

        .strong {
            text-decoration:solid;
        }
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: solid;
            border-color:black;
            border-width: 1px;
        } 
        .contentLi {
          float: left;
          width: 30%;
          margin-top:-15px;

        }
        .contentTtd {
          float: left;
          width: 30%;
          margin-top:-15px;

        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th {
            font-size:small;
        }
        
        td {
            font-size:x-small;
        }
        
        .textright {
            text-align:right;
        }

        .width-2 {
            width:2%;
        }
        .width-3 {
            width:3%;
        }
        .width-5 {
            width:5%;
        }
        .width-10 {
            width:10%;
        }
        .width-15 {
            width:15%;
        }
        .width-20 {
            width:20%;
        }

    </style>
</head>

<body style='margin-right:10%;margin-left:10%'>
    <div class='wrapper'>   
       <div class='inner' style='text-align:center;'>
            <p style='text-decoration: underline;'><strong>REKAP HONOR DOSEN</strong></p>
            <p >Bulan November 2015 (11/1/2015 s/d 11/12/2015)</p>
        </div>
        <div class='inner' style='text-align:center;'>
            <table>
                <tr>
                    <th class='width-2'>N0</th>
                    <th class='width-3'>KD NIK</th>
                    <th class='width-10'>NAMA DOSEN</th>
                    <th class='width-10'>No Rekening</th>
                    <th class='width-10'>JML YG di Terima I</th>
                    <th class='width-10'>Biaya Bank I</th>
                    <th class='width-10'>JML YG di Terima II</th>
                    <th class='width-10'>Nama Bank</th>
                    <th class='width-10'>Nama di Rekening</th>
                    <th class='width-10'>K.B</th>";
            return html;
        }

        public string htmlReportTotalDetails(sp_get_honor_dosen_Result item)
        {
            string html = @"<tr><td>" + item.no.ToString() + @"</td><td>" +
                        item.kd_nik_xls + @"</td><td>" +
                        item.nama_dosen_xls + @"</td><td>" +
                        item.no_rekening + @"</td><td class='textright'>" +
                        item.total_keseluruhan + @"</td><td>" +
                        "" + @"</td><td>" +
                        item.total_keseluruhan + @"</td><td class='textright'>" +
                        item.nama_bank_cabang + @"</td><td>" +
                        item.atas_nama + @"</td><td>" +
                        item.kode_bank + @"</td>" +
                        @"</tr>";
            return html;
        }

        public string htmlReportTotalFooter()
        {
            return @" </table>            
                    </div>
                </div>
            </body>
            </html>";
        }

        public ActionResult ReportDetailsPdf(int reportId)
        {
            try
            {
                LocalReport lr = new LocalReport();
                string path = Path.Combine(Server.MapPath("~/Report"), "ReportHonorDetails.rdlc");
                if (System.IO.File.Exists(path))
                {
                    lr.ReportPath = path;
                }
                else
                {
                    return View("Index");
                }
                List<tbl_report_honor_dosen> cm = new List<tbl_report_honor_dosen>();
                string param1 = "";
                string param2 = "";
                string paramSemester = "";
                string paramTahunAjaran = "";
                using (stmtsdmEntities dc = new stmtsdmEntities())
                {
                    tbl_generate_report tblGenReport = new tbl_generate_report();
                    tblGenReport = dc.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();

                    if (tblGenReport == null)
                    {
                        return Content("Report Tidak Ada");
                    }
                    cm = dc.tbl_report_honor_dosen.Where(d => d.reportid == reportId).ToList();
                    foreach (var item in cm)
                    {
                        if (item.jum_transpor == null) item.jum_transpor = 0;
                        if (item.total_transport == null) item.total_transport = 0;
                        if (item.total_honor_tunjangan_per_kelas == null) item.total_honor_tunjangan_per_kelas = 0;
                    }
                    param1 = @"" + tblGenReport.report_start.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) + " " + tblGenReport.report_start.Value.Year +
                            " s/d " + tblGenReport.report_end.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_end.Value.Month) + " " + tblGenReport.report_end.Value.Year;
                    param2 = tblGenReport.periode == null ? "" : tblGenReport.periode;
                    paramTahunAjaran = tblGenReport.tahun_ajaran == null ? "" : tblGenReport.tahun_ajaran;
                    paramSemester = tblGenReport.semester == null ? "" : tblGenReport.semester;


                }
                string KabagAkademik = ConfigurationManager.AppSettings["KabagAkademik"].ToString();
                string Penjadwalan = ConfigurationManager.AppSettings["Penjadwalan"].ToString();
                string WaketII = ConfigurationManager.AppSettings["WaketII"].ToString();
                string WaketI = ConfigurationManager.AppSettings["WaketI"].ToString();
                ReportDataSource rd = new ReportDataSource("DataSet2", cm);


                ReportParameter par1 = new ReportParameter("param1", param1);
                ReportParameter par2 = new ReportParameter("param2", param2);
                ReportParameter par3 = new ReportParameter("paramSemester", paramSemester);
                ReportParameter par4 = new ReportParameter("paramTahunAjaran", paramTahunAjaran);
                ReportParameter par5 = new ReportParameter("KabagAkademik", KabagAkademik);
                ReportParameter par6 = new ReportParameter("Penjadwalan", Penjadwalan);
                ReportParameter par7 = new ReportParameter("WaketII", WaketII);
                ReportParameter par8 = new ReportParameter("WaketI", WaketI);
                lr.SetParameters(new ReportParameter[] { par1, par2, par3, par4, par5, par6, par7, par8 });
                lr.DataSources.Add(rd);

                string reportType = "pdf";
                string mimeType;
                string encoding;
                string fileNameExtension;



                string deviceInfo =

               "<DeviceInfo>" +
              "  <OutputFormat>PDF</OutputFormat>" +
              "  <PageWidth>12.692in</PageWidth>" +
              "  <PageHeight>8.267in</PageHeight>" +
              "  <MarginTop>0.25in</MarginTop>" +
              "  <MarginLeft>0.25in</MarginLeft>" +
              "  <MarginRight>0.25in</MarginRight>" +
              "  <MarginBottom>0.25in</MarginBottom>" +
              "</DeviceInfo>";

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                renderedBytes = lr.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);


                return File(renderedBytes, mimeType);
            }
            catch (ReflectionTypeLoadException ex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Exception exSub in ex.LoaderExceptions)
                {
                    sb.AppendLine(exSub.Message);
                    FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
                    if (exFileNotFound != null)
                    {
                        if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                        {
                            sb.AppendLine("Fusion Log:");
                            sb.AppendLine(exFileNotFound.FusionLog);
                        }
                    }
                    sb.AppendLine();
                }
                return Content(sb.ToString());
                //Display or log the error based on your application.
            }
        }

        public ActionResult ReportDetailsExcel(int reportId)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "ReportHonorDetails.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                //return View("Index");
            }
            List<tbl_report_honor_dosen> cm = new List<tbl_report_honor_dosen>();
            string param1 = "";
            string filename="";
            string param2 = "";
            string paramSemester = "";
            string paramTahunAjaran = "";
            using (stmtsdmEntities dc = new stmtsdmEntities())
            {
                tbl_generate_report tblGenReport = new tbl_generate_report();
                tblGenReport = dc.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();
                if (tblGenReport == null)
                {
                   return Content("Report Tidak Ada");
                }
                cm = dc.tbl_report_honor_dosen.Where(d => d.reportid == reportId).ToList();
                foreach (var item in cm)
                {
                    if (item.jum_transpor == null) item.jum_transpor = 0;
                    if (item.total_transport == null) item.total_transport = 0;
                    if (item.total_honor_tunjangan_per_kelas == null) item.total_honor_tunjangan_per_kelas = 0;
                }
                param1 = @"" + tblGenReport.report_start.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) + " " + tblGenReport.report_start.Value.Year +
                        " s/d " + tblGenReport.report_end.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_end.Value.Month) + " " + tblGenReport.report_end.Value.Year;

                filename=tblGenReport.nama_file;
                param2 = tblGenReport.periode == null ? "" : tblGenReport.periode;
                paramTahunAjaran = tblGenReport.tahun_ajaran == null ? "" : tblGenReport.tahun_ajaran;
                paramSemester = tblGenReport.semester == null ? "" : tblGenReport.semester;
                //var from = DateTime.Parse("10/1/2015");
                //var to = DateTime.Parse("10/28/2015");

                //cm = dc.view_honor_dosen.OrderBy(d => d.kd_nik).Where(d => d.date_document >= from
                //    && d.date_document <= to).ToList();
            }
            string KabagAkademik = ConfigurationManager.AppSettings["KabagAkademik"].ToString();
            string Penjadwalan = ConfigurationManager.AppSettings["Penjadwalan"].ToString();
            string WaketII = ConfigurationManager.AppSettings["WaketII"].ToString();
            string WaketI = ConfigurationManager.AppSettings["WaketI"].ToString();
            ReportDataSource rd = new ReportDataSource("DataSet2", cm);


            ReportParameter par1 = new ReportParameter("param1", param1);
            ReportParameter par2 = new ReportParameter("param2", param2);
            ReportParameter par3 = new ReportParameter("paramSemester", paramSemester);
            ReportParameter par4 = new ReportParameter("paramTahunAjaran", paramTahunAjaran);
            ReportParameter par5 = new ReportParameter("KabagAkademik", KabagAkademik);
            ReportParameter par6 = new ReportParameter("Penjadwalan", Penjadwalan);
            ReportParameter par7 = new ReportParameter("WaketII", WaketII);
            ReportParameter par8 = new ReportParameter("WaketI", WaketI);
            lr.SetParameters(new ReportParameter[] { par1, par2, par3, par4, par5, par6, par7, par8 });
            lr.DataSources.Add(rd);

            string reportType = "xls";
            string mimeType;
            string encoding;
            string fileNameExtension;


            string[] streamids = null;
            String extension = null;
            Byte[] bytes = null;
            Warning[] warnings;

            bytes = lr.Render("Excel", null, out mimeType, out encoding, out extension, out streamids, out warnings);
            Response.Clear();
            Response.Buffer = true;
            Response.Clear();

            //Response.AddHeader("Content-Disposition", "attachment; filename=abc.xls");
            //Response.AddHeader("Content-Length", bytes.Length.ToString());
            //Response.BinaryWrite(bytes);
            //Response.End();

            return File(bytes, "application/vnd.ms-excel", filename+"_Details"+".xls");
        }

        public ActionResult ReportSumPdf(int reportId)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "ReportHonorSum.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<tbl_report_honor_dosen> cm = new List<tbl_report_honor_dosen>();
            string param1 = "";
            string param2 = "";
            string paramTahunAjaran = "";
            string paramSemester = "";
            using (stmtsdmEntities dc = new stmtsdmEntities())
            {
                tbl_generate_report tblGenReport = new tbl_generate_report();
                tblGenReport = dc.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();
                if (tblGenReport == null)
                {
                    return Content("Report Tidak Ada");
                }
                cm = dc.tbl_report_honor_dosen.Where(d => d.reportid == reportId && d.nomor!=null).OrderBy(d=>d.kode_bank).ToList();
                int i = 1;
                foreach (var item in cm)
                {
                    item.nomor = i;
                    i++;
                }
                param1 = @"" + tblGenReport.report_start.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) + " " + tblGenReport.report_start.Value.Year +
                        " s/d " + tblGenReport.report_end.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_end.Value.Month) + " " + tblGenReport.report_end.Value.Year;
                param2 = tblGenReport.periode==null?"":tblGenReport.periode;
                paramTahunAjaran = tblGenReport.tahun_ajaran == null ? "" : tblGenReport.tahun_ajaran;
                paramSemester = tblGenReport.semester == null ? "" : tblGenReport.semester;
            }
            ReportDataSource rd = new ReportDataSource("DataSet2", cm);
            string KabagKeuangan = ConfigurationManager.AppSettings["KabagKeuangan"].ToString();
            string Menyiapkan = ConfigurationManager.AppSettings["Menyiapkan"].ToString();
            string totalBNI = cm.Where(d => d.kode_bank == "1" || d.kode_bank != "2").Sum(d => d.total_keseluruhan).ToString();
            string totalNonBNI = cm.Where(d => d.kode_bank != "1"&& d.kode_bank != "2").Sum(d => d.total_keseluruhan).ToString();
            string totalBanyakNonBNI = cm.Where(d => d.kode_bank != "1" && d.kode_bank != "2").Count().ToString();

            ReportParameter par1 = new ReportParameter("param1", param1);
            ReportParameter par2 = new ReportParameter("param2", param2);
            ReportParameter par3 = new ReportParameter("paramSemester", paramSemester);
            ReportParameter par4 = new ReportParameter("paramTahunAjaran", paramTahunAjaran);
            ReportParameter par5 = new ReportParameter("KabagKeuangan", KabagKeuangan);
            ReportParameter par6 = new ReportParameter("Menyiapkan", Menyiapkan);
            ReportParameter par7 = new ReportParameter("totalBNI", totalBNI);
            ReportParameter par8 = new ReportParameter("totalNonBNI", totalNonBNI);
            ReportParameter par9 = new ReportParameter("totalBanyakNonBNI", totalBanyakNonBNI);

            lr.SetParameters(new ReportParameter[] { par1, par2, par3, par4, par5, par6, par7,par8, par9 });
            lr.DataSources.Add(rd);

            string reportType = "pdf";
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

           "<DeviceInfo>" +
          "  <OutputFormat>PDF</OutputFormat>" +
          "  <PageWidth>11.692in</PageWidth>" +
          "  <PageHeight>8.267in</PageHeight>" +
          "  <MarginTop>0.25in</MarginTop>" +
          "  <MarginLeft>0.25in</MarginLeft>" +
          "  <MarginRight>0.25in</MarginRight>" +
          "  <MarginBottom>0.25in</MarginBottom>" +
          "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }

        public ActionResult ReportSumExcel(int reportId)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "ReportHonorSum.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<tbl_report_honor_dosen> cm = new List<tbl_report_honor_dosen>();
            string param1 = "";
            string filename = "";
            string param2 = "";
            string paramTahunAjaran = "";
            string paramSemester = "";
            using (stmtsdmEntities dc = new stmtsdmEntities())
            {
                tbl_generate_report tblGenReport = new tbl_generate_report();
                tblGenReport = dc.tbl_generate_report.Where(d => d.id == reportId ).FirstOrDefault();
                if (tblGenReport == null)
                {
                    return Content("Report Tidak Ada");
                }
                cm = dc.tbl_report_honor_dosen.Where(d => d.reportid == reportId && d.nomor != null).OrderBy(d=>d.kode_bank).ToList();
                int i = 1;
                foreach (var item in cm)
                {
                    item.nomor = i;
                    i++;
                }
                param1 = @"" + tblGenReport.report_start.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) + " " + tblGenReport.report_start.Value.Year +
                        " s/d " + tblGenReport.report_end.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_end.Value.Month) + " " + tblGenReport.report_end.Value.Year;

                filename = tblGenReport.nama_file;
                param2 = tblGenReport.periode==null?"":tblGenReport.periode;
                paramTahunAjaran = tblGenReport.tahun_ajaran == null ? "" : tblGenReport.tahun_ajaran;
                paramSemester = tblGenReport.semester == null ? "" : tblGenReport.semester;
                //var from = DateTime.Parse("10/1/2015");
                //var to = DateTime.Parse("10/28/2015");

                //cm = dc.view_honor_dosen.OrderBy(d => d.kd_nik).Where(d => d.date_document >= from
                //    && d.date_document <= to).ToList();
            }
            ReportDataSource rd = new ReportDataSource("DataSet2", cm);
            string KabagKeuangan = ConfigurationManager.AppSettings["KabagKeuangan"].ToString();
            string Menyiapkan = ConfigurationManager.AppSettings["Menyiapkan"].ToString();
            string totalBNI = cm.Where( d => d.kode_bank == "1" || d.kode_bank != "2").Sum(d => d.total_keseluruhan).ToString();
            string totalNonBNI = cm.Where(d => d.kode_bank != "1" && d.kode_bank != "2").Sum(d => d.total_keseluruhan).ToString();
            string totalBanyakNonBNI = cm.Where(d => d.total_keseluruhan>0 &&( d.kode_bank != "1" && d.kode_bank != "2")).Count().ToString();

            ReportParameter par1 = new ReportParameter("param1", param1);
            ReportParameter par2 = new ReportParameter("param2", param2);
            ReportParameter par3 = new ReportParameter("paramSemester", paramSemester);
            ReportParameter par4 = new ReportParameter("paramTahunAjaran", paramTahunAjaran);
            ReportParameter par5 = new ReportParameter("KabagKeuangan", KabagKeuangan);
            ReportParameter par6 = new ReportParameter("Menyiapkan", Menyiapkan);
            ReportParameter par7 = new ReportParameter("totalBNI", totalBNI);
            ReportParameter par8 = new ReportParameter("totalNonBNI", totalNonBNI);
            ReportParameter par9 = new ReportParameter("totalBanyakNonBNI", totalBanyakNonBNI);

            lr.SetParameters(new ReportParameter[] { par1, par2, par3, par4, par5, par6, par7, par8, par9 });
            lr.DataSources.Add(rd);

            string reportType = "xls";
            string mimeType;
            string encoding;
            string fileNameExtension;


            string[] streamids = null;
            String extension = null;
            Byte[] bytes = null;
            Warning[] warnings;

            bytes = lr.Render("Excel", null, out mimeType, out encoding, out extension, out streamids, out warnings);
            Response.Clear();
            Response.Buffer = true;
            Response.Clear();

            //Response.AddHeader("Content-Disposition", "attachment; filename=abc.xls");
            //Response.AddHeader("Content-Length", bytes.Length.ToString());
            //Response.BinaryWrite(bytes);
            //Response.End();

            return File(bytes, "application/vnd.ms-excel", filename+"_Summary_"+DateTime.Now.ToShortDateString() + ".xls");
        }

        public ActionResult ReportDetailsInterPdf(int reportId)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "ReportHonorDetailsInter.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<tbl_report_honor_dosen> cm = new List<tbl_report_honor_dosen>();
            string param1 = "";
            string param2 = "";
            string paramSemester = "";
            string paramTahunAjaran = "";
            using (stmtsdmEntities dc = new stmtsdmEntities())
            {
                tbl_generate_report tblGenReport = new tbl_generate_report();
                tblGenReport = dc.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();
                if (tblGenReport == null)
                {
                    return Content("Report Tidak Ada");
                }
                cm = dc.tbl_report_honor_dosen.Where(d => d.reportid == reportId).ToList();
                foreach (var item in cm)
                {
                    if (item.jum_transpor == null) item.jum_transpor = 0;
                    if (item.total_transport == null) item.total_transport = 0;
                    if (item.total_honor_tunjangan_per_kelas == null) item.total_honor_tunjangan_per_kelas = 0;
                    if (item.kd_nik != null)
                    {
                        tbl_dosen tblDosen = dc.tbl_dosen.Where(d => d.kd_nik == item.kd_nik).FirstOrDefault();
                        if (tblDosen != null)
                        {
                            tbl_pegawai tblPegawai = dc.tbl_pegawai.Where(d => d.id == tblDosen.pegawai_id).FirstOrDefault();
                            if (tblPegawai != null)
                            {
                                tbl_rekening tblRekening = dc.tbl_rekening.Where(d => d.pegawai_id == tblPegawai.id).FirstOrDefault();                                
                                if (tblRekening != null)
                                {
                                    item.no_rekening = tblRekening.no_rekening;
                                    item.atas_nama = tblRekening.atas_nama;                                    
                                    item.nama_bank_cabang = tblRekening.nama_bank_cabang;
                                    item.kode_bank = tblRekening.kode_bank;
                                    if (tblRekening.bank_id != null)
                                    {
                                        tbl_ref_bank tblRefBank = dc.tbl_ref_bank.Where(d => d.id == tblRekening.bank_id).FirstOrDefault();
                                        item.nama_bank = tblRefBank.nama_bank;
                                        item.biaya_bank = tblRefBank.biaya_bank;
                                    }                                    
                                }
                            }
                        }
                    }
                }
                param1 = @"" + tblGenReport.report_start.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) + " " + tblGenReport.report_start.Value.Year +
                        " s/d " + tblGenReport.report_end.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_end.Value.Month) + " " + tblGenReport.report_end.Value.Year;
                param2 = tblGenReport.periode == null ? "" : tblGenReport.periode;
                paramTahunAjaran = tblGenReport.tahun_ajaran == null ? "" : tblGenReport.tahun_ajaran;
                paramSemester = tblGenReport.semester == null ? "" : tblGenReport.semester;


            }
            string KabagAkademik = ConfigurationManager.AppSettings["KabagAkademik"].ToString();
            string Penjadwalan = ConfigurationManager.AppSettings["Penjadwalan"].ToString();
            string WaketII = ConfigurationManager.AppSettings["WaketII"].ToString();
            string WaketI = ConfigurationManager.AppSettings["WaketI"].ToString();
            ReportDataSource rd = new ReportDataSource("DataSet2", cm);


            ReportParameter par1 = new ReportParameter("param1", param1);
            ReportParameter par2 = new ReportParameter("param2", param2);
            ReportParameter par3 = new ReportParameter("paramSemester", paramSemester);
            ReportParameter par4 = new ReportParameter("paramTahunAjaran", paramTahunAjaran);
            ReportParameter par5 = new ReportParameter("KabagAkademik", KabagAkademik);
            ReportParameter par6 = new ReportParameter("Penjadwalan", Penjadwalan);
            ReportParameter par7 = new ReportParameter("WaketII", WaketII);
            ReportParameter par8 = new ReportParameter("WaketI", WaketI);
            lr.SetParameters(new ReportParameter[] { par1, par2, par3, par4, par5, par6, par7, par8 });
            lr.DataSources.Add(rd);

            string reportType = "pdf";
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

           "<DeviceInfo>" +
          "  <OutputFormat>PDF</OutputFormat>" +
          "  <PageWidth>11.692in</PageWidth>" +
          "  <PageHeight>8.267in</PageHeight>" +
          "  <MarginTop>0.25in</MarginTop>" +
          "  <MarginLeft>0.25in</MarginLeft>" +
          "  <MarginRight>0.25in</MarginRight>" +
          "  <MarginBottom>0.25in</MarginBottom>" +
          "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }

        public ActionResult ReportDetailsInterExcel(int reportId)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "ReportHonorDetailsInter.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                //return View("Index");
            }
            List<tbl_report_honor_dosen> cm = new List<tbl_report_honor_dosen>();
            string param1 = "";
            string filename = "";
            string param2 = "";
            string paramSemester = "";
            string paramTahunAjaran = "";
            using (stmtsdmEntities dc = new stmtsdmEntities())
            {
                tbl_generate_report tblGenReport = new tbl_generate_report();
                tblGenReport = dc.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();
                if (tblGenReport == null)
                {
                    return Content("Report Tidak Ada");
                }
                cm = dc.tbl_report_honor_dosen.Where(d => d.reportid == reportId).ToList();
                foreach (var item in cm)
                {
                    if (item.jum_transpor == null) item.jum_transpor = 0;
                    if (item.total_transport == null) item.total_transport = 0;
                    if (item.total_honor_tunjangan_per_kelas == null) item.total_honor_tunjangan_per_kelas = 0;
                }
                param1 = @"" + tblGenReport.report_start.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) + " " + tblGenReport.report_start.Value.Year +
                        " s/d " + tblGenReport.report_end.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_end.Value.Month) + " " + tblGenReport.report_end.Value.Year;

                filename = tblGenReport.nama_file;
                param2 = tblGenReport.periode == null ? "" : tblGenReport.periode;
                paramTahunAjaran = tblGenReport.tahun_ajaran == null ? "" : tblGenReport.tahun_ajaran;
                paramSemester = tblGenReport.semester == null ? "" : tblGenReport.semester;
                //var from = DateTime.Parse("10/1/2015");
                //var to = DateTime.Parse("10/28/2015");

                //cm = dc.view_honor_dosen.OrderBy(d => d.kd_nik).Where(d => d.date_document >= from
                //    && d.date_document <= to).ToList();
            }
            string KabagAkademik = ConfigurationManager.AppSettings["KabagAkademik"].ToString();
            string Penjadwalan = ConfigurationManager.AppSettings["Penjadwalan"].ToString();
            string WaketII = ConfigurationManager.AppSettings["WaketII"].ToString();
            string WaketI = ConfigurationManager.AppSettings["WaketI"].ToString();
            ReportDataSource rd = new ReportDataSource("DataSet2", cm);


            ReportParameter par1 = new ReportParameter("param1", param1);
            ReportParameter par2 = new ReportParameter("param2", param2);
            ReportParameter par3 = new ReportParameter("paramSemester", paramSemester);
            ReportParameter par4 = new ReportParameter("paramTahunAjaran", paramTahunAjaran);
            ReportParameter par5 = new ReportParameter("KabagAkademik", KabagAkademik);
            ReportParameter par6 = new ReportParameter("Penjadwalan", Penjadwalan);
            ReportParameter par7 = new ReportParameter("WaketII", WaketII);
            ReportParameter par8 = new ReportParameter("WaketI", WaketI);
            lr.SetParameters(new ReportParameter[] { par1, par2, par3, par4, par5, par6, par7, par8 });
            lr.DataSources.Add(rd);

            string reportType = "xls";
            string mimeType;
            string encoding;
            string fileNameExtension;


            string[] streamids = null;
            String extension = null;
            Byte[] bytes = null;
            Warning[] warnings;

            bytes = lr.Render("Excel", null, out mimeType, out encoding, out extension, out streamids, out warnings);
            Response.Clear();
            Response.Buffer = true;
            Response.Clear();

            //Response.AddHeader("Content-Disposition", "attachment; filename=abc.xls");
            //Response.AddHeader("Content-Length", bytes.Length.ToString());
            //Response.BinaryWrite(bytes);
            //Response.End();

            return File(bytes, "application/vnd.ms-excel", filename + "_Details" + ".xls");
        }

        public ActionResult ReportSlipHonorPdf(int reportId,string kd_nik)
        {
            LocalReport lr = new LocalReport();
            stmtsdmEntities db = new stmtsdmEntities();
            tbl_dosen tblDosenS = db.tbl_dosen.Where(d => d.kd_nik == kd_nik).FirstOrDefault();
            string path = Path.Combine(Server.MapPath("~/Report"), "ReportSlipHonorDosen.rdlc");
            if (tblDosenS != null)
            {
                if (tblDosenS.status_dosen_id == 5)
                {
                    path = Path.Combine(Server.MapPath("~/Report"), "ReportSlipHonorDosenPraktisi.rdlc");
                }
            }
            else
            {
                return Content("Dosen Tidak Ada");
            }
            
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<tbl_report_honor_dosen> cm = new List<tbl_report_honor_dosen>();
            string param1 = "";
            string param2 = "";
            string param3 = "";
            string paramNIk = "";
            string paramNama = "";
            string paramMulaiMengajar = "";
            string paramPendidikan = "";
            string paramJabatan = "";
            string paramStatusDosen = "";
            string paramGolongan = "";
            string paramTotTrans = "";
            string paramTransPerHari = "";
            string paramTotalNgajarPriode = "";
            string paramTotalKeseluruhan = "";

            using (stmtsdmEntities dc = new stmtsdmEntities())
            {
                tbl_generate_report tblGenReport = new tbl_generate_report();                
                tblGenReport = dc.tbl_generate_report.Where(d => d.id == reportId ).FirstOrDefault();
                param2 = tblGenReport.tahun_ajaran.ToString();
                param3 = tblGenReport.semester.ToString(); ;
                if (tblGenReport == null)
                {
                    return Content("Report Tidak Ada");
                }
                cm = dc.tbl_report_honor_dosen.Where(d => d.reportid == reportId && d.kd_nik_xls.Contains(kd_nik)).ToList();
                foreach (var item in cm)
                {
                    if (item.jum_transpor == null) item.jum_transpor = 0;
                    if (item.total_transport == null) item.total_transport = 0;
                    if (item.total_honor_tunjangan_per_kelas == null) item.total_honor_tunjangan_per_kelas = 0;
                }
                param1 = @"Bulan " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) + " (" + tblGenReport.report_start.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) +
                        " s/d " + tblGenReport.report_end.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_end.Value.Month) + " " + tblGenReport.report_end.Value.Year+" )";
                paramNIk = cm.FirstOrDefault().kd_nik_xls;
                paramNama = cm.FirstOrDefault().nama_dosen_xls;
                string KdNIk = cm.FirstOrDefault().kd_nik_xls;
                tbl_dosen tblDosen = dc.tbl_dosen.Where(d => d.kd_nik == KdNIk).FirstOrDefault();
                
                int pegawaiId = tblDosen.pegawai_id.Value;
                tbl_pegawai tblPegawai = dc.tbl_pegawai.Where(d => d.id == pegawaiId).FirstOrDefault();
                DateTime tangglSk=DateTime.Now;
                if (tblPegawai.tanggal_sk != null)
                {
                    tangglSk = tblPegawai.tanggal_sk.Value;
                    paramMulaiMengajar = tangglSk.Day + "/" + tangglSk.Month + "/" + tangglSk.Year;
                }
                else paramMulaiMengajar = " ";
                tbl_dosen_jenjang tblDosenJenjang=tblDosen.tbl_dosen_jenjang.FirstOrDefault();
                int pendidikanid = tblDosenJenjang.pendidikan_id.Value;
                int jabatanid = tblDosenJenjang.jabatan_fungsional_id.Value;
                paramPendidikan = dc.tbl_ref_pendidikan.Where(d => d.id == pendidikanid).FirstOrDefault().tingkat_pendidikan;
                paramJabatan = dc.tbl_ref_jabatan_fungsional.Where(d => d.id == jabatanid).FirstOrDefault().jabatan_fungsional;
                paramStatusDosen = tblDosen.tbl_ref_status_dosen.status_dosen;
                paramGolongan = tblDosenJenjang.tbl_ref_jenjang.jenjang_kelompok;
                paramTotTrans = cm.FirstOrDefault().total_transport.ToString();
                paramTransPerHari = cm.FirstOrDefault().trans_per_hari.ToString();
                paramTotalNgajarPriode = cm.FirstOrDefault().total_honor_per_priode.ToString();
                paramTotalKeseluruhan = cm.FirstOrDefault().total_keseluruhan.ToString();
            }
            string dateNow = NOW.Day.ToString() + " " + ConvertToNamaBulan(NOW.Month) + " " + NOW.Year;

            ReportDataSource rd = new ReportDataSource("DataSet1", cm);


            ReportParameter parm1 = new ReportParameter("param1", param1);
            ReportParameter parm2 = new ReportParameter("paramThnAjaran", param2);
            ReportParameter parm3 = new ReportParameter("paramSemester", param3);
            ReportParameter parmNIk = new ReportParameter("paramNik", paramNIk);
            ReportParameter parmNama = new ReportParameter("paramNama", paramNama);
            ReportParameter parmMulaiMengajar = new ReportParameter("paramMulaiMengajar", paramMulaiMengajar);
            ReportParameter parmPendidikan = new ReportParameter("paramPendidikan", paramPendidikan);
            ReportParameter parmJabatan = new ReportParameter("paramJabatan", paramJabatan);
            ReportParameter parmStatusDosen = new ReportParameter("paramStatusDosen", paramStatusDosen);
            ReportParameter parmGolongan = new ReportParameter("paramGolongan", paramGolongan);
            ReportParameter parmTotTrans = new ReportParameter("paramTotTrans", paramTotTrans);
            ReportParameter parmTransPerHari = new ReportParameter("paramTransPerHari", paramTransPerHari);
            ReportParameter parmTotalNgajarPriode = new ReportParameter("paramTotPriode", paramTotalNgajarPriode);
            ReportParameter parmTotalKeseluruhan = new ReportParameter("paramTotalKeseluruhan", paramTotalKeseluruhan);
            ReportParameter parmTglTTD = new ReportParameter("paramTglTTD", dateNow);

            if (tblDosenS.status_dosen_id == 5)
            {

                lr.SetParameters(new ReportParameter[] { parm1, parm2, parm3, parmMulaiMengajar, parmPendidikan, parmJabatan, parmStatusDosen,
                    parmGolongan,parmTglTTD });
            }
            else
            {
                lr.SetParameters(new ReportParameter[] { parm1, parm2, parm3, parmNIk, parmNama, parmMulaiMengajar, parmPendidikan, parmJabatan,
                parmStatusDosen,parmGolongan,parmTglTTD});
            }
            //lr.SetParameters(new ReportParameter[] { parm1, parm2, parm3, parmNIk, parmNama,
            //    parmMulaiMengajar, parmPendidikan, parmPendidikan,parmJabatan,parmStatusDosen,parmGolongan,parmTotTrans,
            //    parmTransPerHari,parmTotalNgajarPriode,parmTotalKeseluruhan });
            //lr.SetParameters(parm1);
            //lr.SetParameters(parm2);
            //lr.SetParameters(parm3);
            //lr.SetParameters(parmNIk);
            //lr.SetParameters(parmNama);
            //lr.SetParameters(parmMulaiMengajar);
            //lr.SetParameters(parmPendidikan);
            //lr.SetParameters(parmJabatan);
            //lr.SetParameters(parmStatusDosen);
            //lr.SetParameters(parmGolongan);
            //lr.SetParameters(parmTotTrans);
            //lr.SetParameters(parmTransPerHari);
            //lr.SetParameters(parmTotalNgajarPriode);
            //lr.SetParameters(parmTotalKeseluruhan);
            lr.DataSources.Add(rd);

            string reportType = "pdf";
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

           "<DeviceInfo>" +
          "  <OutputFormat>PDF</OutputFormat>" +
          "  <PageWidth>8.267in</PageWidth>" +
          "  <PageHeight>11.692in</PageHeight>" +
          "  <MarginTop>0.25in</MarginTop>" +
          "  <MarginLeft>0.10in</MarginLeft>" +
          "  <MarginRight>0.10in</MarginRight>" +
          "  <MarginBottom>0.25in</MarginBottom>" +
          "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }

        public ActionResult ReportSlipHonorInterPdf(int reportId, string kd_nik)
        {
            LocalReport lr = new LocalReport();
            stmtsdmEntities db = new stmtsdmEntities();
            tbl_dosen tblDosenS = db.tbl_dosen.Where(d => d.kd_nik == kd_nik).FirstOrDefault();
            string path = Path.Combine(Server.MapPath("~/Report"), "ReportSlipHonorDosenInter.rdlc");
            if (tblDosenS != null)
            {
                if (tblDosenS.status_dosen_id == 5)
                {
                    path = Path.Combine(Server.MapPath("~/Report"), "ReportSlipHonorDosenPraktisi.rdlc");
                }
            }
            else
            {
                return Content("Dosen Tidak Ada");
            }

            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<tbl_report_honor_dosen> cm = new List<tbl_report_honor_dosen>();
            string param1 = "";
            string param2 = "";
            string param3 = "";
            string paramNIk = "";
            string paramNama = "";
            string paramMulaiMengajar = "";
            string paramPendidikan = "";
            string paramJabatan = "";
            string paramStatusDosen = "";
            string paramGolongan = "";
            string paramTotTrans = "";
            string paramTransPerHari = "";
            string paramTotalNgajarPriode = "";
            string paramTotalKeseluruhan = "";

            using (stmtsdmEntities dc = new stmtsdmEntities())
            {
                tbl_generate_report tblGenReport = new tbl_generate_report();
                tblGenReport = dc.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();
                param2 = tblGenReport.tahun_ajaran.ToString();
                param3 = tblGenReport.semester.ToString(); ;
                if (tblGenReport == null)
                {
                    return Content("Report Tidak Ada");
                }
                cm = dc.tbl_report_honor_dosen.Where(d => d.reportid == reportId && d.kd_nik_xls.Contains(kd_nik)).ToList();
                foreach (var item in cm)
                {
                    if (item.jum_transpor == null) item.jum_transpor = 0;
                    if (item.total_transport == null) item.total_transport = 0;
                    if (item.total_honor_tunjangan_per_kelas == null) item.total_honor_tunjangan_per_kelas = 0;
                }
                param1 = @"Bulan " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) + " (" + tblGenReport.report_start.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) +
                        " s/d " + tblGenReport.report_end.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_end.Value.Month) + " " + tblGenReport.report_end.Value.Year + " )";
                paramNIk = cm.FirstOrDefault().kd_nik_xls;
                paramNama = cm.FirstOrDefault().nama_dosen_xls;
                string KdNIk = cm.FirstOrDefault().kd_nik_xls;
                tbl_dosen tblDosen = dc.tbl_dosen.Where(d => d.kd_nik == KdNIk).FirstOrDefault();

                int pegawaiId = tblDosen.pegawai_id.Value;
                tbl_pegawai tblPegawai = dc.tbl_pegawai.Where(d => d.id == pegawaiId).FirstOrDefault();
                DateTime tangglSk = DateTime.Now;
                if (tblPegawai.tanggal_sk != null)
                {
                    tangglSk = tblPegawai.tanggal_sk.Value;
                    paramMulaiMengajar = tangglSk.Day + "/" + tangglSk.Month + "/" + tangglSk.Year;
                }
                else paramMulaiMengajar = " ";
                tbl_dosen_jenjang tblDosenJenjang = tblDosen.tbl_dosen_jenjang.FirstOrDefault();
                int pendidikanid = tblDosenJenjang.pendidikan_id.Value;
                int jabatanid = tblDosenJenjang.jabatan_fungsional_id.Value;
                paramPendidikan = dc.tbl_ref_pendidikan.Where(d => d.id == pendidikanid).FirstOrDefault().tingkat_pendidikan;
                paramJabatan = dc.tbl_ref_jabatan_fungsional.Where(d => d.id == jabatanid).FirstOrDefault().jabatan_fungsional;
                paramStatusDosen = tblDosen.tbl_ref_status_dosen.status_dosen;
                paramGolongan = tblDosenJenjang.tbl_ref_jenjang.jenjang_kelompok;
                paramTotTrans = cm.FirstOrDefault().total_transport.ToString();
                paramTransPerHari = cm.FirstOrDefault().trans_per_hari.ToString();
                paramTotalNgajarPriode = cm.FirstOrDefault().total_honor_per_priode.ToString();
                paramTotalKeseluruhan = cm.FirstOrDefault().total_keseluruhan.ToString();
            }
            string dateNow = NOW.Day.ToString() + " " + ConvertToNamaBulan(NOW.Month) + " " + NOW.Year;

            ReportDataSource rd = new ReportDataSource("DataSet1", cm);


            ReportParameter parm1 = new ReportParameter("param1", param1);
            ReportParameter parm2 = new ReportParameter("paramThnAjaran", param2);
            ReportParameter parm3 = new ReportParameter("paramSemester", param3);
            ReportParameter parmNIk = new ReportParameter("paramNik", paramNIk);
            ReportParameter parmNama = new ReportParameter("paramNama", paramNama);
            ReportParameter parmMulaiMengajar = new ReportParameter("paramMulaiMengajar", paramMulaiMengajar);
            ReportParameter parmPendidikan = new ReportParameter("paramPendidikan", paramPendidikan);
            ReportParameter parmJabatan = new ReportParameter("paramJabatan", paramJabatan);
            ReportParameter parmStatusDosen = new ReportParameter("paramStatusDosen", paramStatusDosen);
            ReportParameter parmGolongan = new ReportParameter("paramGolongan", paramGolongan);
            ReportParameter parmTotTrans = new ReportParameter("paramTotTrans", paramTotTrans);
            ReportParameter parmTransPerHari = new ReportParameter("paramTransPerHari", paramTransPerHari);
            ReportParameter parmTotalNgajarPriode = new ReportParameter("paramTotPriode", paramTotalNgajarPriode);
            ReportParameter parmTotalKeseluruhan = new ReportParameter("paramTotalKeseluruhan", paramTotalKeseluruhan);
            ReportParameter parmTglTTD = new ReportParameter("paramTglTTD", dateNow);

            if (tblDosenS.status_dosen_id == 5)
            {

                lr.SetParameters(new ReportParameter[] { parm1, parm2, parm3, parmMulaiMengajar, parmPendidikan, parmJabatan, parmStatusDosen,
                    parmGolongan,parmTglTTD });
            }
            else
            {
                lr.SetParameters(new ReportParameter[] { parm1, parm2, parm3, parmNIk, parmNama, parmMulaiMengajar, parmPendidikan, parmJabatan,
                parmStatusDosen,parmGolongan,parmTglTTD});
            }
            //lr.SetParameters(new ReportParameter[] { parm1, parm2, parm3, parmNIk, parmNama,
            //    parmMulaiMengajar, parmPendidikan, parmPendidikan,parmJabatan,parmStatusDosen,parmGolongan,parmTotTrans,
            //    parmTransPerHari,parmTotalNgajarPriode,parmTotalKeseluruhan });
            //lr.SetParameters(parm1);
            //lr.SetParameters(parm2);
            //lr.SetParameters(parm3);
            //lr.SetParameters(parmNIk);
            //lr.SetParameters(parmNama);
            //lr.SetParameters(parmMulaiMengajar);
            //lr.SetParameters(parmPendidikan);
            //lr.SetParameters(parmJabatan);
            //lr.SetParameters(parmStatusDosen);
            //lr.SetParameters(parmGolongan);
            //lr.SetParameters(parmTotTrans);
            //lr.SetParameters(parmTransPerHari);
            //lr.SetParameters(parmTotalNgajarPriode);
            //lr.SetParameters(parmTotalKeseluruhan);
            lr.DataSources.Add(rd);

            string reportType = "pdf";
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

           "<DeviceInfo>" +
          "  <OutputFormat>PDF</OutputFormat>" +
          "  <PageWidth>8.267in</PageWidth>" +
          "  <PageHeight>11.692in</PageHeight>" +
          "  <MarginTop>0.25in</MarginTop>" +
          "  <MarginLeft>0.10in</MarginLeft>" +
          "  <MarginRight>0.10in</MarginRight>" +
          "  <MarginBottom>0.25in</MarginBottom>" +
          "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }

        public ActionResult ReportPersetujuanPdf(int reportId)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "ReportPersetujuan.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<tbl_report_honor_dosen> cm = new List<tbl_report_honor_dosen>();
            string periode = "";
            string noTahun = "";
            string tanggal = "";
            using (stmtsdmEntities dc = new stmtsdmEntities())
            {
                tbl_generate_report tblGenReport = new tbl_generate_report();
                tblGenReport = dc.tbl_generate_report.Where(d => d.id == reportId).FirstOrDefault();
                if (tblGenReport == null)
                {
                    return Content("Report Tidak Ada");
                }
                cm = dc.tbl_report_honor_dosen.Where(d => d.reportid == reportId && d.nomor != null).OrderBy(d => d.kode_bank).ToList();
                int i = 1;
                foreach (var item in cm)
                {
                    item.nomor = i;
                    i++;
                }
                periode = @"" + tblGenReport.report_start.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_start.Value.Month) +
                        " s/d " + tblGenReport.report_end.Value.Day + " " + ConvertToNamaBulan(tblGenReport.report_end.Value.Month) + " " + tblGenReport.report_end.Value.Year;
                noTahun = tblGenReport.create_on.Value.Year.ToString();
                tanggal = tblGenReport.create_on.Value.Day + " " + ConvertToNamaBulan(tblGenReport.create_on.Value.Month) + " " + tblGenReport.create_on.Value.Year;
            }
            string KabagAkademik = ConfigurationManager.AppSettings["KabagAkademik"].ToString();
            ReportParameter par1 = new ReportParameter("periode", periode);
            ReportParameter par2 = new ReportParameter("tanggal", tanggal);
            ReportParameter par3 = new ReportParameter("noTahun", noTahun);
            ReportParameter par4 = new ReportParameter("KabagAkademik", KabagAkademik);

            lr.SetParameters(new ReportParameter[] { par1, par2, par3, par4 });
            
            string reportType = "pdf";
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

           "<DeviceInfo>" +
          "  <OutputFormat>PDF</OutputFormat>" +
          "  <PageWidth>8.267in</PageWidth>" +
          "  <PageHeight>11.692in</PageHeight>" +
          "  <MarginTop>0.25in</MarginTop>" +
          "  <MarginLeft>0.25in</MarginLeft>" +
          "  <MarginRight>0.25in</MarginRight>" +
          "  <MarginBottom>0.25in</MarginBottom>" +
          "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }

        public ActionResult ReportPdf()
        {
            return View();
        }

        public ActionResult ReportRpt()
        {
            ReportClass rptH = new ReportClass();
            rptH.FileName = Server.MapPath("/Report/honorDosenReguler.rpt");
            rptH.Load();

            DataSetSTMT ds = new DataSetSTMT();
            DataSetSTMT.tbl_report_honor_dosenDataTable ss = new DataSetSTMT.tbl_report_honor_dosenDataTable();

            tbl_report_honor_dosenTableAdapter zz = new tbl_report_honor_dosenTableAdapter();

            var a = zz.GetData();
            

            //rptH.SetDataSource(listHonor);
           

            Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        public ActionResult ReportDetailPdf()
        {
            return View();
        }

        public ActionResult ReportTotalPdf()
        {
            return View();
        }

        #region generate report kelas reguler
        public ActionResult GenerateKelasReguler()
        {
            return View();
        }
        public ActionResult GenerateKelasInternational()
        {
            return View();
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPDSTMT.Helper;
using SPDSTMT.Models;

namespace SPDSTMT.Controllers
{
    public class KehadiranDosenController : BaseController
    {
        public ActionResult Index()
        {
            return View(db.tbl_kehadiran_dosen.ToList());
        }

        //
        // GET: /KehadiranDosen/Details/5

        public ActionResult Details(int id = 0)
        {
            tbl_kehadiran_dosen tbl_kehadiran_dosen = db.tbl_kehadiran_dosen.Find(id);
            if (tbl_kehadiran_dosen == null)
            {
                return HttpNotFound();
            }
            return View(tbl_kehadiran_dosen);
        }

        //
        // GET: /KehadiranDosen/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /KehadiranDosen/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_kehadiran_dosen tbl_kehadiran_dosen)
        {
            if (ModelState.IsValid)
            {
                db.tbl_kehadiran_dosen.Add(tbl_kehadiran_dosen);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_kehadiran_dosen);
        }

        //
        // GET: /KehadiranDosen/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tbl_kehadiran_dosen tbl_kehadiran_dosen = db.tbl_kehadiran_dosen.Find(id);
            if (tbl_kehadiran_dosen == null)
            {
                return HttpNotFound();
            }
            return View(tbl_kehadiran_dosen);
        }

        //
        // POST: /KehadiranDosen/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_kehadiran_dosen tbl_kehadiran_dosen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_kehadiran_dosen).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_kehadiran_dosen);
        }

        //
        // GET: /KehadiranDosen/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tbl_kehadiran_dosen tbl_kehadiran_dosen = db.tbl_kehadiran_dosen.Find(id);
            if (tbl_kehadiran_dosen == null)
            {
                return HttpNotFound();
            }
            return View(tbl_kehadiran_dosen);
        }

        //
        // POST: /KehadiranDosen/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_kehadiran_dosen tbl_kehadiran_dosen = db.tbl_kehadiran_dosen.Find(id);
            db.tbl_kehadiran_dosen.Remove(tbl_kehadiran_dosen);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public class DataTableDataKehadiranDosen : DataTableData
        {
            public List<tbl_kehadiran_dosen> data { get; set; }
        }
        [HttpPost]
        [Authorize]
        public JsonResult GetListKehadiranDosen(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string KdNik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string NamaDosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
            string StatusDosen = System.Web.HttpContext.Current.Request["status_dosen"].ToString();

            List<tbl_kehadiran_dosen> lstKhdDosen = new List<tbl_kehadiran_dosen>();
            DataTableDataKehadiranDosen dt = new DataTableDataKehadiranDosen();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            using (var db = new stmtsdmEntities())
            {
                var a = db.tbl_kehadiran_dosen.OrderByDescending(d=>d.id).AsQueryable();
                dt.recordsTotal = a.Count();
                if (KdNik != string.Empty) a = a.Where(x => x.code_lecturer.Contains(KdNik));
                if (NamaDosen != string.Empty) a = a.Where(x => x.fullname_user.Contains(NamaDosen));
                if (StatusDosen != string.Empty)
                {
                    //int statusDosenId = Convert.ToInt32(StatusDosen);
                    //a = a.Where(x => x.status_dosen_id == statusDosenId);
                }
                if (sortColumn == 0)
                {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.code_lecturer);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.code_lecturer);
                }
                if (sortColumn == 1)
                {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.fullname_user);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.fullname_user);
                }

                dt.data = a.Skip(start).Take(length).ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult ajaxSave(ViewKehadiranDosen kehadiran_dosen)
        {
             string message = "";
            int status = 0;
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    tbl_kehadiran_dosen newKehadiranDosen = new tbl_kehadiran_dosen();
                    newKehadiranDosen.code_lecturer = kehadiran_dosen.code_lecturer;
                    newKehadiranDosen.fullname_user = kehadiran_dosen.fullname_user;
                    newKehadiranDosen.code_course = kehadiran_dosen.code_course;
                    newKehadiranDosen.kelas = kehadiran_dosen.kelas;
                    newKehadiranDosen.name_course = kehadiran_dosen.name_course;
                    newKehadiranDosen.credit_course = kehadiran_dosen.credit_course;
                    newKehadiranDosen.date_document = helper.ConvertDate(kehadiran_dosen.date_document, "dd/MM/yyyy");
                    newKehadiranDosen.create_on = DateTime.Now;
                    newKehadiranDosen.create_by = User.Identity.Name;
                    db.tbl_kehadiran_dosen.Add(newKehadiranDosen);
                    db.SaveChanges();
                    status = 1;
                    message = helper.SaveSuksesHtml();
                }
            }
            catch (Exception ex) { message = helper.ErrorMessageWithElement(ex.ToString()); status = 0; }
            finally { resultMessage.message = message; resultMessage.status = status; }

            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public JsonResult ajaxEdit(ViewKehadiranDosen kehadiran_dosen)
        {
            string message = "";
            int status = 0;
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    if(db.tbl_kehadiran_dosen.Find(kehadiran_dosen.id)!=null){
                        tbl_kehadiran_dosen newKehadiranDosen = db.tbl_kehadiran_dosen.Find(kehadiran_dosen.id);
                        newKehadiranDosen.code_lecturer = kehadiran_dosen.code_lecturer;
                        newKehadiranDosen.fullname_user = kehadiran_dosen.fullname_user;
                        newKehadiranDosen.code_course = kehadiran_dosen.code_course;
                        newKehadiranDosen.kelas = kehadiran_dosen.kelas;
                        newKehadiranDosen.name_course = kehadiran_dosen.name_course;
                        newKehadiranDosen.credit_course = kehadiran_dosen.credit_course;
                        newKehadiranDosen.date_document = helper.ConvertDate(kehadiran_dosen.date_document, "dd/MM/yyyy");
                        newKehadiranDosen.modified_on = DateTime.Now;
                        newKehadiranDosen.modified_by = User.Identity.Name;
                        db.SaveChanges();
                        status = 1;
                        message = helper.UpdateSuksesHtml();
                    }
                }
            }
            catch (Exception ex) { message = helper.ErrorMessageWithElement(ex.ToString()); status = 0; }
            finally { resultMessage.message = message; resultMessage.status = status; }

            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult ajaxDelete(int id)
        {
            string message = "";
            int status = 0;
            try
            {
                using (var db = new stmtsdmEntities())
                {
                    if (db.tbl_kehadiran_dosen.Find(id) != null)
                    {
                        tbl_kehadiran_dosen newKehadiranDosen = db.tbl_kehadiran_dosen.Find(id);
                        db.tbl_kehadiran_dosen.Remove(newKehadiranDosen);
                        db.SaveChanges();
                        status = 1;
                        message = helper.RemoveSuksesHtml();
                    }
                }
            }
            catch (Exception ex) { message = helper.ErrorMessageWithElement(ex.ToString()); status = 0; }
            finally { resultMessage.message = message; resultMessage.status = status; }

            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
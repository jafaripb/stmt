﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using System.Reflection;
using System.Text;
using System.IO;

namespace SPDSTMT.Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Home/
       // string baseUrl = ConfigurationManager.AppSettings["baseUrl"];
        [Authorize]
        public ActionResult Index()
        {
           // ViewBag.baseUrl = baseUrl;
            return View(ViewBag);
        }

        public ActionResult Login()
        {
           // ViewBag.baseUrl = baseUrl;
            return View();
        }

        public ActionResult Logout()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult GoLogin(string username,string password)
        {
           // ViewBag.baseUrl = baseUrl;
            try
            {
                if (ModelState.IsValid && WebSecurity.Login(username, password, persistCookie: true))
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");

                }
            }
            catch (ReflectionTypeLoadException ex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Exception exSub in ex.LoaderExceptions)
                {
                    sb.AppendLine(exSub.Message);
                    FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
                    if (exFileNotFound != null)
                    {
                        if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                        {
                            sb.AppendLine("Fusion Log:");
                            sb.AppendLine(exFileNotFound.FusionLog);
                        }
                    }
                    sb.AppendLine();
                }
                return Content( sb.ToString());
                //Display or log the error based on your application.
            }
            
        }
    }
}

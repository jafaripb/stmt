﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPDSTMT.Models;

namespace SPDSTMT.Controllers
{
    public class AdminController : Controller
    {
        private stmtsdmEntities db = new stmtsdmEntities();
        ResultMessage resultMessage = new ResultMessage();
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
            //return View(db.tbl_username.ToList());
        }
        public class DataTableDataUserRole : DataTableData
        {
            public List<VWUserRole> data { get; set; }
        }
        [Authorize(Roles = "super,admin")]
        public JsonResult GetListUsers(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search"].ToString();
            DataTableDataUserRole dt = new DataTableDataUserRole();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities())
            {
                var a = (from b in db.tbl_username
                         join c in db.tbl_user_role on b.id equals c.username_id
                         select new VWUserRole
                         {
                             UserId = b.id,
                             roleId = c.id,
                             username = b.username,
                             role = c.role
                         }).OrderBy(d=>d.UserId).AsQueryable();
                dt.recordsTotal = a.Count();
                if(string.IsNullOrEmpty(search)) {
                    a=a.Where(d=>d.username.Contains(search)).AsQueryable();                   
                }
                dt.data = a.Skip(start).Take(length).ToList();
                 dt.recordsFiltered = a.Count();
                
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "super,admin")]
        public JsonResult AddEditUser(VWUserRole VuserRole)
        {
            int status = 0;
            string message = "";
            try
            {
                if (VuserRole.UserId > 0)
                {
                    tbl_username MtblUsername = db.tbl_username.Find(VuserRole.UserId);
                    if (MtblUsername != null)
                    {
                        MtblUsername.username = VuserRole.username;
                        MtblUsername.password = VuserRole.password;
                    }

                    tbl_user_role MtblUserRole = db.tbl_user_role.Where(d => d.username_id == VuserRole.UserId).FirstOrDefault();
                    if (MtblUserRole != null)
                    {
                        MtblUserRole.role = VuserRole.role;
                    }
                    db.SaveChanges();
                    status = 1;
                    message = Helper.helper.UpdateSuksesHtml();
                }
                else
                {
                    tbl_username MtblUsername = new tbl_username();
                    MtblUsername.username = VuserRole.username;
                    MtblUsername.password = VuserRole.password;
                    tbl_username cekusername = db.tbl_username.Where(d=>d.username==d.username).FirstOrDefault();
                    if (cekusername == null)
                    {
                        db.tbl_username.Add(MtblUsername);
                        db.SaveChanges();
                        tbl_user_role MtblUserRole = new tbl_user_role();
                        MtblUserRole.role = VuserRole.role;
                        MtblUserRole.username_id = MtblUsername.id;
                        db.tbl_user_role.Add(MtblUserRole);
                        db.SaveChanges();
                        status = 1;
                        message = Helper.helper.SaveSuksesHtml();
                    }
                    else
                    {
                        status = 0;
                        message = Helper.helper.ErrorMessageWithElement("User '" + cekusername+"' Sudah Ada" );
                    }
                }
            }
            catch (Exception ex)
            {
                status = 1;
                message = Helper.helper.ErrorMessageWithElement(ex.ToString());
            }
            finally
            {
                resultMessage.status = status;
                resultMessage.message = message;
            }
           
            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "super,admin")]
        public JsonResult ajaxDelete(int id)
        {
            int status = 0;
            string message = "";
            try
            {
                if (id > 0)
                {
                    tbl_username MtblUsername = db.tbl_username.Find(id);
                    if (MtblUsername != null)
                    {
                        db.tbl_username.Remove(MtblUsername);
                    }

                    tbl_user_role MtblUserRole = db.tbl_user_role.Where(d => d.username_id ==id).FirstOrDefault();
                    if (MtblUserRole != null)
                    {
                        db.tbl_user_role.Remove(MtblUserRole);
                    }
                    db.SaveChanges();
                    status = 1;
                    message = Helper.helper.RemoveSuksesHtml();
                }
            }
            catch (Exception ex)
            {
                status = 0;
                message = Helper.helper.ErrorMessageWithElement(ex.ToString());
            }
            finally
            {
                resultMessage.status = status;
                resultMessage.message = message;
            }

            return Json(resultMessage, JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "super,admin")]
        public ActionResult Details(int id = 0)
        {
            tbl_username tbl_username = db.tbl_username.Find(id);
            if (tbl_username == null)
            {
                return HttpNotFound();
            }
            return View(tbl_username);
        }

        //
        // GET: /Admin/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_username tbl_username)
        {
            if (ModelState.IsValid)
            {
                db.tbl_username.Add(tbl_username);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_username);
        }

        //
        // GET: /Admin/Edit/5

        public ActionResult Edit(int id = 0)
        {
            VWUserRole VWuserRole = (from b in db.tbl_username
                                     join c in db.tbl_user_role on b.id equals c.username_id
                                     where b.id == id
                                     select new VWUserRole
                                     {
                                         username=b.username,
                                         role=c.role,
                                         roleId=c.id,
                                         UserId=b.id
                                     }).FirstOrDefault();
            ViewBag.SelectRole = new SelectList(db.tbl_user_role, "id", "role", VWuserRole.roleId);

            if (VWuserRole == null)
            {
                return HttpNotFound();
            }
            return View(VWuserRole);
        }

        //
        // POST: /Admin/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbl_username tbl_username)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_username).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_username);
        }

        //
        // GET: /Admin/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tbl_username tbl_username = db.tbl_username.Find(id);
            if (tbl_username == null)
            {
                return HttpNotFound();
            }
            return View(tbl_username);
        }

        //
        // POST: /Admin/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_username tbl_username = db.tbl_username.Find(id);
            db.tbl_username.Remove(tbl_username);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPDSTMT.Models;

namespace SPDSTMT.Controllers
{
    public class BP3IPController : Controller
    {
        private stmtsdmEntities db = new stmtsdmEntities();

        //
        // GET: /BP3IP/

        public ActionResult Index()
        {
            return View(db.view_honor_bp3ip.ToList());
        }
        public class DataTableHonorMengajarBP3IP : DataTableData
        {
            public List<view_honor_mengajar_bp3ip> data { get; set; }
        }

        public JsonResult GetListHonorMengajarBP3IP(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string nama_dosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
            string kd_nik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string mata_kuliah = System.Web.HttpContext.Current.Request["mata_kuliah"].ToString();
            string angkatan = System.Web.HttpContext.Current.Request["angkatan"].ToString();
            DataTableHonorMengajarBP3IP dt = new DataTableHonorMengajarBP3IP();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities())
            {
                // var a = db.tbl_ref_jenjang.AsQueryable();
                var a = db.view_honor_mengajar_bp3ip.AsQueryable();

                if (nama_dosen != string.Empty) a = a.Where(x => x.nama_dosen.Contains(nama_dosen));
                if (kd_nik != string.Empty) a = a.Where(x => x.kd_nik.Contains(kd_nik));
                if (angkatan != string.Empty)
                {
                    int ankatanInt = Convert.ToInt32(angkatan);
                    a = a.Where(x => x.angkatan == ankatanInt);
                }
                if (sortColumn == 0)
                {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik);
                }
                if (sortColumn == 1)
                {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.nama_dosen);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_dosen);
                }
                a = a.Skip(start).Take(length);

                dt.data = a.ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Mengajar()
        {
            return View();
        }
        
        public class DataTableHonorBP3IP : DataTableData
        {
            public List<view_honor_bp3ip> data { get; set; }
        }

        public JsonResult GetListHonorBP3IP(int draw, int start, int length)
        {
            string search = System.Web.HttpContext.Current.Request["search[value]"].ToString();
            string nama_dosen = System.Web.HttpContext.Current.Request["nama_dosen"].ToString();
            string kd_nik = System.Web.HttpContext.Current.Request["kd_nik"].ToString();
            string mata_kuliah = System.Web.HttpContext.Current.Request["mata_kuliah"].ToString();
            string angkatan = System.Web.HttpContext.Current.Request["angkatan"].ToString();
            DataTableHonorBP3IP dt = new DataTableHonorBP3IP();
            int sortColumn = 0;
            string sortDirection = "asc";
            if (length == -1)
            {
                length = 0;
            }
            sortColumn = Convert.ToInt32(System.Web.HttpContext.Current.Request["order[0][column]"].ToString());
            sortDirection = System.Web.HttpContext.Current.Request["order[0][dir]"].ToString();
            //}
            using (var db = new stmtsdmEntities())
            {
                // var a = db.tbl_ref_jenjang.AsQueryable();
                var a = db.view_honor_bp3ip.AsQueryable();

                if (nama_dosen != string.Empty) a = a.Where(x => x.nama_dosen.Contains(nama_dosen));
                if (kd_nik != string.Empty) a = a.Where(x => x.kd_nik.Contains(kd_nik));
                if (angkatan != string.Empty)
                {
                    int ankatanInt = Convert.ToInt32(angkatan);
                    a = a.Where(x => x.angkatan == ankatanInt);
                }
                if (sortColumn == 0)
                {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.kd_nik);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.kd_nik);
                }
                if (sortColumn == 1)
                {
                    if (sortDirection == "asc") a = a.OrderBy(x => x.nama_dosen);
                    if (sortDirection == "desc") a = a.OrderByDescending(x => x.nama_dosen);
                }
                a = a.Skip(start).Take(length);

                dt.data = a.ToList();
                dt.recordsFiltered = a.Count();
            }
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /BP3IP/Details/5

        public ActionResult Details(string id = null)
        {
            view_honor_bp3ip view_honor_bp3ip = db.view_honor_bp3ip.Find(id);
            if (view_honor_bp3ip == null)
            {
                return HttpNotFound();
            }
            return View(view_honor_bp3ip);
        }

        //
        // GET: /BP3IP/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BP3IP/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(view_honor_bp3ip view_honor_bp3ip)
        {
            if (ModelState.IsValid)
            {
                db.view_honor_bp3ip.Add(view_honor_bp3ip);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(view_honor_bp3ip);
        }

        //
        // GET: /BP3IP/Edit/5

        public ActionResult Edit(string id = null)
        {
            view_honor_bp3ip view_honor_bp3ip = db.view_honor_bp3ip.Find(id);
            if (view_honor_bp3ip == null)
            {
                return HttpNotFound();
            }
            return View(view_honor_bp3ip);
        }

        //
        // POST: /BP3IP/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(view_honor_bp3ip view_honor_bp3ip)
        {
            if (ModelState.IsValid)
            {
                db.Entry(view_honor_bp3ip).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(view_honor_bp3ip);
        }

        //
        // GET: /BP3IP/Delete/5

        public ActionResult Delete(string id = null)
        {
            view_honor_bp3ip view_honor_bp3ip = db.view_honor_bp3ip.Find(id);
            if (view_honor_bp3ip == null)
            {
                return HttpNotFound();
            }
            return View(view_honor_bp3ip);
        }

        //
        // POST: /BP3IP/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            view_honor_bp3ip view_honor_bp3ip = db.view_honor_bp3ip.Find(id);
            db.view_honor_bp3ip.Remove(view_honor_bp3ip);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
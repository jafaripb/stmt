﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SPDSTMT.Models;

namespace SPDSTMT.Controllers
{
    public class BaseController : Controller
    {
        protected stmtsdmEntities db = new stmtsdmEntities();
        protected string user = System.Web.HttpContext.Current.User.Identity.Name;
        protected DateTime NOW = DateTime.Now;
        protected ResultMessage resultMessage = new ResultMessage();
        protected ValidasiModel validasiModel = new ValidasiModel();

    }
}

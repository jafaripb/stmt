﻿
$(function () {
    $('.datepicker').datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true,
        //showButtonPanel: true,
        yearRange: "-60:+0",
        onClose: function (selectedDate) {
            
        }
    });
    $("[name=code_lecturer").autocomplete({
        source: "../MasterData/ajaxListKDNIKDosen?take=10",
        minLength: 2,
        select: function (event, ui) {
           // $(this).parent().next().html(ui.item.satuan);
            $("#code_lecturer").val(ui.item.code_lecturer);
            $("#fullname_user").val(ui.item.fullname_user);
            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        var html = '<div class="vendor">' +
                '<div class="box-typehead-content">' +
                  '<span class="box-typehead-title-auto">' + item.fullname_user + '</span>' +
                  '<span class="box-typehead-desk-auto"> ' + item.code_lecturer + '</span>' +
                '</div>' +
            '</div>'
        return $("<li class='item'>")
            .data("item.autocomplete", item)
            .append(html)
            .appendTo(ul);

    };
    $("#name_course").autocomplete({
        source: "../MasterData/ajaxListMataKuliah?take=10",
        minLength: 2,
        select: function (event, ui) {
            // $(this).parent().next().html(ui.item.satuan);
            $("#name_course").val(ui.item.name_course);
            $("#code_course").val(ui.item.code_course);
            $("#kelas").val(ui.item.kelas);
            $("#credit_course").val(ui.item.credit_course);
            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        var html = '<div class="vendor">' +
                '<div class="box-typehead-content">' +
                  '<span class="box-typehead-title-auto">' + item.name_course + '</span>' +
                  '<span class="box-typehead-desk-auto"> ' + item.code_course + '</span>' +
                  '<span class="box-typehead-desk-auto"> (' + item.kelas + ')</span>' 
                '</div>' +
            '</div>'
        return $("<li class='item'>")
            .data("item.autocomplete", item)
            .append(html)
            .appendTo(ul);
    };
    $("#kelas").autocomplete({
        source: "../MasterData/ajaxListKelas?take=10",
        minLength: 2,
        select: function (event, ui) {
            // $(this).parent().next().html(ui.item.satuan);
            $("#kelas").val(ui.item.label);
            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        var html = '<div class="vendor">' +
                '<div class="box-typehead-content">' +
                  '<span class="box-typehead-title-auto">' + item.label + '</span>' 
        '</div>' +
    '</div>'
        return $("<li class='item'>")
            .data("item.autocomplete", item)
            .append(html)
            .appendTo(ul);

    };
    $(".btnSave").on("click", function () {
        var newKehadiranDosen = {};
        newKehadiranDosen.code_lecturer = $("#code_lecturer").val();
        newKehadiranDosen.code_course = $("#code_course").val();
        newKehadiranDosen.fullname_user = $("#fullname_user").val();
        newKehadiranDosen.kelas = $("#kelas").val();
        newKehadiranDosen.name_course = $("#name_course").val();
        newKehadiranDosen.credit_course = $("#credit_course").val();
        newKehadiranDosen.date_document = $("#date_document").val();
        $.ajax({
            method: "POST",
            url: "ajaxSave",
            dataType: "json",
            data: JSON.stringify(newKehadiranDosen),
            contentType: 'application/json; charset=utf-8'
        }).done(function (data) {
            $("#info_submit").html(data.message);
            $("#info_submit").click();
            ClearForm();
        });
    });
    $(".btnUpdate").on("click", function () {
        var newKehadiranDosen = {};
        newKehadiranDosen.id = $("#id").val();
        newKehadiranDosen.code_lecturer = $("#code_lecturer").val();
        newKehadiranDosen.code_course = $("#code_course").val();
        newKehadiranDosen.fullname_user = $("#fullname_user").val();
        newKehadiranDosen.kelas = $("#kelas").val();
        newKehadiranDosen.name_course = $("#name_course").val();
        newKehadiranDosen.credit_course = $("#credit_course").val();
        newKehadiranDosen.date_document = $("#date_document").val();
        $.ajax({
            method: "POST",
            url: "ajaxEdit",
            dataType: "json",
            data: JSON.stringify(newKehadiranDosen),
            contentType: 'application/json; charset=utf-8'
        }).done(function (data) {
            $("#info_submit").html(data.message);
            $("#info_submit").click();
           // ClearForm();
        });
    });
    
});

function reValue(el,val) {
    $(el).val(val);
}

function ClearForm() {

    $("input").val("");
    $("textarea").val("");
    $("select").val("");
}

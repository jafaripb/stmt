﻿function generateYearSelect(idSlect) {
    var minOffset = 0, maxOffset = 4;

    var thisYear = new Date().getFullYear();
    var select = $(idSlect);

    for (var i = minOffset; i <= maxOffset; i++) {
        
        var year = thisYear - i;
        console.log(year);
        $('<option>', { value: year + "/"+(year+1), text: year + "/"+(year+1) }).appendTo(select);
    }
   // select.appendTo('body');
}

function ClearForm() {

    $("input").val("");
    $("textarea").val("");
    $("select").val("");
}

function formDeleteDialog(el, url, elinfo, otable) {
    $(el).dialog({
        height: 160,
        modal: true,
        buttons: {
            "Delete": function () {
                $.ajax(url)
              .done(function (data) {
                  $(elinfo).html(data.message);
                  $(el).dialog("close");
                  //reloadSelected();
                  otable.draw();
              });
            },
            Cancel: function () {
                $(el).dialog("close");
            }
        }
    });
}